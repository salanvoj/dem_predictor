import numpy as np
import matplotlib.pyplot as plt
import torch
import network_s2d
import network_d2rpz
from scipy import ndimage
import glob

if __name__ == '__main__':
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    model_s2d = network_s2d.Net()
    #model_s2d.load_state_dict(torch.load("../data/s2d_network/net_s2d_p5", map_location=device))
    model_s2d.load_state_dict(torch.load("/run/user/1001/gvfs/sftp:host=goedel.felk.cvut.cz/mnt/home.dokt/salanvoj/python_scripts/dem_predictor/data/s2d2rpz_network/run_p5_rpz_o1_2020-06-03_08:17:49/net_s2d_epoch_000095", map_location=device))
    #model_s2d.load_state_dict(torch.load("/run/user/1001/gvfs/sftp:host=goedel.felk.cvut.cz/mnt/home.dokt/salanvoj/python_scripts/dem_predictor/data/s2d2rpz_network/run_p5_rpz_o1_2020-06-01_09:07:10/net_s2d_epoch_000099", map_location=device))
    #model_s2d.load_state_dict(torch.load("/run/user/1001/gvfs/sftp:host=goedel.felk.cvut.cz/mnt/home.dokt/salanvoj/python_scripts/dem_predictor/data/s2d2rpz_network/run_p5_sc_rpz_o1_2020-06-03_14:38:02/net_s2d_epoch_000095", map_location=device))

    model_s2d.to(device)
    model_s2d_rpzt = network_s2d.Net()
    #model_s2d_rpzt.load_state_dict(torch.load("/run/user/1001/gvfs/sftp:host=goedel.felk.cvut.cz/mnt/home.dokt/salanvoj/python_scripts/dem_predictor/data/s2d2rpz_network/run_p5_2020-05-28_15:36:20/net_s2d_epoch_000019", map_location=device))
    model_s2d_rpzt.load_state_dict(torch.load("/run/user/1001/gvfs/sftp:host=goedel.felk.cvut.cz/mnt/home.dokt/salanvoj/python_scripts/dem_predictor/data/s2d2rpz_network/run_p5_2020-05-28_15:36:20/net_s2d_epoch_000019", map_location=device))
    #model_s2d_rpzt.load_state_dict(torch.load("/run/user/1001/gvfs/sftp:host=goedel.felk.cvut.cz/mnt/home.dokt/salanvoj/python_scripts/dem_predictor/data/s2d2rpz_network/run_p5_sc_kkt_2020-06-03_14:39:13/net_s2d_epoch_000019", map_location=device))
    model_s2d_rpzt.to(device)
    model_d2rpz = network_d2rpz.Net()
    model_d2rpz.load_state_dict(torch.load("../data/d2rpz_network/net_weights_d2rpz", map_location=device))
    model_d2rpz.to(device)
    loss_roll = 0
    loss_pitch = 0
    loss_roll_t = 0
    loss_pitch_t = 0
    loss_z = 0
    loss_z_t = 0
    loss_dem = 0
    loss_dem_t = 0
    #[0.10078910178883367, 0.04759928625754596, 0.10395024159561372, 0.10561107663707234, 0.12186254793431305, 0.14321598974620386, 0.197681359219132, 0.2375222770497203]

    FPR_t_all = []
    TPR_t_all = []
    FPR_all = []
    TPR_all = []
    data_path = 's2d_tst_big'
    #data_path = 's2d_josef2'
    #data_path = 's2d_palety_tst_new'

    num_labels = len(glob.glob('../data/'+data_path + '/*label.npz'))
    for thresholds in [0]:#np.linspace(0,0.1,200).tolist():
        loss_roll = []
        loss_pitch = []
        loss_roll_t = []
        loss_pitch_t = []
        loss_z = []
        loss_z_t = []
        loss_abs = []
        loss_abs_t = []
        rpz_all = []
        rpz_gt_all = []
        rpzt_all = []
        #num_labels = 17
        for i in range(0,num_labels,1):
            label = np.load('../data/'+data_path+'/000{:03}'.format(i)+'_label.npz')

            yaw_mask = abs(label['yaw_label'])<(np.pi/8)

            dem_label = label['label']
            #dem_label_d = label['label_d']


            yaw_indexes = np.where(yaw_mask)  ## here are indexes of known roll, pitch, z

            roll_label = label['roll_label']
            pitch_label = label['pitch_label']
            z_label = label['z_baselink_label']
            roll_label[~yaw_mask] = np.nan
            pitch_label[~yaw_mask] = np.nan
            z_label[~yaw_mask] = np.nan


            vis = label['visible_mask']
            vis[~np.isnan(vis)] = 0
            vis[np.isnan(vis)] = 1
            bw_dist = ndimage.distance_transform_edt(vis)
            pos_mask = ~np.isnan(yaw_mask) & (bw_dist < 10)  # distance in decimeters from visible
            pos_indexes = np.where(pos_mask)

            roll_label[~pos_mask] = np.nan
            pitch_label[~pos_mask] = np.nan
            z_label[~pos_mask] = np.nan

            rpz = np.stack([roll_label,pitch_label,z_label])
            mask = torch.from_numpy((~np.isnan(label['input'])).astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0)
            input = torch.from_numpy(label['input'].astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0)

            dem_weights = ~np.isnan(dem_label) & (bw_dist < 10)  # distance in decimeters from visible

            input[torch.isnan(input)] = 0
            input_w_mask = torch.cat([input, mask], 1)
            dense = model_s2d(input_w_mask)
            #dense = dense.cpu().detach().numpy()
            #dense[:, :, mask.cpu().detach().numpy().astype(bool)[0, 0, :, :]] = input.cpu().detach().numpy()[:, :, mask.cpu().detach().numpy().astype(bool)[0, 0, :, :]]
            #dense = torch.tensor(dense).to(device)
            #dense = input_w_mask
            output_rpz = model_d2rpz(dense[:, 0, :, :][np.newaxis]).permute([1, 0, 2, 3])
            output_rpz = output_rpz.detach().cpu().numpy()
            output_rpz = np.pad(output_rpz[:, 0, :, :], [(0, 0), (3, 3), (4, 4)], mode='constant')


            dense_rpzt = model_s2d_rpzt(input_w_mask)
            #dense_rpzt = dense_rpzt.cpu().detach().numpy()
            #dense_rpzt[:, :, mask.cpu().detach().numpy().astype(bool)[0, 0, :, :]] = input.cpu().detach().numpy()[:, :,
            #                                                                    mask.cpu().detach().numpy().astype(bool)[0,
            #                                                                    0, :, :]]
            #dense_rpzt = torch.tensor(dense_rpzt).to(device)
            output_rpzt = model_d2rpz(dense_rpzt[:, 0, :, :][np.newaxis]).permute([1, 0, 2, 3])
            output_rpzt = output_rpzt.detach().cpu().numpy()
            output_rpzt = np.pad(output_rpzt[:, 0, :, :], [(0, 0), (3, 3), (4, 4)], mode='constant')
            deg_th = 0
            rpz_mask = (abs(rpz[0,:,:]) >deg_th) | (abs(rpz[1,:,:]) >deg_th)
            if (np.sum(yaw_mask & pos_mask)>0)&(np.sum(np.isnan(rpz[0, yaw_mask&pos_mask].T))==0):
                loss_roll.append((rpz[0, yaw_mask&pos_mask&rpz_mask].T-output_rpz[0,yaw_mask&pos_mask&rpz_mask].T)**2)
                loss_pitch.append((rpz[1, yaw_mask&pos_mask&rpz_mask].T-output_rpz[1,yaw_mask&pos_mask&rpz_mask].T)**2)
                loss_roll_t.append((rpz[0, yaw_mask&pos_mask&rpz_mask].T-output_rpzt[0,yaw_mask&pos_mask&rpz_mask].T)**2)
                loss_pitch_t.append((rpz[1, yaw_mask&pos_mask&rpz_mask].T-output_rpzt[1,yaw_mask&pos_mask&rpz_mask].T)**2)
                loss_z.append((rpz[2, yaw_mask & pos_mask].T - output_rpz[2, yaw_mask & pos_mask].T) ** 2)
                loss_z_t.append((rpz[2, yaw_mask & pos_mask].T - output_rpzt[2, yaw_mask & pos_mask].T) ** 2)
                rpzt_all.append(output_rpzt[:,yaw_mask&pos_mask&rpz_mask])
                rpz_all.append(output_rpz[:,yaw_mask&pos_mask&rpz_mask])
                rpz_gt_all.append(rpz[:,yaw_mask&pos_mask&rpz_mask])
    #            loss_roll += np.sqrt(np.mean((rpz[0, yaw_mask&pos_mask].T-output_rpz[0,yaw_mask&pos_mask].T)**2))
    #            loss_pitch += np.sqrt(np.mean((rpz[1, yaw_mask&pos_mask].T-output_rpz[1,yaw_mask&pos_mask].T)**2))
    #            loss_roll_t += np.sqrt(np.mean((rpz[0, yaw_mask&pos_mask].T-output_rpzt[0,yaw_mask&pos_mask].T)**2))
    #            loss_pitch_t += np.sqrt(np.mean((rpz[1, yaw_mask&pos_mask].T-output_rpzt[1,yaw_mask&pos_mask].T)**2))
    #            loss_z += np.sqrt(np.mean((rpz[2, yaw_mask & pos_mask].T - output_rpz[2, yaw_mask & pos_mask].T) ** 2))
    #            loss_z_t += np.sqrt(np.mean((rpz[2, yaw_mask & pos_mask].T - output_rpzt[2, yaw_mask & pos_mask].T) ** 2))
            dem_label[np.isnan(dem_label)] = 0
            #dem_label[dem_label > 0.5] = 0.5
            dem_weights[(dem_label>0.3)] = 0
            #dem_weights[(dem_label<0.2)] = 0

            #dem_weights[~np.isnan(label['input'])] = 0

            #dem_label = dem_label[64:-64, 64:-64]
            #dem_weights = dem_weights[64:-64, 64:-64]
            #dense=dense[:, :, 64:192, 64:192]
            #dense_rpzt=dense_rpzt[:, :, 64:192, 64:192]
            #input=input[:, :, 64:192, 64:192]

            #dem_label = dem_label[64:-64, 64:-64]
            #dem_weights = dem_weights[64:-64, 64:-64]
            #dense=dense[:, :, 64:192, 64:192]
            #dense_rpzt=dense_rpzt[:, :, 64:192, 64:192]
            #input=input[:, :, 64:192, 64:192]

            output = dense
            target = torch.from_numpy(dem_label.astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0)
            dem_weights_rpz = dem_weights.copy()
            #dem_weights_rpz[(dem_label >= 0.5)&(output.cpu().detach().numpy()[0,0] > 0.5)] = 0
            weight = torch.from_numpy(dem_weights_rpz.astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0)
            pred = (output[:, 0, :, :][np.newaxis]).permute([1, 0, 2, 3])
            loss_prediction = torch.sqrt((torch.sum(weight * (pred - target) ** 2) / weight.sum()))
            loss_abs.append(abs((weight * (pred - target))[weight == 1].cpu().detach().numpy()))

            output = dense_rpzt
            dem_weights_rpzt = dem_weights.copy()
            #dem_weights_rpzt[(dem_label >= 0.5) & (output.cpu().detach().numpy()[0, 0] > 0.5)] = 0
            weight = torch.from_numpy(dem_weights_rpzt.astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0)
            pred = (output[:, 0, :, :][np.newaxis]).permute([1, 0, 2, 3])
            loss_prediction = torch.sqrt((torch.sum(weight * (pred - target) ** 2) / weight.sum()))
            loss_abs_t.append(abs((weight * (pred - target))[weight == 1].cpu().detach().numpy()))

            #loss_abs_t = abs((weight * (pred - target))[weight == 1].cpu().detach().numpy())

            _,loss_dem_one,_ = network_s2d.weighted_mse_loss(dense,torch.from_numpy(dem_label.astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0),torch.from_numpy(dem_weights.astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0))
            _,loss_dem_t_one,_= network_s2d.weighted_mse_loss(dense_rpzt,torch.from_numpy(dem_label.astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0),torch.from_numpy(dem_weights.astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0))
            loss_dem += loss_dem_one.cpu().detach().numpy()
            loss_dem_t += loss_dem_t_one.cpu().detach().numpy()

            if False:
                print([i, loss_dem,loss_dem_t])
                # plot predictions

                #i_min = 105
                #i_max = 156
                #j_min = 145
                #j_max = 196
                i_min = 0
                i_max = 256
                j_min = 0
                j_max = 256

                z_max = 0.4
                z_min = -0.1

                #dem_label = dem_label_d
                input_np = input.detach().cpu().numpy()
                #input_np[:,:,np.isnan(label['input'])] = -0.3
                input_np[input_np==0] = np.nan
                input_np[input_np>z_max] = z_max
                input_np[input_np<z_min] = z_min

                #dem_label[dem_weights==0] =-0.3
                dem_label[dem_label>z_max] = z_max
                dem_label[dem_label<z_min] = z_min

                dem_label[dem_label == 0] = np.nan
                #dem_label[i_min,j_min] = z_max

                dense_np  =(dense.detach().cpu().numpy())
                #dense_np[:,:,dem_weights == 0] = -0.5
                dense_np[dense_np>z_max] = z_max
                dense_np[dense_np<z_min] = z_min

                #dense_np[0,0,i_min,j_min] = z_max
                dense_rpzt_np = (dense_rpzt.detach().cpu().numpy())
                #dense_rpzt_np[:, :, dem_weights == 0] = -0.5
                dense_rpzt_np[dense_rpzt_np>z_max] = z_max
                #dense_rpzt_np[0,0,i_min,j_min] = z_max
                dense_rpzt_np[dense_rpzt_np<z_min] = z_min



                plt.imshow(input_np[0, 0, i_min:i_max, j_min:j_max])
                plt.colorbar()

                plt.show()
                plt.imshow((dem_label[ i_min:i_max,j_min:j_max]))
                #plt.colorbar()

                plt.show()
                #plt.imshow((output_rpzt[0,i_min:i_max, j_min:j_max]))
                #plt.colorbar()
                #plt.show()

                #toplot = abs(dense_np[0, 0, i_min:i_max, j_min:j_max] - (dem_label[i_min:i_max, j_min:j_max]))
                toplot =dense_np[0, 0, i_min:i_max, j_min:j_max]
                plt.imshow(toplot)
                plt.colorbar()
                plt.show()

                #toplot = abs((dense_rpzt_np[0, 0, i_min:i_max, j_min:j_max]) - (dem_label[i_min:i_max, j_min:j_max]))
                toplot =dense_rpzt_np[0, 0, i_min:i_max, j_min:j_max]
                plt.imshow(toplot)
                plt.colorbar()


                plt.show()
                '''
                fig, ax = plt.subplots(3, 3)
                ax[0, 0].imshow(input.detach().cpu().numpy()[0, 0])
                d1 =  ax[0, 1].imshow(dense.detach().cpu().numpy()[0, 0])
                fig.colorbar(d1,ax=ax[0,1])
    
                d2 = ax[0, 2].imshow(dense_rpzt.detach().cpu().numpy()[0, 0,:,:])
                fig.colorbar(d2,ax=ax[0,2])
    
                ax[1, 0].plot(rpz[0, yaw_mask].T)
                ax[1, 0].plot(output_rpz[0, yaw_mask].T)
                ax[1, 0].plot(output_rpzt[0, yaw_mask].T)
                ax[1, 1].plot(rpz[1, yaw_mask].T)
                ax[1, 1].plot(output_rpz[1, yaw_mask].T)
                ax[1, 1].plot(output_rpzt[1, yaw_mask].T)
                ax[1, 2].plot(rpz[2, yaw_mask].T)
                ax[1, 2].plot(output_rpz[2, yaw_mask].T)
                ax[1, 2].plot(output_rpzt[2, yaw_mask].T)
                ax[1, 2].plot(dem_label[yaw_mask])
                ax[2, 0].imshow(dem_label)
                #e1 = ax[2, 1].imshow(abs(dem_label-dem_label_d)*dem_weights)
    
                e1 = ax[2, 1].imshow(abs(dem_label-dense.detach().cpu().numpy()[0, 0])*dem_weights)
                fig.colorbar(e1,ax=ax[2,1])
                e2 = ax[2, 2].imshow(abs(dem_label-dense_rpzt.detach().cpu().numpy()[0, 0])*dem_weights)
                fig.colorbar(e2,ax=ax[2,2])
                print(sum(sum(abs(dem_label-dense.detach().cpu().numpy()[0, 0])*dem_weights)))
    
                print(sum(sum(abs(dem_label-dense_rpzt.detach().cpu().numpy()[0, 0])*dem_weights)))
                #plt.colorbar()
    
                plt.show()
                '''
                raw_input("Press Enter to continue...")
                plt.close()

        gt_index_nt = abs(np.concatenate(rpz_gt_all,1)[0:2,:])>=thresholds
        rpz_index_nt = abs(np.concatenate(rpz_all,1)[0:2,:])>=thresholds
        rpzt_index_nt = abs(np.concatenate(rpzt_all,1)[0:2,:])>=thresholds

        TP = (sum((sum(gt_index_nt == rpz_index_nt)==2) & (sum(gt_index_nt == True)==2))).astype(np.float32)
        P = sum(sum(gt_index_nt == True)==2)
        TPR =  TP/P

        FP = (sum((sum(gt_index_nt != rpz_index_nt)>0) & (sum(gt_index_nt == False)>0))).astype(np.float32)
        N = sum(sum(gt_index_nt == False)>0)
        FPR = FP/N

        TPR_all.append(TPR)
        FPR_all.append(FPR)
        print TPR

        TP = (sum((sum(gt_index_nt == rpzt_index_nt) == 2) & (sum(gt_index_nt == True) == 2))).astype(np.float32)
        #P = sum(sum(gt_index_nt == True))
        TPR = TP / P

        FP = (sum((sum(gt_index_nt != rpzt_index_nt) > 0) & (sum(gt_index_nt == False) > 0))).astype(np.float32)
        #N = sum(sum(gt_index_nt == False))
        FPR = FP / N

        TPR_t_all.append(TPR)
        FPR_t_all.append(FPR)

        print TPR

        #print (sum(gt_index_nt))
        #print ((sum((gt_index_nt == rpz_index_nt) & (gt_index_nt == True)))).astype(np.float32)/(sum(gt_index_nt))
        #print ((sum((gt_index_nt == rpzt_index_nt) & (gt_index_nt == True)))).astype(np.float32)/(sum(gt_index_nt))
        #print (sum((gt_index_nt == rpz_index_nt) & (gt_index_nt == True))).astype(np.float32)/np.concatenate(rpz_gt_all, 1)[1, :].size
        #print (sum((gt_index_nt == rpzt_index_nt) & (gt_index_nt == True))).astype(np.float32)/np.concatenate(rpz_gt_all, 1)[1, :].size
        #TP.append((sum((gt_index_nt == rpz_index_nt) & (gt_index_nt == True))).astype(np.float32)/np.concatenate(rpz_gt_all, 1)[1, :].size)
        #TP_t.append((sum((gt_index_nt == rpzt_index_nt) & (gt_index_nt == True))).astype(np.float32)/np.concatenate(rpz_gt_all, 1)[1, :].size)
        loss_abs = np.concatenate(loss_abs)
        loss_abs_t = np.concatenate(loss_abs_t)
        print np.mean(loss_abs)
        print np.mean(loss_abs_t)
        print(["error",'-s2d-','-s2d_kkt-'])
        print(['roll ','{:.3}'.format(np.sqrt(np.mean(np.concatenate(loss_roll)))), '{:.3}'.format(np.sqrt(np.mean(np.concatenate(loss_roll_t))))])
        print(['pitch','{:.3}'.format(np.sqrt(np.mean(np.concatenate(loss_pitch)))), '{:.3}'.format(np.sqrt(np.mean(np.concatenate(loss_pitch_t))))])
        print(['z    ','{:.3}'.format(np.sqrt(np.mean(np.concatenate(loss_z)))), '{:.3}'.format(np.sqrt(np.mean(np.concatenate(loss_z_t))))])
        print(['dem  ','{:.3}'.format(loss_dem/num_labels), '{:.3}'.format(loss_dem_t/num_labels)])
        #print([loss_pitch/num_labels, loss_pitch_t/num_labels, loss_roll/num_labels, loss_roll_t/num_labels, loss_z/num_labels, loss_z_t/num_labels, loss_dem/num_labels, loss_dem_t/num_labels])
    print FPR
    np.save('FPR_rpz_all', np.asarray(FPR_all))
    np.save('TPR_rpz_all',np.asarray(TPR_all))
    np.save('FPR_t_all', np.asarray(FPR_t_all))
    np.save('TPR_t_all', np.asarray(TPR_t_all))

    #print TP_t
    #plt.plot(TP)
    #plt.plot(TP_t)
    #plt.show()












