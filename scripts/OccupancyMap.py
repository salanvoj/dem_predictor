import numpy as np
import matplotlib.cm as cm
import sensor_msgs.point_cloud2 as pc2
from voxel_map import VoxelMap
from nav_msgs.msg import OccupancyGrid
from geometry_msgs.msg import Point
import struct
from sensor_msgs.msg import PointCloud2,PointField
from std_msgs.msg import Header
import rospy
import ros_numpy
import time


def filter_pc(map):
    grid_res = map.msg.info.resolution
    x, l, v = map.voxel_map.get_voxels()
    x = x[:, v > 0]
    l = l[v > 0]
    column_size = 15
    nx = x[0, :]
    x_allgrid = nx[:, None] * np.ones([1, column_size])
    ny = x[1, :]
    y_allgrid = ny[:, None] * np.ones([1, column_size])
    nz = x[2, :]
    z_allgrid = nz[:, None] - np.arange(grid_res, grid_res * (column_size + 1), grid_res)
    to_get_under = np.array([x_allgrid.T.ravel(), y_allgrid.T.ravel(), z_allgrid.T.ravel()])
    v_under = map.voxel_map.get_voxels(to_get_under, np.zeros(to_get_under.shape[1]))
    v_under = v_under.reshape(column_size, nx.shape[0])
    v_sum = sum((np.isnan(v_under) | (v_under > 0)).astype(int))
    x_del = x[:, v_sum <= 9]
    map.voxel_map.set_voxels(x_del, np.zeros_like(x_del[0, :], dtype=np.float64),
                             np.ones_like(x_del[0, :], dtype=np.float64) * -10)

    z_allgrid = nz[:, None] + np.arange(grid_res, grid_res * (column_size + 1), grid_res)
    to_get_over = np.array([x_allgrid.T.ravel(), y_allgrid.T.ravel(), z_allgrid.T.ravel()])
    v_over = map.voxel_map.get_voxels(to_get_over, np.zeros(to_get_over.shape[1]))
    v_over = v_over.reshape(column_size, nx.shape[0])
    v_sum_over = sum((np.isnan(v_over)).astype(int))
    x_del = x[:, v_sum_over > 13]
    map.voxel_map.set_voxels(x_del, np.zeros_like(x_del[0, :], dtype=np.float64),
                             np.ones_like(x_del[0, :], dtype=np.float64) * -10)

    return map

def costs_to_cols(costs, max_cost = None):

     if max_cost is None:
         max_cost = np.max(costs)
         print("max val %10.5f" % max_cost)
     cols = cm.jet(costs/max_cost)
     #print cols
     return cols

def slots(msg):
     """Return message attributes (slots) as list."""
     return [getattr(msg, var) for var in msg.__slots__]

def array(msg):
     """Return message attributes (slots) as array."""
     return np.array(slots(msg))

def col(arr):
     """Convert array to column vector."""
     return arr.reshape((arr.size, 1))

def logistic(x):
     return 1. / (1. + np.exp(-x))

def points_3d(x):
     """Pad with zeros up to tree rows."""
     assert x.shape[0] <= 3
     x = np.concatenate((x, np.zeros((3 - x.shape[0], x.shape[1]))))
     return x

def points_from_msg(msg, fields=('x', 'y','z')):
     x = pc2.read_points(msg, field_names=fields, skip_nans=True)
     x = np.array(list(x), dtype=np.float64).T
     return x

def mkmat(rows, cols, L):
     mat = np.matrix(L, dtype='float64')
     mat.resize((rows,cols))
     return mat


class OccupancyMap(object):
    def __init__(self, frame_id, resolution=0.15):
        self.voxel_map = VoxelMap(resolution, -1.0, 40.0, 0.0)
        self.min = -100.0
        self.max = 100.0
        self.msg = OccupancyGrid()
        self.msg.header.frame_id = frame_id
        self.msg.info.resolution = resolution
        # Initially, set the grid origin to identity.
        self.msg.info.origin.orientation.w = 1.0

    def to_2d(self, map2d, sensor_to_map):
        # get occupied voxels
        x, l, v = self.voxel_map.get_voxels()
        x = x[:, v > 0]
        # get only points around the robot position
        nx = x[:, (x[0, :] < sensor_to_map.transform.translation.x + 13) &
                  (x[0, :] > sensor_to_map.transform.translation.x - 13) &
                  (x[1, :] < sensor_to_map.transform.translation.y + 13) &
                  (x[1, :] > sensor_to_map.transform.translation.y - 13)]
        mesh_xy = np.meshgrid(np.unique(nx[0, :]), np.unique(nx[1, :]))
        ind = (mesh_xy[0].ravel()[np.newaxis].T == nx[0, :]) & (mesh_xy[1].ravel()[np.newaxis].T == nx[1, :])
        new_x = []
        new_y = []
        new_z = []
        # create 2.5d map from 3d
        new_x = mesh_xy[0].ravel()[np.any(ind, axis=1)]
        new_y = mesh_xy[1].ravel()[np.any(ind, axis=1)]
        index = ind[np.any(ind, axis=1)]
        new_z = [np.max(nx[2, index[i]]) for i in range(len(index))]
        map2d.voxel_map.set_voxels(np.array([new_x, new_y, np.ones_like(new_z) * self.msg.info.resolution / 2]),
                                            np.zeros_like(new_x, dtype=np.float64), new_z)
        return map2d

    def to_2d_highres(self, map2d, sensor_to_map):
        # get occupied voxels
        x, l, v = self.voxel_map.get_voxels()
        x = x[:, v > 0]
        # get only points around the robot position
        nx = x[:, (x[0, :] < sensor_to_map.transform.translation.x + 13) &
                  (x[0, :] > sensor_to_map.transform.translation.x - 13) &
                  (x[1, :] < sensor_to_map.transform.translation.y + 13) &
                  (x[1, :] > sensor_to_map.transform.translation.y - 13)]
        mesh_xy = np.meshgrid(np.unique(nx[0, :]), np.unique(nx[1, :]))
        ind = (mesh_xy[0].ravel()[np.newaxis].T == nx[0, :]) & (mesh_xy[1].ravel()[np.newaxis].T == nx[1, :])
        new_x = []
        new_y = []
        new_z = []
        # create 2.5d map from 3d
        new_x = mesh_xy[0].ravel()[np.any(ind, axis=1)]
        new_y = mesh_xy[1].ravel()[np.any(ind, axis=1)]
        index = ind[np.any(ind, axis=1)]
        new_z = [np.max(nx[2, index[i]]) for i in range(len(index))]
        map2d.voxel_map.set_voxels(np.array([new_x, new_y, np.ones_like(new_z) * map2d.msg.info.resolution / 2]),
                                            np.zeros_like(new_x, dtype=np.float64), new_z)
        return map2d

    def to_2d_precise(self, map2d, sensor_to_map, all_points):
        # get occupied voxels
        #x, l, v = self.voxel_map.get_voxels()
        #x = x[:, v > 0]
        # get only points around the robot position

        t0 = time.time()
        new_x = []
        new_y = []
        new_z = []

        n_all_points = all_points[:,(all_points[0, :] < sensor_to_map.transform.translation.x + 13) &
                  (all_points[0, :] > sensor_to_map.transform.translation.x - 13) &
                  (all_points[1, :] < sensor_to_map.transform.translation.y + 13) &
                  (all_points[1, :] > sensor_to_map.transform.translation.y - 13)]
        n_all_points[0:2,:] = (np.floor((n_all_points[0:2, :] * 10)) + 0.5) / 10
        mesh_xy = np.meshgrid(np.unique(n_all_points[0, :]), np.unique(n_all_points[1, :]))

        t1 = time.time()

        ind = (mesh_xy[0].ravel()[np.newaxis].T == n_all_points[0, :]) & (mesh_xy[1].ravel()[np.newaxis].T == n_all_points[1, :])

        # create 2.5d map from 3d
        new_x = mesh_xy[0].ravel()[np.any(ind, axis=1)]
        new_y = mesh_xy[1].ravel()[np.any(ind, axis=1)]

        index = ind[np.any(ind, axis=1)]

        t2 = time.time()

        new_z = [np.max(n_all_points[2, index[i]]) for i in range(len(index))]
        t3 = time.time()

        #for i in range(len(new_x)):
            #new_z = [np.max(n_all_points[2, (((n_all_points[0, :][np.newaxis] - new_x[np.newaxis].T) < 0.1) & ((n_all_points[1, :][np.newaxis] - new_y[np.newaxis].T) < 0.1))[i]])]

        #all_points[:, (((all_points[0, :][np.newaxis] - new_x[np.newaxis].T) < 0.1) & ((all_points[1, :][np.newaxis] - new_y[np.newaxis].T) < 0.1))[0]]

        #new_z = all_points[:,(all_points[0,:] -new_x[0]<0.1)&(all_points[1,:] -new_y[0]<0.1)]

        map2d.voxel_map.set_voxels(np.array([new_x, new_y, np.ones_like(new_z) * self.msg.info.resolution / 2]),
                                            np.zeros_like(new_x, dtype=np.float64), new_z)
        t4 = time.time()
        print(t1-t0)
        print(t2-t1)
        print(t3-t2)
        print(t4-t3)

        return map2d


    def to_3d(self, map3d, sensor_to_map):
        x, l, v = self.voxel_map.get_voxels()
        map3d.voxel_map.set_voxels(np.array([x[0,:], x[1,:], v+sensor_to_map.transform.translation.z ]),
                                   np.zeros_like(v, dtype=np.float64),  np.ones_like(v, dtype=np.float64))
        return map3d

    def to_3d_dem(self, map3d_dem, sensor_to_map=None, depth=0.2):
        x, l, v = self.voxel_map.get_voxels()
        x = x[:,~np.isnan(v)]
        v = v[~np.isnan(v)]
        x = np.array([x[0, :], x[1, :], v])
        x_old = x
        for i in range(int(-depth / self.voxel_map.voxel_size), int(np.max(v) / self.voxel_map.voxel_size)):
            x_new = x_old[0:2, v > (i * self.voxel_map.voxel_size)]
            x_new = np.array([x_new[0, :], x_new[1, :], np.ones_like(x_new[0, :]) * i * self.voxel_map.voxel_size])
            x = np.hstack((x, x_new))
        if(sensor_to_map is not None):
            x[2, :] = x[2, :] + sensor_to_map.transform.translation.z
        map3d_dem.voxel_map.set_voxels(x, np.zeros_like(x[2, :], dtype=np.float64), np.ones_like(x[2, :], dtype=np.float64))

        return map3d_dem

    def to_3d_dem_with_conf(self, map3d_dem, map2d_conf, sensor_to_map=None, depth=0.2):
        x, l, v = self.voxel_map.get_voxels()
        x_conf,_ , v_conf = map2d_conf.voxel_map.get_voxels()
        x = x[:,~np.isnan(v)]
        v = v[~np.isnan(v)]
        x = np.array([x[0, :], x[1, :], v, v_conf])
        x_old = x
        for i in range(int(-depth / self.voxel_map.voxel_size), int(np.max(v) / self.voxel_map.voxel_size)):
            x_new = x_old[0:2, v > (i * self.voxel_map.voxel_size)]
            x_new_conf = x_old[3, v > (i * self.voxel_map.voxel_size)]
            x_new = np.array([x_new[0, :], x_new[1, :], np.ones_like(x_new[0, :]) * i * self.voxel_map.voxel_size, x_new_conf])
            x = np.hstack((x, x_new))
        if(sensor_to_map is not None):
            x[2, :] = x[2, :] + sensor_to_map.transform.translation.z
        map3d_dem.voxel_map.set_voxels(x[0:3,:], np.zeros_like(x[2, :], dtype=np.float64), np.ones_like(x[2, :], dtype=np.float64))
        map3d_dem_conf = OccupancyMap(self.msg.header.frame_id, self.msg.info.resolution)
        map3d_dem_conf.voxel_map.set_voxels(x[0:3,:], np.zeros_like(x[2, :], dtype=np.float64),x[3,:])
        return map3d_dem, map3d_dem_conf


    def to_pc_msg(self, frame_id, occ_value=0):
        vals, l, costs = self.voxel_map.get_voxels()
        v = vals[:,costs> occ_value]
        c = costs[costs> occ_value]

        data = np.zeros(len(v.T), dtype=[
            ('x', np.float32),
            ('y', np.float32),
            ('z', np.float32),
            ('val', np.float32)
        ])
        data['x'] = v[0,:]
        data['y'] = v[1,:]
        data['z'] = v[2,:]
        data['val'] = c

        msg = ros_numpy.msgify(PointCloud2, data)
        msg.header.frame_id = frame_id
        msg.header.stamp = rospy.Time.now()
        return msg



    def to_pc_msg_with_conf(self, frame_id, confidence_map2d, occ_value=0):
        vals, l, costs = self.voxel_map.get_voxels()
        c_vals, _, confs = confidence_map2d.voxel_map.get_voxels()
        v = vals[:,costs> occ_value]
        c = costs[costs> occ_value]
        conf = confs[costs> occ_value]

        data = np.zeros(len(v.T), dtype=[
            ('x', np.float32),
            ('y', np.float32),
            ('z', np.float32),
            ('val', np.float32),
            ('confidence',np.float32)
        ])
        data['x'] = v[0,:]
        data['y'] = v[1,:]
        data['z'] = v[2,:]
        data['val'] = c
        data['confidence'] = conf

        msg = ros_numpy.msgify(PointCloud2, data)
        msg.header.frame_id = frame_id
        msg.header.stamp = rospy.Time.now()
        return msg

    def to_pc_msg_with_conf(self, frame_id, confidence_map2d, occ_value=0):
        vals, l, costs = self.voxel_map.get_voxels()
        c_vals, _, confs = confidence_map2d.voxel_map.get_voxels()
        v = vals[:,costs> occ_value]
        c = costs[costs> occ_value]
        conf = confs[costs> occ_value]

        data = np.zeros(len(v.T), dtype=[
            ('x', np.float32),
            ('y', np.float32),
            ('z', np.float32),
            ('val', np.float32),
            ('confidence',np.float32)
        ])
        data['x'] = v[0,:]
        data['y'] = v[1,:]
        data['z'] = v[2,:]
        data['val'] = c
        data['confidence'] = conf

        msg = ros_numpy.msgify(PointCloud2, data)
        msg.header.frame_id = frame_id
        msg.header.stamp = rospy.Time.now()
        return msg


    # Functions below are depricated and has to be checked again before use



    def map_to_grid(self, x):
        """Transform points from map coordinates to grid."""
        # TODO: Handle orientation too.
        x = x - col(array(self.msg.info.origin.position))
        return x

    def grid_to_map(self, x):
        """Transform points from grid coordinates to map."""
        # TODO: Handle orientation too.
        x = x + col(array(self.msg.info.origin.position))
        return x

    def fit_grid(self):
        """Accommodate the grid to contain all points."""
        # Update grid origin so that all coordinates are non-negative.
        x, _, v = self.voxel_map.get_voxels()
        x = x[:2]  # Only x,y used in 2D grid.
        x_min = x.min(axis=1) - self.voxel_map.voxel_size / 2.
        x_max = x.max(axis=1) + self.voxel_map.voxel_size / 2.
        nx = np.round((x_max - x_min) / self.msg.info.resolution).astype(np.int)
        self.msg.info.origin.position = Point(x_min[0], x_min[1], 0.0)
        self.msg.info.width, self.msg.info.height = nx

    def grid_voxels(self):
        """Return voxel coordinates corresponding to the current grid."""
        i, j = np.meshgrid(np.arange(self.msg.info.width),
                           np.arange(self.msg.info.height),
                           indexing='xy')
        x = np.stack((i.ravel(), j.ravel(), np.zeros_like(i).ravel()))
        x = (x + 0.5) * self.msg.info.resolution
        return x

    def to_msg(self):
        """Return as grid message. (Update grid parameters as needed.)"""
        self.fit_grid()
        x = self.grid_voxels()
        x = self.grid_to_map(x)
        x[2, :] = self.voxel_map.voxel_size / 2.0
        l = np.zeros((x.shape[1],))
        v = self.voxel_map.get_voxels(x, l)
        v = 100. * logistic(v)
        v[np.isnan(v)] = -1.
        self.msg.data = v.astype(int).tolist()
        return self.msg

    def fill_nans(self, val=-0.5):
        self.fit_grid()
        x = self.grid_voxels()
        x = self.grid_to_map(x)
        x[2, :] = self.voxel_map.voxel_size / 2.0
        l = np.zeros((x.shape[1],))
        v = self.voxel_map.get_voxels(x, l)
        # v = 100. * logistic(v)
        v[np.isnan(v)] = val
        self.voxel_map.set_voxels(x, l, v)
        # self.msg.data = v.astype(int).tolist()
        return self.msg

    def voxel_map_points(self, x):
        x = points_3d(x.copy())
        x[2, :] = self.voxel_map.voxel_size / 2.0
        return x

    def update(self, x, y, stamp):
        """Update internal occupancy map."""
        x = self.voxel_map_points(x)
        y = self.voxel_map_points(y)
        if x.shape[1] == 1:
            x = np.broadcast_to(x, y.shape)
        elif y.shape[1] == 1:
            y = np.broadcast_to(y, x.shape)
        self.voxel_map.update_lines(x, y)
        self.clip_values()
        self.msg.header.stamp = stamp
        self.msg.info.map_load_time = stamp

    def occupied(self, x):
        x = self.voxel_map_points(x)
        l = np.zeros((x.shape[1],))
        v = self.voxel_map.get_voxels(x, l)
        occupied = v > self.voxel_map.occupied_threshold
        return occupied

    def clip_values(self):
        x, l, v = self.voxel_map.get_voxels()
        v = np.clip(v, self.min, self.max)
        self.voxel_map.set_voxels(x, l, v)

    def voxelmap_to_matrix(self, map_to_origin):
        # i, j = np.meshgrid(np.arange(self.msg.info.width + 2), np.arange(self.msg.info.height + 2), indexing='xy')
        i, j = np.meshgrid(np.arange(-100, 100), np.arange(-100, 100), indexing='xy')
        # x[0:2] -= 1
        i = (i + 0.5) * self.msg.info.resolution
        j = (j + 0.5) * self.msg.info.resolution
        i += map_to_origin[0]
        j += map_to_origin[1]
        x = np.stack((i.ravel(), j.ravel(), np.ones_like(i).ravel() * self.voxel_map.voxel_size / 2.0))
        # x = (x + 0.5) * self.msg.info.resolution
        # x[0:2] = x[0:2] + map_to_origin[0:2]
        # x = self.grid_to_map(x)
        l = np.zeros((x.shape[1],))
        v = self.voxel_map.get_voxels(x, l)
        v = v.reshape([200, 200])
        return v, i, j