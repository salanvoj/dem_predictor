import numpy as np
import matplotlib.pyplot as plt
import torch
import network_sf2d
import network_d2rpz
from scipy import ndimage
import glob
#import rospy
#from sensor_msgs.msg import PointCloud2, Image
from PIL import Image as PilImage
#from mit_semseg.models import ModelBuilder, SegmentationModule
import torch.nn as nn
#import ros_numpy
import cv2
from torchvision.transforms import Compose, CenterCrop, Normalize, Resize, ToTensor
from project_features import Render


def class_mapping(input):
    c = input.copy()
    # our class 0 = irrelevant
    condition_0 = (c == 2) | (c == 5) | (c == 7) | (c == 8) | (c == 18) | (c == 23) | (c == 24) | (c == 30) | (c == 31) | (c == 37) | (c == 78) | (c == 81) | (c == 82) | (c == 85) | (c == 86) | (c == 92) | (c == 108) | (c == 134) | (c == 137)
    input[condition_0] = 0

    # class 1 = very soft (water)
    condition_1 = (c == 21) | (c == 26) | (c == 60) | (c == 109) |(c == 113) | (c == 128)
    input[condition_1] = 1

    # class 2 = soft (grass, plants)
    condition_2 = (c == 9) | (c == 17) | (c == 39) | (c == 57) | (c == 66)
    input[condition_2] = 2

    # class 3 = medium (sand)
    condition_3 = (c == 29) | (c == 46) | (c == 91) | (c == 94) | (c == 125)
    input[condition_3] = 3

    # class 4 = hard (trees)
    condition_4 = (c == 4) | (c == 72) | (c == 114) | (c == 115) | (c == 126) | (c == 131) | (c == 149)
    input[condition_4] = 4

    # class 5 = very hard (walls, buildings)
    condition_5 = ~ (condition_0 | condition_1 | condition_2 | condition_3 | condition_4)
    # condition_5 = ~ (condition_0 | condition_1 | condition_2 | condition_3 | condition_4 | (c == -1))     # !!! if there is something unlabelled as -1 in labelling (only for future training)
    input[condition_5] = 5
    return input


def load_resnet_model():
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')
    # Network Builders
    net_encoder = ModelBuilder.build_encoder(
        arch="resnet50dilated",
        fc_dim=2048,
        #weights="/home/honza/trav_project/semantic-segmentation-pytorch/ade20k-resnet50dilated-ppm_deepsup/encoder_epoch_20.pth")
        weights = "/home/salanvoj/PycharmProjects/semantic-segmentation-pytorch/ade20k-resnet50dilated-ppm_deepsup/encoder_epoch_20.pth")

    net_decoder = ModelBuilder.build_decoder(
        arch="ppm_deepsup",
        fc_dim=2048,
        num_class=150,
        #weights="/home/honza/trav_project/semantic-segmentation-pytorch/ade20k-resnet50dilated-ppm_deepsup/decoder_epoch_20.pth",use_softmax=True)
        weights="/home/salanvoj/PycharmProjects/semantic-segmentation-pytorch/ade20k-resnet50dilated-ppm_deepsup/decoder_epoch_20.pth",
        use_softmax=True)
    crit = nn.NLLLoss(ignore_index=-1)

    segmentation_module = SegmentationModule(net_encoder, net_decoder, crit)
    segmentation_module.to(device)
    segmentation_module.eval()

    return segmentation_module

def get_features_resnet(img, resnet, dim_reduction=1):
    input_transform = Compose([
        Resize((1616 / (dim_reduction * 2), 1232 / (dim_reduction * 2)), PilImage.BILINEAR),
        ToTensor(),
        Normalize([.485, .456, .406], [.229, .224, .225]),
    ])
    #image_np = cv2.imdecode(img, cv2.IMREAD_COLOR)
    #image_np = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
    image_np = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    im_pil = PilImage.fromarray(image_np).convert("RGB")
    img = input_transform(im_pil).unsqueeze(0)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    img = img.to(device)

    with torch.no_grad():
        feed_dict = {}
        feed_dict['img_data'] = img
        segSize = (img.shape[2], img.shape[3])
        pred_tmp = resnet(feed_dict, segSize=segSize)
    _, pred = torch.max(pred_tmp, dim=1)
    pred = pred.detach().cpu().numpy()
    pred = pred.transpose((1, 2, 0))
    return pred
    #return class_mapping(pred)

if __name__ == '__main__':

    #rospy.init_node('SupportTerrain_publisher')

    #resnet = load_resnet_model()
    '''
    input_2d_pc_pub = rospy.Publisher('input_2d_pc', PointCloud2, queue_size=2)
    output_2d_pc_pub = rospy.Publisher('output_2d_pc', PointCloud2, queue_size=2)
    st_2d_pc_pub = rospy.Publisher('support_terrain_pc', PointCloud2, queue_size=2)
    hardness_2d_pc_pub = rospy.Publisher('hardness_pc', PointCloud2, queue_size=2)
    im_pub = rospy.Publisher('img', Image, queue_size=2)
    f_pub = rospy.Publisher('im_features', Image, queue_size=2)

    '''
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


    robot_grid = np.meshgrid(np.arange(-12.75, 12.85, 0.1), np.arange(-12.75, 12.85, 0.1))


    data_path = 's2d_3d_outdoor7'


    list_labels = glob.glob(data_path + '/*label.npz')
    num_labels = len(list_labels)

    loss_dem = 0
    loss_st = 0
    loss_roll = []
    loss_pitch = []
    loss_z = []
    loss_abs = []
#    render_class = Render(net='resnet50dilated')


    for i in range(0,num_labels,1):
        label = np.load(list_labels[i])
        im0 = cv2.imdecode(label['img0'], cv2.IMREAD_COLOR)
        im1 = cv2.imdecode(label['img1'], cv2.IMREAD_COLOR)
        im2 = cv2.imdecode(label['img2'], cv2.IMREAD_COLOR)
        im3 = cv2.imdecode(label['img3'], cv2.IMREAD_COLOR)
        im4 = cv2.imdecode(label['img4'], cv2.IMREAD_COLOR)

        input = label['input']
        T_baselink_zpr = label['T_baselink_zpr']

        #features = render_class.render(input, im0, im1, im2, im3, im4, T_baselink_zpr, dim_reduction=1)
        #features = features[1:,:,:]

        input = label['input']
        vis = label['visible_mask']
        vis[~np.isnan(vis)] = 0
        vis[np.isnan(vis)] = 1
        vis[~np.isnan(input)] = 0

        bw_dist = ndimage.distance_transform_edt(vis)
        vis = ~np.isnan(label['label']) & (bw_dist < 10)
        lab = label['label']
        lab[~np.isnan(label['input'])] = label['input'][~np.isnan(label['input'])]
        toplot_label = lab
        toplot_label[~vis] = np.nan
        plt.imsave(list_labels[i].replace(data_path,data_path+'_nf')[:-3]+'png', np.hstack([label['input'],toplot_label,np.argmax(label['hardness'],axis=0)]))
        print('Done: '+str(i)+'/'+str(num_labels))


        features = label['hardness']
        new_features = [False,
                        True,
                        True,
                        True,
                        True,
                        True,
                        True,
                        True,
                        False,
                        True,
                        True,
                        False,
                        True,
                        True,
                        True,
                        True,
                        False,
                        False,
                        True,
                        False,
                        True,
                        True,
                        True,
                        False,
                        False,
                        False,
                        True,
                        True,
                        False,
                        False,
                        False,
                        False,
                        True,
                        True,
                        False,
                        True,
                        False,
                        False,
                        False,
                        True,
                        False,
                        True,
                        False,
                        False,
                        False,
                        False,
                        False,
                        True,
                        False,
                        False,
                        False,
                        False,
                        True,
                        True,
                        True,
                        True,
                        True,
                        False,
                        False,
                        False,
                        True,
                        True,
                        True,
                        False,
                        False,
                        False,
                        False,
                        True,
                        False,
                        False,
                        False,
                        False,
                        False,
                        True,
                        False,
                        False,
                        False,
                        True,
                        False,
                        False,
                        True,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        True,
                        True,
                        False,
                        True,
                        True,
                        True,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        True,
                        True,
                        False,
                        False,
                        False,
                        True,
                        False,
                        False,
                        False,
                        True,
                        True,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        True,
                        True,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        True,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False,
                        False]
        features[new_features,:,:]

        np.savez_compressed(list_labels[i].replace(data_path,data_path+'_nf'), input=label['input'].astype(np.float16),
                            input_d=label['input_d'].astype(np.float16),
                            label=lab.astype(np.float16),
                            label_d=label['label_d'].astype(np.float16),
                            visible_mask=label['visible_mask'].astype(np.uint8),
                            coords_mapframe=label['coords_mapframe'].astype(np.float16),
                            sensor_to_map=label['sensor_to_map'].astype(np.float16),
                            pitch=label['pitch'].astype(np.float16),
                            roll=label['roll'].astype(np.float16),
                            yaw=label['yaw'].astype(np.float16),
                            pitch_label=label['pitch_label'].astype(np.float16),
                            roll_label=label['roll_label'].astype(np.float16),
                            yaw_label=label['yaw_label'].astype(np.float16),
                            z_map=label['z_map'].astype(np.float16),
                            z_baselink=label['z_baselink'].astype(np.float16),
                            z_map_label=label['z_map_label'].astype(np.float16),
                            z_baselink_label=label['z_baselink_label'].astype(np.float16),
                            #input_3d=label['input_3d'].astype(np.float32),
                            #label_3d=label['label_3d'].astype(np.float32),
                            img0=label['img0'],
                            img1=label['img1'],
                            img2=label['img2'],
                            img3=label['img3'],
                            img4=label['img4'],
                            T_baselink_zpr=label['T_baselink_zpr'].astype(np.float16),
                            flipper_fl=label['flipper_fl'].astype(np.float16),
                            flipper_fr=label['flipper_fr'].astype(np.float16),
                            flipper_rl=label['flipper_rl'].astype(np.float16),
                            flipper_rr=label['flipper_rr'].astype(np.float16),
                            hardness= features[new_features,:,:])
                            #hardness=features.astype(np.float16))

