import numpy as np
import rospy
import torch
import matplotlib.pyplot as plt
from ros_numpy import numpify, msgify
from voxel_map import VoxelMap
from sensor_msgs.msg import PointCloud2, CompressedImage
from geometry_msgs.msg import Transform, TransformStamped
from nav_msgs.msg import Odometry
from OccupancyMap import OccupancyMap, filter_pc
from network_s2d import Net
import tf2_ros
import os
from tf.transformations import euler_from_quaternion as efq
import glob
import time
import message_filters
from project_features import Render
import cv2
from nifti_robot_driver_msgs.msg import FlippersStateStamped


DEBUG = True

class DEM_sparse2dense():
    def __init__(self):
        rospy.init_node('DEM_sparse2dense')
        self.grid_res = 0.1
        self.map_frame = 'map'

        self.tf = tf2_ros.Buffer(cache_time=rospy.Duration(300))
        self.tf_sub = tf2_ros.TransformListener(self.tf)

        self.input_map3d = OccupancyMap(self.map_frame, self.grid_res)
        self.input_map2d = OccupancyMap(self.map_frame, self.grid_res)
        self.features_map = []
        for i in range(151):
            self.features_map.append(OccupancyMap(self.map_frame, self.grid_res))

        self.input_map2d_d = OccupancyMap(self.map_frame, self.grid_res)
        self.position_map2d = OccupancyMap(self.map_frame, self.grid_res)
        self.roll_map2d = OccupancyMap(self.map_frame, self.grid_res)
        self.pitch_map2d = OccupancyMap(self.map_frame, self.grid_res)
        self.yaw_map2d = OccupancyMap(self.map_frame, self.grid_res)
        self.z_map2d = OccupancyMap(self.map_frame, self.grid_res)

        self.flipper_fl_map2d = OccupancyMap(self.map_frame, self.grid_res)
        self.flipper_fr_map2d = OccupancyMap(self.map_frame, self.grid_res)
        self.flipper_rl_map2d = OccupancyMap(self.map_frame, self.grid_res)
        self.flipper_rr_map2d = OccupancyMap(self.map_frame, self.grid_res)




        self.visible_map2d = OccupancyMap(self.map_frame, self.grid_res)
        self.visible_map2d.voxel_map.free_update = 1
        self.visible_map2d.voxel_map.hit_update = 1

        #self.points_sub = rospy.Subscriber(rospy.get_param("~bigbox_topic", '/dynamic_point_cloud_bigbox'), PointCloud2, self.points_cb, queue_size=100)
        self.points_sub = message_filters.Subscriber(rospy.get_param("~bigbox_topic", '/dynamic_point_cloud_bigbox'), PointCloud2)

        self.pos_sub = rospy.Subscriber(rospy.get_param("~odom_topic", '/imu_odom'), Odometry, self.pos_cb, queue_size=2)

        self.flippers_sub = rospy.Subscriber(rospy.get_param("~flippers_topic", '/flippers_state'), FlippersStateStamped, self.flippers_cb, queue_size=2)
        self.flippers = []

        self.input_pc_pub = rospy.Publisher('input_pc', PointCloud2, queue_size=2)
        self.input_filtered_pc_pub = rospy.Publisher('input_filtered_pc', PointCloud2, queue_size=2)
        self.input_2d_pc_pub = rospy.Publisher('input_2d_pc',  PointCloud2, queue_size=2)
        self.visible_2d_pc_pub = rospy.Publisher('visible_2d_pc',  PointCloud2, queue_size=2)

        self.model_s2d = Net()
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

        self.data_path = "../data/s2d_3d_outdoor8_kn/"
        try:
            os.listdir(self.data_path)
        except:
            os.makedirs(self.data_path)
        self.number_of_prepared_data = len(glob.glob(self.data_path+'*label.npz'))

        self.new_data = 0
        self.input_label_shift = 10
        self.last_pc_time = 0

        image0_sub = message_filters.Subscriber("/viz/camera_0/image/compressed", CompressedImage)
        image1_sub = message_filters.Subscriber("/viz/camera_1/image/compressed", CompressedImage)
        image2_sub = message_filters.Subscriber("/viz/camera_2/image/compressed", CompressedImage)
        image3_sub = message_filters.Subscriber("/viz/camera_3/image/compressed", CompressedImage)
        image4_sub = message_filters.Subscriber("/viz/camera_4/image/compressed", CompressedImage)
        '''
        cam_info_topic1 = rospy.get_param("~camera_info_topic", "/viz/camera_0/camera_info")
        cam_info_topic2 = rospy.get_param("~camera_info_topic2", "/viz/camera_1/camera_info")
        cam_info_topic3 = rospy.get_param("~camera_info_topic3", "/viz/camera_2/camera_info")
        cam_info_topic4 = rospy.get_param("~camera_info_topic4", "/viz/camera_3/camera_info")
        cam_info_topic5 = rospy.get_param("~camera_info_topic5", "/viz/camera_4/camera_info")
        self.camera_ids = [cam_info_topic1, cam_info_topic2, cam_info_topic3, cam_info_topic4, cam_info_topic5]
        '''
        self.render_class = Render(net='resnet50dilated')

        time_synchro = message_filters.ApproximateTimeSynchronizer([self.points_sub, image0_sub, image1_sub, image2_sub, image3_sub, image4_sub], 10,1)
        time_synchro.registerCallback(self.points_cb)

    def flippers_cb(self,msg):
        self.flippers = [msg.frontLeft,msg.frontRight,msg.rearLeft,msg.rearRight]

    def pos_cb(self, msg):
        pos = msg.pose.pose.position

        self.position_map2d.voxel_map.set_voxels(np.array([pos.x,pos.y, np.ones_like(pos.x) * self.grid_res / 2])[np.newaxis].T,
                                                 np.zeros_like(pos.x, dtype=np.float64)[np.newaxis][np.newaxis], np.array(pos.z)[np.newaxis][np.newaxis])

        #quat = msg.pose.pose.orientation
        try:
            sensor_to_map = self.tf.lookup_transform(self.map_frame, 'base_link', rospy.Time(0))
            #sensor_to_map = self.tf.lookup_transform(self.map_frame, 'base_link', msg.header.stamp, timeout = rospy.Duration(3))
            quat = sensor_to_map.transform.rotation
            trans = sensor_to_map.transform.translation

            roll, pitch, yaw = efq((quat.x, quat.y, quat.z, quat.w))
        except:
            print('postion error')
            return

        self.roll_map2d.voxel_map.set_voxels(
            np.array([trans.x, trans.y, np.ones_like(trans.x) * self.grid_res / 2])[np.newaxis].T,
            np.zeros_like(roll, dtype=np.float64)[np.newaxis][np.newaxis], np.array(roll)[np.newaxis][np.newaxis])
        self.pitch_map2d.voxel_map.set_voxels(
            np.array([trans.x, trans.y, np.ones_like(trans.x) * self.grid_res / 2])[np.newaxis].T,
            np.zeros_like(pitch, dtype=np.float64)[np.newaxis][np.newaxis], np.array(pitch)[np.newaxis][np.newaxis])
        self.yaw_map2d.voxel_map.set_voxels(
            np.array([trans.x, trans.y, np.ones_like(trans.x) * self.grid_res / 2])[np.newaxis].T,
            np.zeros_like(yaw, dtype=np.float64)[np.newaxis][np.newaxis], np.array(yaw)[np.newaxis][np.newaxis])
        self.z_map2d.voxel_map.set_voxels(
            np.array([trans.x, trans.y, np.ones_like(trans.x) * self.grid_res / 2])[np.newaxis].T,
            np.zeros_like(yaw, dtype=np.float64)[np.newaxis][np.newaxis], np.array(trans.z)[np.newaxis][np.newaxis])

        self.flipper_fl_map2d.voxel_map.set_voxels(
            np.array([trans.x, trans.y, np.ones_like(trans.x) * self.grid_res / 2])[np.newaxis].T,
            np.zeros_like(yaw, dtype=np.float64)[np.newaxis][np.newaxis], np.array(self.flippers[0])[np.newaxis][np.newaxis])
        self.flipper_fr_map2d.voxel_map.set_voxels(
            np.array([trans.x, trans.y, np.ones_like(trans.x) * self.grid_res / 2])[np.newaxis].T,
            np.zeros_like(yaw, dtype=np.float64)[np.newaxis][np.newaxis],
            np.array(self.flippers[1])[np.newaxis][np.newaxis])
        self.flipper_rl_map2d.voxel_map.set_voxels(
            np.array([trans.x, trans.y, np.ones_like(trans.x) * self.grid_res / 2])[np.newaxis].T,
            np.zeros_like(yaw, dtype=np.float64)[np.newaxis][np.newaxis],
            np.array(self.flippers[2])[np.newaxis][np.newaxis])
        self.flipper_rr_map2d.voxel_map.set_voxels(
            np.array([trans.x, trans.y, np.ones_like(trans.x) * self.grid_res / 2])[np.newaxis].T,
            np.zeros_like(yaw, dtype=np.float64)[np.newaxis][np.newaxis],
            np.array(self.flippers[3])[np.newaxis][np.newaxis])

        #print([roll,pitch,yaw])
        #x1, y1 = [np.cos(yaw), np.sin(yaw)]
        ## TODO: save roll and pitch to some other maps
        return


    def points_cb(self, pc_msg, im0_msg, im1_msg, im2_msg, im3_msg, im4_msg):
        print('cb')
        if (rospy.get_rostime() - pc_msg.header.stamp).secs > 5:
            print('old msg')
            print((rospy.get_rostime() - pc_msg.header.stamp).secs)
            return
        # get TF
        try:
            sensor_to_map = self.tf.lookup_transform(self.map_frame, pc_msg.header.frame_id, pc_msg.header.stamp, timeout=rospy.Duration(3))
            T = numpify(sensor_to_map.transform)
            sensor_zpr_to_map = self.tf.lookup_transform(self.map_frame, 'base_link_zero_roll_pitch',pc_msg.header.stamp, timeout=rospy.Duration(3))
            T_zpr = numpify(sensor_zpr_to_map.transform)
            baseliknk_zpr = self.tf.lookup_transform('base_link', 'base_link_zero_roll_pitch',pc_msg.header.stamp, timeout=rospy.Duration(3))
            T_baselink_zpr = numpify(baseliknk_zpr.transform)
        except:
            print('no tf')
            return
            #sensor_to_odom = self.tf.lookup_transform('odom', msg.header.frame_id, msg.header.stamp)
            #odom_to_map = self.tf.lookup_transform(self.map_frame, 'odom', rospy.Time(0))
            #T = numpify(odom_to_map.transform) * numpify(sensor_to_odom.transform) # TODO .transform??? + T bellow?
            #sensor_to_map = TransformStamped()
            #sensor_to_map.transform.translation.x = T[0,3]
            #sensor_to_map.transform.translation.y = T[1,3]
            #sensor_to_map.transform.translation.z = T[2,3]

        # map frame could be not known
        # sensor_to_map = self.tf.lookup_transform(self.map_frame, msg.header.frame_id, msg.header.stamp)

        # TODO: FIFO with only surrounding points in robot surroundings
        # create pc from msg
        pc_cloud = numpify(pc_msg)
        pc_array_homo = np.concatenate([np.asarray([pc_cloud['x'].ravel(),pc_cloud['y'].ravel(),pc_cloud['z'].ravel()]), np.ones_like(pc_cloud['z'])[np.newaxis]])
        # cloud in map frame
        pc_cloud_in_map = np.matmul(T, pc_array_homo)

        # creates sensor origin in map frame
        origins = np.ones_like(pc_cloud['x']) * np.array(
            [sensor_to_map.transform.translation.x, sensor_to_map.transform.translation.y,
             sensor_to_map.transform.translation.z], )[np.newaxis].T

        # create 3d voxel map
        #self.input_map3d = OccupancyMap(self.map_frame, self.grid_res)
        self.input_map3d.voxel_map.update_lines(origins, pc_cloud_in_map[:3, :])

        if DEBUG:
            self.input_pc_pub.publish(self.input_map3d.to_pc_msg(self.map_frame))

        # filter ceilings and measurements outliers
        #self.input_map3d = filter_pc(self.input_map3d)

        #if DEBUG:
        #    self.input_filtered_pc_pub.publish(self.input_map3d.to_pc_msg(self.map_frame))

        # 2d from 3d
        #self.input_map2d_d = self.input_map3d.to_2d(self.input_map2d_d, sensor_to_map)


        #self.input_map2d.voxel_map.set_voxels(np.array([ pc_cloud_in_map[0, :],pc_cloud_in_map[1, :], np.ones_like(pc_cloud_in_map[0, :]) * self.grid_res / 2]),
        #                                         np.zeros_like(pc_cloud_in_map[0, :], dtype=np.float64)[np.newaxis][np.newaxis], np.array(pc_cloud_in_map[2, :])[np.newaxis][np.newaxis])

        #self.input_map2d = self.input_map3d.to_2d_highres(self.input_map2d, sensor_to_map)
        t0 = time.time()

        self.input_map2d = self.input_map3d.to_2d_precise(self.input_map2d, sensor_to_map, pc_cloud_in_map[:3, :])
        t1 = time.time()
        print ('---')
        if DEBUG:
            self.input_2d_pc_pub.publish(self.input_map2d.to_pc_msg(self.map_frame, -np.inf))

        # grid in robot frame
        robot_grid = np.meshgrid(np.arange(-12.75, 12.85, 0.1), np.arange(-12.75, 12.85, 0.1))
        robot_grid_3d = np.meshgrid(np.arange(-12.75, 12.85, 0.1), np.arange(-12.75, 12.85, 0.1), np.arange(-0.95, 2.05, 0.1))
        robot_grid_array = np.asarray([robot_grid[0].ravel(), robot_grid[1].ravel(), np.ones_like(robot_grid[1].ravel()) * self.grid_res])
        robot_grid_3d_array = np.asarray([robot_grid_3d[0].ravel(),robot_grid_3d[1].ravel(), robot_grid_3d[2].ravel()])
        # to homogeneous coordinates
        robot_grid_array = np.concatenate([robot_grid_array, np.ones(robot_grid_array.shape[1])[np.newaxis]]) # not sure about z=0.1
        robot_grid_3d_array = np.concatenate([robot_grid_3d_array, np.ones(robot_grid_3d_array.shape[1])[np.newaxis]])

        # transform robot grid to map frame
        robot_grid_mapframe = np.matmul(T, robot_grid_array)
        robot_grid_mapframe_3d = np.matmul(T_zpr, robot_grid_3d_array)[0:3,:]

        # fill the input from 2d map
        robot_grid_mapframe_2d = np.concatenate([robot_grid_mapframe[0:2, :], np.ones(robot_grid_array.shape[1])[np.newaxis] * self.grid_res / 2]) # x,y,z in 2.5d map

        v = self.input_map2d.voxel_map.get_voxels(robot_grid_mapframe_2d, np.zeros(robot_grid_array.shape[1])) - sensor_to_map.transform.translation.z
        # sparse map
        input_grid = np.reshape(v, robot_grid[0].shape)
        v = self.input_map2d_d.voxel_map.get_voxels(robot_grid_mapframe_2d, np.zeros(
            robot_grid_array.shape[1])) - sensor_to_map.transform.translation.z
        # sparse map
        input_grid_d = np.reshape(v, robot_grid[0].shape)
        #mask = torch.from_numpy((~np.isnan(input_grid)).astype(np.float32)).to(self.device).unsqueeze(0).unsqueeze(0)
        #input_grid[np.isnan(input_grid)] = 0
        #input = torch.from_numpy(input_grid.astype(np.float32)).to(self.device).unsqueeze(0).unsqueeze(0)

        directions = np.array([np.cos(np.arange(-3.14, 3.14, 0.03)), np.sin(np.arange(-3.14, 3.14, 0.03)),
                               np.zeros_like(np.arange(-3.14, 3.14, 0.03))])*20
        min_val = -10.0 + sensor_to_map.transform.translation.z
        max_val = 1 + sensor_to_map.transform.translation.z
        max_range = 15.0
        trace_origin = origins[:,0][np.newaxis].T
        trace_origin[2] = self.grid_res / 2
        new_directions = directions
        #new_directions[2] = self.grid_res / 2
        [h, v] = self.input_map2d.voxel_map.trace_rays(np.ones_like(directions) * trace_origin, new_directions, max_range, min_val, max_val, True)

        self.visible_map2d.voxel_map.update_lines(np.ones_like(directions) * trace_origin, h)

        # hrozna prasarna ale funkcni
        im_np0_arr = np.fromstring(im0_msg.data, np.uint8)
        im_np1_arr = np.fromstring(im1_msg.data, np.uint8)
        im_np2_arr = np.fromstring(im2_msg.data, np.uint8)
        im_np3_arr = np.fromstring(im3_msg.data, np.uint8)
        im_np4_arr = np.fromstring(im4_msg.data, np.uint8)

        im_np0 = cv2.imdecode(im_np0_arr, cv2.IMREAD_COLOR)
        im_np1 = cv2.imdecode(im_np1_arr, cv2.IMREAD_COLOR)
        im_np2 = cv2.imdecode(im_np2_arr, cv2.IMREAD_COLOR)
        im_np3 = cv2.imdecode(im_np3_arr, cv2.IMREAD_COLOR)
        im_np4 = cv2.imdecode(im_np4_arr, cv2.IMREAD_COLOR)
        '''
        im_np0 = im_np0[:1150,:,:]
        im_np1 = im_np1[:1150,:,:]
        im_np2 = im_np2[:1150,:,:]
        im_np3 = im_np3[:1150,:,:]
        im_np4 = im_np4[:1150,:,:]
        '''
        input = input_grid.copy()
        #input[np.isnan(input)]=0

        features_all = self.render_class.render(input, im_np0, im_np1, im_np2, im_np3, im_np4, T_baselink_zpr, dim_reduction=1)

        for j in range(150):
            features = features_all[j,:,:]
            self.features_map[j].voxel_map.set_voxels(np.array([robot_grid_mapframe[0,~np.isnan(features.ravel())], robot_grid_mapframe[1,~np.isnan(features.ravel())], np.ones_like(robot_grid_mapframe[0,~np.isnan(features.ravel())]) * self.grid_res / 2]),np.zeros_like(robot_grid_mapframe[0,~np.isnan(features.ravel())], dtype=np.float64)[np.newaxis][np.newaxis], np.array(features.ravel()[~np.isnan(features.ravel())])[np.newaxis][np.newaxis])

        self.save_label(input_grid,input_grid_d, robot_grid_mapframe_2d,robot_grid_mapframe_3d, T, im_np0_arr, im_np1_arr, im_np2_arr, im_np3_arr, im_np4_arr, T_baselink_zpr)





    def save_label(self, input_data, input_data_d, input_robot_grid_mapframe_2d,input_robot_grid_mapframe_3d, input_T_sensor_to_map, img0, img1, img2, img3, img4, T_baselink_zpr):
        #cam_matrix, _, _ = self.cam_map.voxelmap_to_matrix(origin)
        input_name = '{:06}'.format(self.number_of_prepared_data) + '_input'
        visible_name = '{:06}'.format(self.number_of_prepared_data) + '_visible'
        v = self.visible_map2d.voxel_map.get_voxels(input_robot_grid_mapframe_2d, np.zeros(
            input_robot_grid_mapframe_2d.shape[1]))
        visible_mask = np.reshape(v, input_data.shape)
        #plt.imsave(self.data_path + visible_name + '.png', visible_mask)
        v = self.roll_map2d.voxel_map.get_voxels(input_robot_grid_mapframe_2d, np.zeros(
            input_robot_grid_mapframe_2d.shape[1]))
        roll = np.reshape(v, input_data.shape)
        v = self.pitch_map2d.voxel_map.get_voxels(input_robot_grid_mapframe_2d, np.zeros(
            input_robot_grid_mapframe_2d.shape[1]))
        pitch = np.reshape(v, input_data.shape)
        v = self.yaw_map2d.voxel_map.get_voxels(input_robot_grid_mapframe_2d, np.zeros(
            input_robot_grid_mapframe_2d.shape[1]))
        yaw = np.reshape(v, input_data.shape)
        v = self.z_map2d.voxel_map.get_voxels(input_robot_grid_mapframe_2d, np.zeros(
            input_robot_grid_mapframe_2d.shape[1]))
        z_map = np.reshape(v, input_data.shape)

        features_map = []
        for j in range(0,150):
            v = self.features_map[j].voxel_map.get_voxels(input_robot_grid_mapframe_2d, np.zeros(
                input_robot_grid_mapframe_2d.shape[1]))
            features_map.append(np.reshape(v, input_data.shape))
        features_map = np.asarray(features_map)

        transform = msgify(Transform, input_T_sensor_to_map)
        quat = transform.rotation
        trans = transform.translation

        z_baselink = z_map - trans.z

        _, _, actual_yaw = efq((quat.x, quat.y, quat.z, quat.w))
        yaw[np.isnan(yaw)==False] -= actual_yaw

        v = self.input_map3d.voxel_map.get_voxels(input_robot_grid_mapframe_3d, np.zeros(input_robot_grid_mapframe_3d.shape[1]))
        input_3d = np.reshape(v,(256,256,30))
        input_3d[np.isnan(input_3d)] = 0
        input_3d[input_3d<0] = -1
        input_3d[input_3d>0] = 1



        np.savez_compressed(self.data_path + input_name, input=input_data,input_d=input_data_d, visible_mask=visible_mask, coords_mapframe=input_robot_grid_mapframe_2d,
                            sensor_to_map=input_T_sensor_to_map, pitch=pitch, roll=roll, yaw=yaw, z_map=z_map,z_baselink=z_baselink,input_3d = input_3d,  coords_mapframe_3d = (input_robot_grid_mapframe_3d*10).astype(np.int32), img0= img0, img1= img1, img2= img2, img3= img3, img4= img4, T_baselink_zrp=T_baselink_zpr,hardness=features_map)
        #plt.imsave(self.data_path + input_name + '.png', input_data)
        #plt.imsave(self.data_path +'tree'+input_name + '_f.png', features_map==4)
        #plt.imsave(self.data_path +input_name + '_f.png', features_map)



        if(self.new_data-self.input_label_shift>=0):
            inp_label_name = '{:06}'.format(self.number_of_prepared_data-self.input_label_shift) + '_input'
            label_name = '{:06}'.format(self.number_of_prepared_data-self.input_label_shift) + '_label'

            inp_label = np.load(self.data_path+inp_label_name+'.npz')

            robot_grid_mapframe_2d = inp_label['coords_mapframe']
            robot_grid_mapframe_3d = inp_label['coords_mapframe_3d'].astype(np.double)/10

            T_sensor_to_map = inp_label['sensor_to_map']
            input = inp_label['input']
            input_d = inp_label['input_d']
            input_3d = inp_label['input_3d']
            img0_in = inp_label['img0']
            img1_in = inp_label['img1']
            img2_in = inp_label['img2']
            img3_in = inp_label['img3']
            img4_in = inp_label['img4']
            in_T_baselink_zrp = inp_label['T_baselink_zrp']
            in_features_map = inp_label['hardness']

            visible_mask = inp_label['visible_mask']
            roll = inp_label['roll']
            pitch = inp_label['pitch']
            yaw = inp_label['yaw']
            v = self.roll_map2d.voxel_map.get_voxels(robot_grid_mapframe_2d, np.zeros(
                robot_grid_mapframe_2d.shape[1]))
            roll_label = np.reshape(v, input_data.shape)
            v = self.pitch_map2d.voxel_map.get_voxels(robot_grid_mapframe_2d, np.zeros(
                robot_grid_mapframe_2d.shape[1]))
            pitch_label = np.reshape(v, input_data.shape)
            v = self.yaw_map2d.voxel_map.get_voxels(robot_grid_mapframe_2d, np.zeros(
                robot_grid_mapframe_2d.shape[1]))
            yaw_label = np.reshape(v, input_data.shape)
            v = self.z_map2d.voxel_map.get_voxels(robot_grid_mapframe_2d, np.zeros(
                input_robot_grid_mapframe_2d.shape[1]))
            z_map = np.reshape(v, input_data.shape)
            transform = msgify(Transform, T_sensor_to_map)

            v = self.flipper_fl_map2d.voxel_map.get_voxels(robot_grid_mapframe_2d, np.zeros(
                input_robot_grid_mapframe_2d.shape[1]))
            flipper_fl = np.reshape(v, input_data.shape)
            v = self.flipper_fr_map2d.voxel_map.get_voxels(robot_grid_mapframe_2d, np.zeros(
                input_robot_grid_mapframe_2d.shape[1]))
            flipper_fr = np.reshape(v, input_data.shape)
            v = self.flipper_rl_map2d.voxel_map.get_voxels(robot_grid_mapframe_2d, np.zeros(
                input_robot_grid_mapframe_2d.shape[1]))
            flipper_rl = np.reshape(v, input_data.shape)
            v = self.flipper_rr_map2d.voxel_map.get_voxels(robot_grid_mapframe_2d, np.zeros(
                input_robot_grid_mapframe_2d.shape[1]))
            flipper_rr = np.reshape(v, input_data.shape)


            quat = transform.rotation
            trans = transform.translation
            z_baselink = z_map - trans.z

            _, _, actual_yaw = efq((quat.x, quat.y, quat.z, quat.w))
            yaw_label[np.isnan(yaw_label) == False] -= actual_yaw

            v = self.input_map2d.voxel_map.get_voxels(robot_grid_mapframe_2d, np.zeros(
                robot_grid_mapframe_2d.shape[1])) - transform.translation.z
            label = np.reshape(v, input.shape)

            label[~np.isnan(input)] = input[~np.isnan(input)]

            v = self.input_map3d.voxel_map.get_voxels(robot_grid_mapframe_3d,
                                                      np.zeros(robot_grid_mapframe_3d.shape[1]))
            label_3d = np.reshape(v, (256, 256, 30))
            label_3d[np.isnan(label_3d)] = 0
            label_3d[label_3d < 0] = -1
            label_3d[label_3d > 0] = 1

            #self.render_class.render(self.input_map3d, output, cam_0, cam_1, cam_2, cam_3, cam_4, robot_grid_mapframe_2d,
            #                         get_robot_grid, pcd_to_map, self.tf, grid_size=robot_grid[0].shape[0])


            v = self.input_map2d_d.voxel_map.get_voxels(robot_grid_mapframe_2d, np.zeros(
                robot_grid_mapframe_2d.shape[1])) - transform.translation.z
            label_d = np.reshape(v, input.shape)



            np.savez_compressed(self.data_path + label_name,
                                input=input.astype(np.float16),
                                input_d=input_d.astype(np.float16),
                                label=label.astype(np.float16),
                                label_d=label_d.astype(np.float16),
                                visible_mask=visible_mask.astype(np.float16),
                                coords_mapframe=robot_grid_mapframe_2d.astype(np.float16),
                                sensor_to_map=T_sensor_to_map.astype(np.float16),
                                pitch=pitch.astype(np.float16),
                                roll=roll.astype(np.float16),
                                yaw=yaw.astype(np.float16),
                                pitch_label=pitch_label.astype(np.float16),
                                roll_label=roll_label.astype(np.float16),
                                yaw_label=yaw_label.astype(np.float16),
                                z_map=inp_label['z_map'].astype(np.float16),
                                z_baselink=inp_label['z_baselink'].astype(np.float16),
                                z_map_label=z_map.astype(np.float16),
                                z_baselink_label=z_baselink.astype(np.float16),
                                input_3d = input_3d.astype(np.float16),
                                coords_mapframe_3d = (input_robot_grid_mapframe_3d*10).astype(np.int32),
                                label_3d = label_3d.astype(np.float16),
                                img0= img0_in,
                                img1= img1_in,
                                img2= img2_in,
                                img3= img3_in,
                                img4= img4_in,
                                T_baselink_zpr = in_T_baselink_zrp.astype(np.float16),
                                hardness=in_features_map.astype(np.float16),
                                flipper_fl=flipper_fl.astype(np.float16),
                                flipper_fr=flipper_fr.astype(np.float16),
                                flipper_rl=flipper_rl.astype(np.float16),
                                flipper_rr=flipper_rr.astype(np.float16))
            #plt.imsave(self.data_path + label_name + '.png', label)
            plt.imsave(self.data_path + label_name + '.png', np.hstack([inp_label['input'], label,np.argmax(inp_label['hardness']/150,axis=0)]))

        self.number_of_prepared_data += 1
        self.new_data += 1

if __name__ == '__main__':
    s2d = DEM_sparse2dense()
    rospy.spin()