from image_geometry import PinholeCameraModel
import numpy as np
import cv2
import torch
import torch.nn as nn
import time
from PIL import Image
from torchvision.transforms import Compose, CenterCrop, Normalize, Resize
from torchvision.transforms import ToTensor, ToPILImage
from ERFNet import load_erfnet_model
from OccupancyMap import OccupancyMap, filter_pc
from mit_semseg.models import ModelBuilder, SegmentationModule
import yaml
import munch
import matplotlib.pyplot as plt


class Render():
    def __init__(self, map_frame='map', grid_resolution=0.1, net='resnet50dilated'):
        self.map_frame = map_frame
        self.grid_resolution = grid_resolution

        if torch.cuda.is_available():
            self.device = torch.device('cuda')
        else:
            self.device = torch.device('cpu')
        self.voxel_map = OccupancyMap(self.map_frame, self.grid_resolution)

        self.camera_model = {}
        self.camera_transforms = []
        self.camera_transforms_inv = []
        self.camera_masks = []
        for i in range(5):
            # create camera models from yaml files
            #with open("/home/honza/Desktop/testing/cam_" + str(i) + ".yaml", "r") as file_handle:
            with open("/home/salanvoj/PycharmProjects/detector_ws/src/dem_predictor/data/camera_info/cam_" + str(i) + ".yaml", "r") as file_handle:
                calib_data = yaml.load(file_handle)
            msg = munch.munchify(calib_data)
            self.camera_model[msg.header.frame_id] = PinholeCameraModel()
            self.camera_model[msg.header.frame_id].fromCameraInfo(msg)
            #self.camera_transforms.append(np.load("/home/honza/Desktop/testing/camera_tf_" + str(i) + ".npy"))
            self.camera_transforms.append(np.load("/home/salanvoj/PycharmProjects/detector_ws/src/dem_predictor/data/camera_info/camera_tf_" + str(i) + ".npy"))
            self.camera_transforms_inv.append(np.linalg.inv(self.camera_transforms[i]))

            im_mask = cv2.imread("/home/salanvoj/PycharmProjects/detector_ws/src/dem_predictor/data/robot_masks/camera_" + str(i) + "_mask.png",
                                 cv2.IMREAD_GRAYSCALE)
            self.camera_masks.append(im_mask)
            # self.camera_transforms_inv.append(np.load("/home/honza/Desktop/testing/camera_inv_tf_" + str(i) + ".npy"))
        if net == 'erfnet':
            self.segmentation_model = load_erfnet_model()
            self.get_features = self.get_features_erfnet
        elif net == 'resnet50dilated':
            self.segmentation_model = load_resnet_model()
            self.get_features = self.get_features_resnet
        else:
            raise ValueError('Wrong specification of segmentation network')
        self.tf = None
        self.tracing_map = None

    def get_features_resnet(self, img, dim_reduction=2):
        input_transform = Compose([
            Resize((1616 / (dim_reduction * 2), 1232 / (dim_reduction * 2)), Image.BILINEAR),
            ToTensor(),
            Normalize([.485, .456, .406], [.229, .224, .225]),
        ])
        #image_np = cv2.imdecode(img, cv2.IMREAD_COLOR)
        #image_np = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
        #plt.imshow(img)
        image_np = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        im_pil = Image.fromarray(image_np).convert("RGB")
        img = input_transform(im_pil).unsqueeze(0)
        img = img.to(self.device)

        with torch.no_grad():
            feed_dict = {}
            feed_dict['img_data'] = img
            segSize = (img.shape[2], img.shape[3])
            pred_tmp = self.segmentation_model(feed_dict, segSize=segSize)
        #_, pred = torch.max(pred_tmp, dim=1)
        pred = pred_tmp.detach().cpu().numpy()
        #pred = pred.transpose((1, 2, 0))
        pred = pred[0].transpose((1, 2, 0))
        return pred
        #return class_mapping(pred)

    def get_features_erfnet(self, img, dim_reduction=2):
        input_transform = Compose([
            Resize((1616/dim_reduction, 1232/dim_reduction), Image.BILINEAR),
            ToTensor(),
            # Normalize([.485, .456, .406], [.229, .224, .225]),
        ])

        np_arr = np.fromstring(img, np.uint8)
        image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
        image_np = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
        im_pil = Image.fromarray(image_np).convert("RGB")
        im_pil = input_transform(im_pil).unsqueeze(0)
        im_pil = im_pil.to(self.device)
        features = self.segmentation_model(im_pil)
        features = features.detach().cpu().numpy().transpose(0, 2, 3, 1)
        return features.reshape(features.shape[1], features.shape[2], features.shape[3])

    def transform_to_cam(self, cam_tf, T):
        voxels, _, _ = self.tracing_map.voxel_map.get_voxels()
        voxels = np.asarray(voxels)
        voxels = np.concatenate([voxels, np.ones(voxels.shape[1])[np.newaxis]])

        voxels_cam_frame = np.matmul(T, voxels)
        voxels_cam_frame = np.matmul(cam_tf, voxels_cam_frame)
        # omit voxels behind camera
        voxels_cam_frame = voxels_cam_frame[:3, voxels_cam_frame[2] > 0]    # z-coord is negative for such voxels

        self.voxel_map.voxel_map.set_voxels(voxels_cam_frame, np.zeros_like(voxels_cam_frame[1]), np.ones_like(voxels_cam_frame[1]))
        voxels_cam_frame, _, _ = self.voxel_map.voxel_map.get_voxels()
        self.voxel_map.voxel_map.clear()

        return voxels_cam_frame

    def render(self, output, cam_0, cam_1, cam_2, cam_3, cam_4, T, dim_reduction=2, grid_size=256, mode='2d'):
        if mode == '2d':
            return self.render_2d(output, cam_0, cam_1, cam_2, cam_3, cam_4, T, dim_reduction=dim_reduction, grid_size=grid_size)
        elif mode == '3d':
            return self.render_3d(output, cam_0, cam_1, cam_2, cam_3, cam_4, T, dim_reduction=dim_reduction, grid_size=grid_size)
        else:
            raise ValueError("Wrong mode specified. Use '2d' or '3d'.")

    def render_2d(self, output, cam_0, cam_1, cam_2, cam_3, cam_4, T, dim_reduction=2, grid_size=256):
        # handle different shape for later indexing
        if output.shape == (256, 256, 1):
            output = output[:, :, 0]
        # grid in robot frame
        robot_grid = np.meshgrid(np.arange(-12.75, 12.85, self.grid_resolution),
                                 np.arange(-12.75, 12.85, self.grid_resolution))
        get_robot_grid = np.asarray([robot_grid[0].ravel(), robot_grid[1].ravel(), np.ones_like(robot_grid[1].ravel()) * self.grid_resolution / 2])
        robot_grid_array = get_robot_grid[:, ~np.isnan(output).ravel()]
        output_reduced = output[~np.isnan(output)].ravel()

        output_map = OccupancyMap(self.map_frame, self.grid_resolution)
        output_map.voxel_map.set_voxels(robot_grid_array, np.zeros_like(output_reduced, dtype=np.float64),
                                          output_reduced-self.grid_resolution)

        # convert output to 3d map
        self.tracing_map = OccupancyMap(self.map_frame, self.grid_resolution)
        self.tracing_map = output_map.to_3d_dem(self.tracing_map)

        voxels_cam_frame_0 = self.transform_to_cam(self.camera_transforms[0], T)
        voxels_cam_frame_1 = self.transform_to_cam(self.camera_transforms[1], T)
        voxels_cam_frame_2 = self.transform_to_cam(self.camera_transforms[2], T)
        voxels_cam_frame_3 = self.transform_to_cam(self.camera_transforms[3], T)
        voxels_cam_frame_4 = self.transform_to_cam(self.camera_transforms[4], T)

        print("Getting image features ...")
        features_0 = self.get_features(cam_0, dim_reduction=dim_reduction)
        #features_1 = self.get_features(cam_1, dim_reduction=dim_reduction)
        #features_2 = self.get_features(cam_2, dim_reduction=dim_reduction)
        #features_3 = self.get_features(cam_3, dim_reduction=dim_reduction)
        features_4 = self.get_features(cam_4, dim_reduction=dim_reduction)

        z_buffer_0 = Zbuffer(features_0, self.camera_masks[0], dim_reduction=dim_reduction)
        # z_buffer_1 = Zbuffer(features_1, self.camera_masks[1], dim_reduction=dim_reduction)
        # z_buffer_2 = Zbuffer(features_2, self.camera_masks[2], dim_reduction=dim_reduction)
        # z_buffer_3 = Zbuffer(features_3, self.camera_masks[3], dim_reduction=dim_reduction)
        z_buffer_4 = Zbuffer(features_4, self.camera_masks[4], dim_reduction=dim_reduction)


        print("Find frontiers ...")

        frontiers_voxels0, features0 = z_buffer_0.projection(voxels_cam_frame_0,
                                                             self.camera_model['camera_0'],
                                                             self.grid_resolution, dim_reduction=dim_reduction)
        #frontiers_voxels1, features1 = z_buffer_1.projection(voxels_cam_frame_1,
        #                                                     self.camera_model['camera_1'],
        #                                                     self.grid_resolution, dim_reduction=dim_reduction)
        #frontiers_voxels2, features2 = z_buffer_2.projection(voxels_cam_frame_2,
        #                                                     self.camera_model['camera_2'],
        #                                                     self.grid_resolution, dim_reduction=dim_reduction)
        #frontiers_voxels3, features3 = z_buffer_3.projection(voxels_cam_frame_3,
        #                                                     self.camera_model['camera_3'],
        #                                                     self.grid_resolution, dim_reduction=dim_reduction)
        frontiers_voxels4, features4 = z_buffer_4.projection(voxels_cam_frame_4,
                                                             self.camera_model['camera_4'],
                                                             self.grid_resolution, dim_reduction=dim_reduction)

        frontiers_voxels0 = np.concatenate([frontiers_voxels0, np.ones(frontiers_voxels0.shape[1])[np.newaxis]])
        frontiers_map_frame0 = np.matmul(self.camera_transforms_inv[0], frontiers_voxels0)

        #frontiers_voxels1 = np.concatenate([frontiers_voxels1, np.ones(frontiers_voxels1.shape[1])[np.newaxis]])
        #frontiers_map_frame1 = np.matmul(self.camera_transforms_inv[1], frontiers_voxels1)

        #frontiers_voxels2 = np.concatenate([frontiers_voxels2, np.ones(frontiers_voxels2.shape[1])[np.newaxis]])
        #frontiers_map_frame2 = np.matmul(self.camera_transforms_inv[2], frontiers_voxels2)

        #frontiers_voxels3 = np.concatenate([frontiers_voxels3, np.ones(frontiers_voxels3.shape[1])[np.newaxis]])
        #frontiers_map_frame3 = np.matmul(self.camera_transforms_inv[3], frontiers_voxels3)

        frontiers_voxels4 = np.concatenate([frontiers_voxels4, np.ones(frontiers_voxels4.shape[1])[np.newaxis]])
        frontiers_map_frame4 = np.matmul(self.camera_transforms_inv[4], frontiers_voxels4)

        #frontiers_map_frame = np.hstack((frontiers_map_frame0, frontiers_map_frame1, frontiers_map_frame2,
        #                                 frontiers_map_frame3, frontiers_map_frame4))
        #frontiers_map_frame = np.hstack((frontiers_map_frame0, frontiers_map_frame1,
        #                                 frontiers_map_frame3, frontiers_map_frame4))
        frontiers_map_frame = np.hstack((frontiers_map_frame0, frontiers_map_frame4))
        #features = np.vstack((features0, features1, features2, features3, features4))
        #features = np.vstack((features0, features1, features3, features4))
        features = np.vstack((features0, features4))

        frontiers_map = OccupancyMap(self.map_frame, self.grid_resolution)
        # set voxels with features
        # frontiers_map.voxel_map.set_voxels(frontiers_map_frame[:3, :], np.zeros_like(frontiers_map_frame[0], dtype=np.float64), features)
        h_xy = np.concatenate([frontiers_map_frame[0:2, :], np.ones(frontiers_map_frame.shape[1])[np.newaxis] * self.grid_resolution / 2])
        #h_xy = frontiers_map_frame[0:3, :]
        new_h_xy = []
        new_features = []
        eval_h_xy = np.zeros(h_xy.shape[1])
        for i in range(h_xy.shape[1]):
            if (eval_h_xy[i]==0):
                new_features.append(np.max(features[np.sum(np.equal(h_xy, np.expand_dims(h_xy[:, i], axis=1)), axis=0) == 3],axis=0))
                new_h_xy.append(h_xy[:, i])
                eval_h_xy[np.sum(np.equal(h_xy, np.expand_dims(h_xy[:, i], axis=1)), axis=0) == 3] = 1

        new_h_xy = np.asarray(new_h_xy).T
        new_features = np.asarray(new_features)

        # set features to voxel map
        for i in range(len(new_features[0])):
            frontiers_map.voxel_map.set_voxels(new_h_xy, np.ones((new_features.shape[0], 1)) * i,
                                                   new_features[:, i])
        # make grid with corresponding features
        for i in range(len(new_features[0])):
            v = frontiers_map.voxel_map.get_voxels(get_robot_grid, np.ones(grid_size * grid_size) * i)
            grid = np.reshape(v, (1, grid_size, grid_size))
            if i ==0:
                output_features = grid
            else:
                output_features = np.concatenate((output_features, grid))

        return output_features

    def render_3d(self, output, cam_0, cam_1, cam_2, cam_3, cam_4, T, dim_reduction=2, grid_size=256):
        # robot 3d grid
        robot_grid_3d = np.meshgrid(np.arange(-12.75, 12.85, 0.1), np.arange(-12.75, 12.85, 0.1),
                                    np.arange(-0.95, 1.05, 0.1))
        robot_grid_3d = np.asarray(robot_grid_3d)
        get_robot_grid_3d = np.asarray([robot_grid_3d[0].ravel(), robot_grid_3d[1].ravel(), robot_grid_3d[2].ravel()])
        robot_grid_3d_pcd = robot_grid_3d[:, output == 1]
        robot_grid_3d_array = np.asarray([robot_grid_3d_pcd[0].ravel(), robot_grid_3d_pcd[1].ravel(), robot_grid_3d_pcd[2].ravel()])

        self.tracing_map = OccupancyMap(self.map_frame, self.grid_resolution)
        self.tracing_map.voxel_map.set_voxels(robot_grid_3d_array, np.zeros_like(robot_grid_3d_array[0]), np.ones_like(robot_grid_3d_array[0]))

        voxels_cam_frame_0 = self.transform_to_cam(self.camera_transforms[0], T)
        voxels_cam_frame_1 = self.transform_to_cam(self.camera_transforms[1], T)
        voxels_cam_frame_2 = self.transform_to_cam(self.camera_transforms[2], T)
        voxels_cam_frame_3 = self.transform_to_cam(self.camera_transforms[3], T)
        voxels_cam_frame_4 = self.transform_to_cam(self.camera_transforms[4], T)

        print("Getting image features ...")
        features_0 = self.get_features(cam_0, dim_reduction=dim_reduction)
        features_1 = self.get_features(cam_1, dim_reduction=dim_reduction)
        features_2 = self.get_features(cam_2, dim_reduction=dim_reduction)
        features_3 = self.get_features(cam_3, dim_reduction=dim_reduction)
        features_4 = self.get_features(cam_4, dim_reduction=dim_reduction)

        z_buffer_0 = Zbuffer(features_0, self.camera_masks[0], dim_reduction=dim_reduction)
        z_buffer_1 = Zbuffer(features_1, self.camera_masks[1], dim_reduction=dim_reduction)
        z_buffer_2 = Zbuffer(features_2, self.camera_masks[2], dim_reduction=dim_reduction)
        z_buffer_3 = Zbuffer(features_3, self.camera_masks[3], dim_reduction=dim_reduction)
        z_buffer_4 = Zbuffer(features_4, self.camera_masks[4], dim_reduction=dim_reduction)

        print("Find frontiers ...")

        frontiers_voxels0, features0 = z_buffer_0.projection(voxels_cam_frame_0,
                                                             self.camera_model['camera_0'],
                                                             self.grid_resolution, dim_reduction=dim_reduction)
        frontiers_voxels1, features1 = z_buffer_1.projection(voxels_cam_frame_1,
                                                             self.camera_model['camera_1'],
                                                             self.grid_resolution, dim_reduction=dim_reduction)
        frontiers_voxels2, features2 = z_buffer_2.projection(voxels_cam_frame_2,
                                                             self.camera_model['camera_2'],
                                                             self.grid_resolution, dim_reduction=dim_reduction)
        frontiers_voxels3, features3 = z_buffer_3.projection(voxels_cam_frame_3,
                                                             self.camera_model['camera_3'],
                                                             self.grid_resolution, dim_reduction=dim_reduction)
        frontiers_voxels4, features4 = z_buffer_4.projection(voxels_cam_frame_4,
                                                             self.camera_model['camera_4'],
                                                             self.grid_resolution, dim_reduction=dim_reduction)

        frontiers_voxels0 = np.concatenate([frontiers_voxels0, np.ones(frontiers_voxels0.shape[1])[np.newaxis]])
        frontiers_map_frame0 = np.matmul(self.camera_transforms_inv[0], frontiers_voxels0)

        frontiers_voxels1 = np.concatenate([frontiers_voxels1, np.ones(frontiers_voxels1.shape[1])[np.newaxis]])
        frontiers_map_frame1 = np.matmul(self.camera_transforms_inv[1], frontiers_voxels1)

        frontiers_voxels2 = np.concatenate([frontiers_voxels2, np.ones(frontiers_voxels2.shape[1])[np.newaxis]])
        frontiers_map_frame2 = np.matmul(self.camera_transforms_inv[2], frontiers_voxels2)

        frontiers_voxels3 = np.concatenate([frontiers_voxels3, np.ones(frontiers_voxels3.shape[1])[np.newaxis]])
        frontiers_map_frame3 = np.matmul(self.camera_transforms_inv[3], frontiers_voxels3)

        frontiers_voxels4 = np.concatenate([frontiers_voxels4, np.ones(frontiers_voxels4.shape[1])[np.newaxis]])
        frontiers_map_frame4 = np.matmul(self.camera_transforms_inv[4], frontiers_voxels4)

        # frontiers_map_frame = frontiers_map_frame0
        frontiers_map_frame = np.hstack((frontiers_map_frame0, frontiers_map_frame1, frontiers_map_frame2,
                                         frontiers_map_frame3, frontiers_map_frame4))
        # features = features0
        features = np.vstack((features0, features1, features2, features3, features4))
        frontiers_map = OccupancyMap(self.map_frame, self.grid_resolution)

        # set voxels with features
        frontiers_map.voxel_map.set_voxels(frontiers_map_frame[0:3], np.zeros_like(frontiers_map_frame[0]), features.ravel() + 1)
        v = frontiers_map.voxel_map.get_voxels(get_robot_grid_3d, np.zeros(grid_size * grid_size * 20))
        output_features = np.reshape(v, (256, 256, 20))
        output_features[np.isnan(output_features)] = 0
        output_features[output == -1] = -1

        # output tensor:   -1 = no voxel, but measured, as in input tensor
        #                   0 = no voxel measured
        #                   1 = first class, but in our case irrelevant
        #                 2-6 = classes sorted by toughness
        return output_features


class Zbuffer():
    def __init__(self, features, mask, dim_reduction=2):
        self.width = 1232/(2*dim_reduction)
        self.height = 1616/(2*dim_reduction)
        self.depths = np.ones((1616/(2*dim_reduction), 1232/(2*dim_reduction))) * 100
        self.pcd = np.zeros((3, 1616/(2*dim_reduction), 1232/(2*dim_reduction)))
        # self.features = np.ones((1616/(2*dim_reduction), 1232/(2*dim_reduction))) * 255*256*256     # just init red color for DEBUG
        self.features = features        # ERFNet features
        self.mask = cv2.resize(mask, dsize=(1232 / (2 * dim_reduction), 1616 / (2 * dim_reduction)))

    def projection(self, voxels, camera_model, resolution, dim_reduction=2):
        start = time.time()
        fx = camera_model.fx()
        fy = camera_model.fy()
        Tx = camera_model.Tx()
        Ty = camera_model.Ty()
        cx = camera_model.cx()
        cy = camera_model.cy()

        u0_arr = np.round(
            ((fx * (voxels[0, :] - resolution / 2) + Tx) / voxels[2, :] + cx) / (
                        2 * dim_reduction)).astype(np.int)
        v0_arr = np.round(
            ((fy * (voxels[1, :] - resolution / 2) + Ty) / voxels[2, :] + cy) / (
                        2 * dim_reduction)).astype(np.int)

        u2_arr = np.round(
            ((fx * (voxels[0, :] + resolution / 2) + Tx) / voxels[2, :] + cx) / (
                        2 * dim_reduction)).astype(np.int)
        v1_arr = np.round(
            ((fy * (voxels[1, :] + resolution / 2) + Ty) / voxels[2, :] + cy) / (
                        2 * dim_reduction)).astype(np.int)

        # pick only valid pixels (in image plane)
        condition_indices = np.logical_not(
            np.logical_or(np.logical_and(u0_arr < 0, u2_arr < 0), np.logical_and(v0_arr < 0, v1_arr < 0),
                          np.logical_or(np.logical_and(u0_arr >= self.width, u2_arr >= self.width),
                                        np.logical_and(v0_arr >= self.height, v1_arr >= self.height))))
        u_arr0 = u0_arr[condition_indices]
        u_arr2 = u2_arr[condition_indices]
        v_arr0 = v0_arr[condition_indices]
        v_arr1 = v1_arr[condition_indices]
        z_arr = voxels[2, :][condition_indices]
        pcd_arr = voxels[:, condition_indices]

        # np.save("/home/honza/Desktop/pcd.npy", pcd_arr)

        # correction of indices
        u_arr0[u_arr0 < 0] = 0
        u_arr2[u_arr2 < 0] = 0
        v_arr0[v_arr0 < 0] = 0
        v_arr1[v_arr1 < 0] = 0
        u_arr0[u_arr0 >= self.width] = self.width - 1
        u_arr2[u_arr2 >= self.width] = self.width - 1
        v_arr0[v_arr0 >= self.height] = self.height - 1
        v_arr1[v_arr1 >= self.height] = self.height - 1

        # Z-buffering
        start_buffer = time.time()

        for i in range(len(u_arr0)):
            u0 = u_arr0[i]
            u2 = u_arr2[i]
            v0 = v_arr0[i]
            v1 = v_arr1[i]
            for ii in range(u0, u2 + 1):  # for ii in range(min(u0, u2), max(u0, u2)+1):
                for jj in range(v0, v1 + 1):  # for jj in range(min(v0, v1), max(v0, v1)+1):
                    if self.depths[jj, ii] > z_arr[i] and self.mask[jj, ii] != 0:
                        self.depths[jj, ii] = z_arr[i]
                        self.pcd[:, jj, ii] = pcd_arr[:, i]

        print("Z buffering through reduced PCD took: " + str(time.time() - start_buffer))

        print("Projection to pixels took: " + str(time.time() - start))
        frontiers_voxels = self.pcd[:, self.depths < 100]
        features = self.features[self.depths < 100]

        return frontiers_voxels, features


def load_resnet_model():
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')
    # Network Builders
    net_encoder = ModelBuilder.build_encoder(
        arch="resnet50dilated",
        fc_dim=2048,
        #weights="/home/honza/trav_project/semantic-segmentation-pytorch/ade20k-resnet50dilated-ppm_deepsup/encoder_epoch_20.pth")
        weights = "/home/salanvoj/PycharmProjects/semantic-segmentation-pytorch/ade20k-resnet50dilated-ppm_deepsup/encoder_epoch_20.pth")

    net_decoder = ModelBuilder.build_decoder(
        arch="ppm_deepsup",
        fc_dim=2048,
        num_class=150,
        #weights="/home/honza/trav_project/semantic-segmentation-pytorch/ade20k-resnet50dilated-ppm_deepsup/decoder_epoch_20.pth",use_softmax=True)
        weights="/home/salanvoj/PycharmProjects/semantic-segmentation-pytorch/ade20k-resnet50dilated-ppm_deepsup/decoder_epoch_20.pth",
        use_softmax=True)


    crit = nn.NLLLoss(ignore_index=-1)

    segmentation_module = SegmentationModule(net_encoder, net_decoder, crit)
    segmentation_module.to(device)
    segmentation_module.eval()

    return segmentation_module


def class_mapping(input):
    c = input.copy()
    # our class 0 = irrelevant
    condition_0 = (c == 2) | (c == 5) | (c == 7) | (c == 8) | (c == 18) | (c == 23) | (c == 24) | (c == 30) | (c == 31) | (c == 37) | (c == 78) | (c == 81) | (c == 82) | (c == 85) | (c == 86) | (c == 92) | (c == 108) | (c == 134) | (c == 137)
    input[condition_0] = 0

    # class 1 = very soft (water)
    condition_1 = (c == 21) | (c == 26) | (c == 60) | (c == 109) |(c == 113) | (c == 128)
    input[condition_1] = 1

    # class 2 = soft (grass, plants)
    condition_2 = (c == 9) | (c == 17) | (c == 39) | (c == 57) | (c == 66)
    input[condition_2] = 2

    # class 3 = medium (sand)
    condition_3 = (c == 29) | (c == 46) | (c == 91) | (c == 94) | (c == 125)
    input[condition_3] = 3

    # class 4 = hard (trees)
    condition_4 = (c == 4) | (c == 72) | (c == 114) | (c == 115) | (c == 126) | (c == 131) | (c == 149)
    input[condition_4] = 4

    # class 5 = very hard (walls, buildings)
    condition_5 = ~ (condition_0 | condition_1 | condition_2 | condition_3 | condition_4)
    # condition_5 = ~ (condition_0 | condition_1 | condition_2 | condition_3 | condition_4 | (c == -1))     # !!! if there is something unlabelled as -1 in labelling (only for future training)
    input[condition_5] = 5
    return input
