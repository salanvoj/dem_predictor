import numpy as np
import matplotlib.pyplot as plt
import itertools
from network_s2d import Net
import torch
from voxel_map import VoxelMap
import time

from mpl_toolkits.mplot3d import Axes3D


def support_plane(dem):
    # size of robot
    length = 1.
    width = 0.6
    grid_res = 0.1

    robot_grid = np.meshgrid(np.arange(-length/2, length/2+grid_res, grid_res), np.arange(-width/2, width/2+grid_res, grid_res))

    dem_grid = np.meshgrid(np.arange(-dem.shape[0] * 0.1 / 2 + grid_res / 2, dem.shape[0] * 0.1 / 2 + grid_res / 2, grid_res),np.arange(-dem.shape[1] * 0.1 / 2 + grid_res / 2, dem.shape[1] * 0.1 / 2 + grid_res / 2, grid_res))
    voxel_map = VoxelMap(grid_res, -1.0, 1.0, 0.0)

    notnan_mask = ~np.isnan(dem)

    voxel_map.set_voxels(np.array([dem_grid[0][notnan_mask], dem_grid[1][notnan_mask], np.ones_like(dem_grid[0][notnan_mask]) * grid_res / 2]),
                         np.zeros_like(dem_grid[0][notnan_mask], dtype=np.float64)[np.newaxis],
                         np.array(dem[notnan_mask])[np.newaxis])

    #TODO:yaw

    LTB = np.zeros_like(robot_grid[0],dtype=int)
    LTB[0:2, 0:5] = 1
    LTB_indices = np.where(LTB == 1)
    LTF = np.zeros_like(robot_grid[0],dtype=int)
    LTF[0:2, 5:] = 1
    LTF_indices = np.where(LTF == 1)
    RTB = np.zeros_like(robot_grid[0],dtype=int)
    RTB[5:, 0:5] = 1
    RTB_indices = np.where(RTB == 1)
    RTF = np.zeros_like(robot_grid[0],dtype=int)
    RTF[5:, 5:] = 1
    RTF_indices = np.where(RTF == 1)

    anti_padding = 100
    for i in range(anti_padding, dem.shape[0]-anti_padding):
        for j in range(anti_padding, dem.shape[1]-anti_padding):
            robot_grid_to_get = np.concatenate([robot_grid[0].ravel()[np.newaxis]+dem_grid[0][i,j], robot_grid[1].ravel()[np.newaxis]+dem_grid[1][i,j],
                                                np.ones_like(robot_grid[0].ravel()[np.newaxis]) * grid_res / 2], 0)

            v = voxel_map.get_voxels(robot_grid_to_get, np.zeros_like(robot_grid[0].ravel()[np.newaxis]))
            under_robot_dem = np.reshape(v, robot_grid[0].shape)

            RTB_argmax = np.argmax(under_robot_dem[RTB == 1])
            RTB_x = robot_grid[0][RTB_indices[0][RTB_argmax]][RTB_indices[1][RTB_argmax]]
            RTB_y = robot_grid[1][RTB_indices[0][RTB_argmax]][RTB_indices[1][RTB_argmax]]
            RTB_z = np.max(under_robot_dem[RTB==1])

            RTB_x = robot_grid[0][RTB_indices[0],RTB_indices[1]]
            RTB_y = robot_grid[1][RTB_indices[0],RTB_indices[1]]
            RTB_z = (under_robot_dem[RTB == 1])

            RTF_argmax = np.argmax(under_robot_dem[RTF==1])
            RTF_x = robot_grid[0][RTF_indices[0][RTF_argmax]][RTF_indices[1][RTF_argmax]]
            RTF_y = robot_grid[1][RTF_indices[0][RTF_argmax]][RTF_indices[1][RTF_argmax]]
            RTF_z = np.max(under_robot_dem[RTF==1])

            RTF_x = robot_grid[0][RTF_indices[0],RTF_indices[1]]
            RTF_y = robot_grid[1][RTF_indices[0],RTF_indices[1]]
            RTF_z = (under_robot_dem[RTF == 1])

            LTB_argmax = np.argmax(under_robot_dem[LTB==1])
            LTB_x = robot_grid[0][LTB_indices[0][LTB_argmax]][LTB_indices[1][LTB_argmax]]
            LTB_y = robot_grid[1][LTB_indices[0][LTB_argmax]][LTB_indices[1][LTB_argmax]]
            LTB_z = np.max(under_robot_dem[LTB==1])

            LTB_x = robot_grid[0][LTB_indices[0],LTB_indices[1]]
            LTB_y = robot_grid[1][LTB_indices[0],LTB_indices[1]]
            LTB_z = (under_robot_dem[LTB == 1])

            LTF_argmax = np.argmax(under_robot_dem[LTF==1])
            LTF_x = robot_grid[0][LTF_indices[0][LTF_argmax]][LTF_indices[1][LTF_argmax]]
            LTF_y = robot_grid[1][LTF_indices[0][LTF_argmax]][LTF_indices[1][LTF_argmax]]
            LTF_z = np.max(under_robot_dem[LTF==1])

            LTF_x = robot_grid[0][LTF_indices[0],LTF_indices[1]]
            LTF_y = robot_grid[1][LTF_indices[0],LTF_indices[1]]
            LTF_z = (under_robot_dem[LTF == 1])


            x = list(itertools.chain.from_iterable([RTF_x.tolist(),RTB_x.tolist(),LTF_x.tolist(),LTB_x.tolist()]))
            y = list(itertools.chain.from_iterable([RTF_y.tolist(),RTB_y.tolist(),LTF_y.tolist(),LTB_y.tolist()]))
            z = list(itertools.chain.from_iterable([RTF_z.tolist(),RTB_z.tolist(),LTF_z.tolist(),LTB_z.tolist()]))




            #A = np.array([[RTF_x, RTF_y, 1],[RTB_x, RTB_y, 1],[LTF_x, LTF_y, 1],[LTB_x, LTB_y, 1]])
            A = np.array([x, y, np.ones_like(x)]).T
            #b = np.array([[RTF_z],[RTB_z],[LTF_z],[LTB_z]])
            b = np.array(z)[np.newaxis].T
            plane_params = np.matmul(np.linalg.pinv(A),b)

            if i > 120:
                ax = plt.subplot(111, projection='3d')
                plt.gca().set_aspect('equal', adjustable='box')
                ax.scatter(x, y, z, color='b')

                # plot plane
                xlim = ax.get_xlim()
                ylim = ax.get_ylim()
                X, Y = np.meshgrid(np.arange(xlim[0], xlim[1]),
                                   np.arange(ylim[0], ylim[1]))
                X = robot_grid[0]
                Y = robot_grid[1]

                Z = np.zeros(X.shape)
                for r in range(X.shape[0]):
                    for c in range(X.shape[1]):
                        Z[r, c] = plane_params[0] * X[r, c] + plane_params[1] * Y[r, c] + plane_params[2]
                ax.plot_wireframe(X, Y, Z, color='k')

                ax.set_xlabel('x')
                ax.set_ylabel('y')
                ax.set_zlabel('z')

                plt.show()

    print(robot_grid)

def rigid_body_contact(dem):
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    device = torch.device("cpu")

    grid_res = 0.1
    dem_grid = np.meshgrid(
        np.arange(-dem.shape[0] * grid_res / 2 + grid_res / 2, dem.shape[0] * grid_res / 2 + grid_res / 2, grid_res),
        np.arange(-dem.shape[1] * grid_res / 2 + grid_res / 2, dem.shape[1] * grid_res / 2 + grid_res / 2, grid_res))
    voxel_map = VoxelMap(grid_res, -1.0, 1.0, 0.0)
    notnan_mask = ~np.isnan(dem)
    voxel_map.set_voxels(np.array(
        [dem_grid[0][notnan_mask], dem_grid[1][notnan_mask], np.ones_like(dem_grid[0][notnan_mask]) * grid_res / 2]),
                         np.zeros_like(dem_grid[0][notnan_mask], dtype=np.float64)[np.newaxis],
                         np.array(dem[notnan_mask])[np.newaxis])

    ax = plt.axes(projection='3d')

    ### Define rigid body (set of 3D points)
    length = 0.8
    width = 0.6
    robot_grid = np.meshgrid(np.arange(-length / 2, length / 2 + grid_res, grid_res),
                             np.arange(-width / 2, width / 2 + grid_res, grid_res))
    ### Define tracks (set of 3D points)
    LT = np.zeros_like(robot_grid[0], dtype=int)
    LT[0:2, :] = 1
    RT = np.zeros_like(robot_grid[0], dtype=int)
    RT[5:, :] = 1
    X = robot_grid[0]
    Y = robot_grid[1]
    Z = np.zeros_like(X)
    Z[(LT == 0) & (RT == 0)] += 0.1
    N = Z.size
    P = np.stack((X.reshape([1, N]).squeeze(), Y.reshape([1, N]).squeeze(), Z.reshape([1, N]).squeeze()), 0)
    ax.scatter(P[0, :], P[1, :], P[2, :], marker='x')
    points = torch.tensor(P).to(device) #dem

    anti_padding=5
    rpz_label = np.zeros_like(np.stack([dem_grid[0], dem_grid[0], dem_grid[0], dem_grid[0], dem_grid[0], dem_grid[0]]))

    for yaw in [0]:#np.linspace(0, (2 * np.pi) - np.pi / 4, 8).tolist():
        Ryaw = np.array([[np.cos(yaw), -np.sin(yaw)], [np.sin(yaw), np.cos(yaw)]])
        robot_grid_rot_ravel = np.matmul(Ryaw, np.stack([robot_grid[0].ravel(), robot_grid[1].ravel()]))
        robot_grid_rot = np.zeros_like(robot_grid)
        robot_grid_rot[0] = np.reshape(robot_grid_rot_ravel[0], robot_grid[0].shape)
        robot_grid_rot[1] = np.reshape(robot_grid_rot_ravel[1], robot_grid[0].shape)
        for i in range(anti_padding, dem.shape[0]-anti_padding):
            for j in range(anti_padding, dem.shape[1]-anti_padding):
                time_start = time.time()

                robot_grid_to_get = np.concatenate([robot_grid_rot[0].ravel()[np.newaxis]+dem_grid[0][i,j], robot_grid_rot[1].ravel()[np.newaxis]+dem_grid[1][i,j],
                                                    np.ones_like(robot_grid_rot[0].ravel()[np.newaxis]) * grid_res / 2], 0)

                v = voxel_map.get_voxels(robot_grid_to_get, np.zeros_like(robot_grid_rot[0].ravel()[np.newaxis]))
                under_robot_dem = np.reshape(v, robot_grid[0].shape)


                ### Define terrain (height map)

                X = robot_grid[0]
                Y = robot_grid[1]
                Z = under_robot_dem
                surf = ax.plot_surface(X, Y, Z, linewidth=0, antialiased=False)
                height = torch.tensor(Z).to(device)
                border_x = torch.tensor(length/2)
                border_y = torch.tensor(width/2)
                length_t = torch.tensor(length).to(device)
                width_t = torch.tensor(width).to(device)
                height_under_tracks = torch.tensor(Z.ravel())
                step_t = torch.tensor(grid_res).to(device)

                # Define variables: pitch=0, roll=0, yaw=0, tz=0
                x_in = torch.tensor([0, 0, 0, 0.5])
                x = torch.tensor(x_in, requires_grad=True, dtype=torch.double).to(device)

                lamb = torch.tensor([30], requires_grad=True, dtype=torch.double).to(device)

                # gradient descend
                plt.clf()
                epochs = 500
                lr_w = np.linspace(1,0.001,epochs)
                for i_optim in range(0, epochs):
                    # create rotation matrix and translation vector from variable x
                    Rx = torch.tensor(torch.zeros(3, 3), requires_grad=False, dtype=torch.double).to(device)
                    Ry = torch.tensor(torch.zeros(3, 3), requires_grad=False, dtype=torch.double).to(device)
                    Rz = torch.tensor(torch.zeros(3, 3), requires_grad=False, dtype=torch.double).to(device)
                    t = torch.tensor(torch.zeros(3, 1), requires_grad=False, dtype=torch.double).to(device)

                    Rx[0, 0] = 1
                    Rx[1, 1] = torch.cos(x[0]).to(device)
                    Rx[1, 2] = -torch.sin(x[0]).to(device)
                    Rx[2, 1] = torch.sin(x[0]).to(device)
                    Rx[2, 2] = torch.cos(x[0]).to(device)

                    Ry[0, 0] = torch.cos(x[1]).to(device)
                    Ry[0, 2] = torch.sin(x[1]).to(device)
                    Ry[1, 1] = 1
                    Ry[2, 0] = -torch.sin(x[1]).to(device)
                    Ry[2, 2] = torch.cos(x[1]).to(device)

                    #yaw does not apply
                    Rz[0, 0] = 1#torch.cos(x[2])
                    Rz[0, 1] = 0#-torch.sin(x[2])
                    Rz[1, 0] = 0#torch.sin(x[2])
                    Rz[1, 1] = 1#torch.cos(x[2])
                    Rz[2, 2] = 1

                    t[0, 0] = 0
                    t[1, 0] = 0
                    t[2, 0] = x[3]

                    # define loss
                    pt = (torch.mm(torch.mm(torch.mm(Rx,Ry),Rz), points) + t.repeat(1, N)).to(device) # transform pointcloud
                    #rr = border_y + torch.clamp(pt[1, :].long()-grid_res/2, -width/2, width/2)
                    #cc = border_x + torch.clamp(pt[0, :].long()-grid_res/2, -length/2, length/2)
                    #min_heights = height[rr,cc] # search for corresponding heights
                    #min_heights = height_under_tracks
                    rr = torch.clamp(((width_t / 2 + pt[1, :]) / step_t).long(), 0, height.shape[0] - 1).to(device)
                    cc = torch.clamp(((length_t / 2 + pt[0, :]) / step_t).long(), 0, height.shape[1] - 1).to(device)
                    min_heights = height[rr, cc]  # search for corresponding heights
                    loss_collision = (lamb*torch.relu(min_heights-pt[2,:])).sum().to(device)
                    loss_energy = torch.add((pt[2,:28]*2).sum(), pt[2,28:].sum()).to(device) #pow(2).sum()
                    loss = (loss_collision + loss_energy).to(device)

                    #print(i_optim, ") Loss: ", loss.item(), " = collision_loss: ", loss_collision.item(), " + potential_energy_loss: ", loss_energy.item())

                    # gradient descent alignment
                    x.retain_grad()
                    loss.backward(retain_graph=True)
                    learning_rate = ((torch.tensor([1,1,0,0.5], requires_grad=False, dtype=torch.double)*0.05/(N))*lr_w[i_optim]).to(device)

                    #print("Gradient x: ",x.grad.detach().numpy(),"Gradient lambda: ",lamb.grad.detach().numpy(), )
                    with torch.no_grad():
                        x = x - torch.clamp(learning_rate * x.grad,-0.1,0.1)
                        #lamb = lamb + 1.1*lamb.grad
                        x.requires_grad_()
                        lamb.requires_grad_()
                    #print("x= ", x.detach().numpy(), "lambda= ", lamb.detach().numpy())


                    # visualize
                    if np.bool((np.mod(i_optim,499)==0)&(i_optim!=0)):
                        draw_bars = False
                        plt.clf()
                        ax = plt.axes(projection='3d')
                        Z = height.detach().cpu().numpy()
                        if draw_bars:
                            ax.bar3d(X.ravel(), Y.ravel(), Z.ravel(), 0.1, 0.1, 0.01, shade=True, alpha=0.5)
                        else:
                            surf = ax.plot_surface(X, Y, height.detach().cpu().numpy(), linewidth=0, antialiased=False,
                                                   alpha=0.5)
                        PT = pt.detach().cpu().numpy()
                        idx = (pt[2, :].detach().cpu()-min_heights.detach().cpu()).numpy() < 0.05
                        #ax.scatter(PT[0, ~idx], PT[1, ~idx], PT[2, ~idx], marker='o', color='b')
                        #ax.scatter(PT[0, idx], PT[1, idx], PT[2, idx], marker='o', color='r')
                        #ax.plot_wireframe(np.reshape(PT[0],robot_grid[0].shape), np.reshape(PT[1],robot_grid[0].shape), np.reshape(PT[2],robot_grid[0].shape), color='k')

                        idx_grid = np.reshape(idx, robot_grid[0].shape)
                        idx_grid_tracks = idx_grid[(LT==1)|(RT==1)]
                        idx_grid_body = idx_grid[(LT == 0)&(RT == 0)]
                        #plt.pause(0.001)
                        #plt.draw()
                        rpz_label[0:4, i, j] = x.detach().numpy()
                        rpz_label[4, i, j] = np.sum(idx_grid_tracks)
                        rpz_label[5, i, j] = np.sum(idx_grid_body)
                        #plt.savefig('../data/rigid_body/rigid_body_{:03.2f}'.format(yaw)+'_{:03}'.format(i)+'_{:03}'.format(j)+'.png')
                        print([i,j,time.time() - time_start])
            np.save("../data/d2rpz/000052_label",rpz_label)


if __name__ == '__main__':
    data = np.load('../data/s2d_dfki_run4/001146_label.npz')
    dem = data['label']
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model_s2d = Net()

    model_s2d.load_state_dict(torch.load("../data/s2d_network/best_weights", map_location=device))
    model_s2d.to(device)
    dem[np.isnan(dem)] = 0

    input = torch.from_numpy(dem.astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0)

    # TODO create model to get dense map from sparse
    output = model_s2d(input, input)
    output = output.detach().cpu().numpy()[0][0]
    np.save('001146_output',output)
    #rigid_body_contact(output)
    #support_plane(output)