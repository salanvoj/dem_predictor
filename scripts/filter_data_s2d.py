import glob
import os
from shutil import copyfile

if __name__ == '__main__':
    root_dir = '../data/s2d_josef2/'
    list_data = glob.glob(root_dir + '*label.npz')
    size_data = len(list_data)
    list_data.sort()

    export_dir = '../data/s2d_palety_tst_big/'

    if not os.path.exists(export_dir):
        os.makedirs(export_dir)
    list_ex_data = glob.glob(export_dir + '*label.npz')
    size_ex_data = len(list_ex_data)

    to_create_i = len(list_ex_data)

    for i in range(size_data):
        #source_file = root_dir + '{:06}'.format(i) + '_label.npz'
        source_file = list_data[i]
        new_file = export_dir +  '{:06}'.format(to_create_i) + '_label.npz'
        copyfile(source_file, new_file)
        to_create_i += 1

    #label_file = '{:06}'.format(idx) + '_label.npz'
    #data = np.load(self.root_dir + label_file)
