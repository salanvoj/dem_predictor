import numpy as np
import matplotlib.pyplot as plt
from voxel_map import VoxelMap
import torch
from mpl_toolkits.mplot3d import Axes3D
import time


float_formatter = "{:.4f}".format
np.set_printoptions(formatter={'float_kind': float_formatter})
anti_padding = 5
dem = np.load('test_output.npy')

YAW = [0.]
I = [40, 20, 113, 98, 94]#np.arange(anti_padding, dem.shape[0]-anti_padding) # 40 20 113 98 94
J = [40, 20, 118, 80, 111]#np.arange(anti_padding, dem.shape[1]-anti_padding) # 40 20 118 80 111
LAMB = 10
LAMB_MULT = 1.05
EPOCHS = 50
LR = 50


# def rigid_body_contact(dem):
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
device = torch.device("cpu")

grid_res = 0.1
dem_grid = np.meshgrid(
    np.arange(-dem.shape[0] * grid_res / 2 + grid_res / 2, dem.shape[0] * grid_res / 2 + grid_res / 2, grid_res),
    np.arange(-dem.shape[1] * grid_res / 2 + grid_res / 2, dem.shape[1] * grid_res / 2 + grid_res / 2, grid_res))
voxel_map = VoxelMap(grid_res, -1.0, 1.0, 0.0)
notnan_mask = ~np.isnan(dem)
voxel_map.set_voxels(np.array(
    [dem_grid[0][notnan_mask], dem_grid[1][notnan_mask], np.ones_like(dem_grid[0][notnan_mask]) * grid_res / 2]),
    np.zeros_like(dem_grid[0][notnan_mask], dtype=np.float64)[np.newaxis],
    np.array(dem[notnan_mask])[np.newaxis])

ax = plt.axes(projection='3d')

### Define rigid body (set of 3D points)
length = 0.8
width = 0.6
robot_grid = np.meshgrid(np.arange(-length / 2, length / 2 + grid_res, grid_res),
                         np.arange(-width / 2, width / 2 + grid_res, grid_res))
### Define tracks (set of 3D points)
LT = np.zeros_like(robot_grid[0], dtype=int)
LT[0:2, :] = 1
RT = np.zeros_like(robot_grid[0], dtype=int)
RT[5:, :] = 1
X = robot_grid[0]
Y = robot_grid[1]
Z = np.zeros_like(X)
Z[(LT == 0) & (RT == 0)] += 0.1
N = Z.size
P = np.stack((X.reshape([1, N]).squeeze(), Y.reshape([1, N]).squeeze(), Z.reshape([1, N]).squeeze()), 0)
ax.scatter(P[0, :], P[1, :], P[2, :], marker='x')
points = torch.tensor(P).to(device)  # dem

rpz_label = np.zeros_like(np.stack([dem_grid[0], dem_grid[0], dem_grid[0], dem_grid[0], dem_grid[0], dem_grid[0], dem_grid[0], dem_grid[0], dem_grid[0]]))


def loss_function(x, points, lamb):
    # create rotation matrix and translation vector from variable x
    Rx = torch.tensor(torch.zeros(3, 3), requires_grad=False, dtype=torch.double).to(device)
    Ry = torch.tensor(torch.zeros(3, 3), requires_grad=False, dtype=torch.double).to(device)
    Rz = torch.tensor(torch.zeros(3, 3), requires_grad=False, dtype=torch.double).to(device)
    t = torch.tensor(torch.zeros(3, 1), requires_grad=False, dtype=torch.double).to(device)

    Rx[0, 0] = 1
    Rx[1, 1] = torch.cos(x[0]).to(device)
    Rx[1, 2] = -torch.sin(x[0]).to(device)
    Rx[2, 1] = torch.sin(x[0]).to(device)
    Rx[2, 2] = torch.cos(x[0]).to(device)

    Ry[0, 0] = torch.cos(x[1]).to(device)
    Ry[0, 2] = torch.sin(x[1]).to(device)
    Ry[1, 1] = 1
    Ry[2, 0] = -torch.sin(x[1]).to(device)
    Ry[2, 2] = torch.cos(x[1]).to(device)

    # yaw does not apply
    Rz[0, 0] =  torch.cos(x[2])
    Rz[0, 1] =  -torch.sin(x[2])
    Rz[1, 0] =  torch.sin(x[2])
    Rz[1, 1] =  torch.cos(x[2])
    Rz[2, 2] = 1

    t[0, 0] = x[3]
    t[1, 0] = x[4]
    t[2, 0] = x[5]

    # define loss
    pt = (torch.mm(torch.mm(Ry, Rx), points) + t.repeat(1, N)).to(device)  # transform pointcloud
    pt = (torch.mm(torch.mm(torch.mm(Rx, Ry), Rz), points) + t.repeat(1, N)).to(device)  # transform pointcloud
    # rr = border_y + torch.clamp(pt[1, :].long()-grid_res/2, -width/2, width/2)
    # cc = border_x + torch.clamp(pt[0, :].long()-grid_res/2, -length/2, length/2)
    # min_heights = height[rr,cc] # search for corresponding heights
    # min_heights = height_under_tracks
    rr = torch.clamp(((width_t / 2 + pt[1, :]) / step_t).long(), 0, height.shape[0] - 1).to(device)
    cc = torch.clamp(((length_t / 2 + pt[0, :]) / step_t).long(), 0, height.shape[1] - 1).to(device)
    min_heights = height[rr, cc]  # search for corresponding heights
    #loss_collision = (lamb * torch.relu(min_heights - pt[2, :])).sum().to(device)  # linear exterior penalty function
    loss_collision = (lamb * torch.relu(min_heights - pt[2, :])).pow(2).sum().to(device)  # quadratic exterior penalty function
    #loss_collision = (torch.exp(lamb * (min_heights - pt[2, :]))).sum().to(device)  # exponential exterior penalty function
    # loss_collision = (lamb*torch.log(-(min_heights-pt[2,:]))).sum().to(device) # log barrier
    loss_energy = torch.add((pt[2, :28] ).sum(), pt[2, 28:].sum()).to(device)  # pow(2).sum()
    loss = (loss_collision + loss_energy).to(device)
    return loss, loss_energy, loss_collision, pt, min_heights

def set_axes_equal(ax):
    '''Make axes of 3D plot have equal scale so that spheres appear as spheres,
    cubes as cubes, etc..  This is one possible solution to Matplotlib's
    ax.set_aspect('equal') and ax.axis('equal') not working for 3D.

    Input
      ax: a matplotlib axis, e.g., as output from plt.gca().
    '''

    x_limits = ax.get_xlim3d()
    y_limits = ax.get_ylim3d()
    z_limits = ax.get_zlim3d()

    x_range = abs(x_limits[1] - x_limits[0])
    x_middle = np.mean(x_limits)
    y_range = abs(y_limits[1] - y_limits[0])
    y_middle = np.mean(y_limits)
    z_range = abs(z_limits[1] - z_limits[0])
    z_middle = np.mean(z_limits)

    # The plot bounding box is a sphere in the sense of the infinity
    # norm, hence I call half the max range the plot radius.
    plot_radius = 0.7*max([x_range, y_range, z_range])

    ax.set_xlim3d([x_middle - plot_radius, x_middle + plot_radius])
    ax.set_ylim3d([y_middle - plot_radius, y_middle + plot_radius])
    ax.set_zlim3d([z_middle - plot_radius, z_middle + plot_radius])


for yaw in YAW:  # np.linspace(0, (2 * np.pi) - np.pi / 4, 8).tolist():
    Ryaw = np.array([[np.cos(yaw), -np.sin(yaw)], [np.sin(yaw), np.cos(yaw)]])
    robot_grid_rot_ravel = np.matmul(Ryaw, np.stack([robot_grid[0].ravel(), robot_grid[1].ravel()]))
    robot_grid_rot = np.zeros_like(robot_grid)
    robot_grid_rot[0] = np.reshape(robot_grid_rot_ravel[0], robot_grid[0].shape)
    robot_grid_rot[1] = np.reshape(robot_grid_rot_ravel[1], robot_grid[0].shape)
    for i in I:  # range(anti_padding, dem.shape[0]-anti_padding):
        for j in J:  # range(anti_padding, dem.shape[1]-anti_padding):
            time_start = time.time()

            robot_grid_to_get = np.concatenate([robot_grid_rot[0].ravel()[np.newaxis] + dem_grid[0][i, j],
                                                robot_grid_rot[1].ravel()[np.newaxis] + dem_grid[1][i, j],
                                                np.ones_like(robot_grid_rot[0].ravel()[np.newaxis]) * grid_res / 2], 0)

            v = voxel_map.get_voxels(robot_grid_to_get, np.zeros_like(robot_grid_rot[0].ravel()[np.newaxis]))
            under_robot_dem = np.reshape(v, robot_grid[0].shape)

            ### Define terrain (height map)

            X = robot_grid[0]
            Y = robot_grid[1]

            Z = under_robot_dem

            #Z = np.zeros_like(under_robot_dem)
            #Z[:,-3:] = 0.15
            surf = ax.plot_surface(X, Y, Z, linewidth=0, antialiased=False)
            height = torch.tensor(Z).to(device)
            border_x = torch.tensor(length / 2)
            border_y = torch.tensor(width / 2)
            length_t = torch.tensor(length).to(device)
            width_t = torch.tensor(width).to(device)
            height_under_tracks = torch.tensor(Z.ravel())
            step_t = torch.tensor(grid_res).to(device)

            # Define variables: pitch=0, roll=0, yaw=0, tz=0
            x_in = torch.tensor([0, 0, 0, 0, 0, 0])
            x = torch.tensor(x_in, requires_grad=True, dtype=torch.double).to(device)

            lamb = torch.tensor([LAMB], requires_grad=True, dtype=torch.double).to(device)
            # gradient descend
            plt.clf()
            epochs = EPOCHS
            for i_optim in range(0, epochs):
                loss, loss_energy, loss_collision, pt, min_heights = loss_function(x, points, lamb)

                #print(i_optim, ") Loss: ", loss.item(), " = collision_loss: ", loss_collision.item(),
                #      " + potential_energy_loss: ", loss_energy.item())

                x.retain_grad()
                loss.backward(retain_graph=True)
                #print("Gradient x: ", x.grad.detach().numpy(), "Gradient lambda: ", lamb.grad.detach().numpy(), )
                #loss_energy.backward(retain_graph=True)
                #print("Gradient x: ", x.grad.detach().numpy(), "Gradient lambda: ", lamb.grad.detach().numpy(), )
                #loss_collision.backward(retain_graph=True)
                #print("Gradient x: ", x.grad.detach().numpy(), "Gradient lambda: ", lamb.grad.detach().numpy(), )
                learning_rate = ((torch.tensor(np.ones(x.size()), requires_grad=False, dtype=torch.double) * LR / N))  # *lr_w[i_optim]).to(device)
                # print("Gradient x: ",x.grad.detach().numpy(),"Gradient lambda: ",lamb.grad.detach().numpy(), )
                with torch.no_grad():
                    step = torch.clamp(learning_rate * x.grad, -10, 10)
                    NN = 20
                    ll = np.zeros(NN)
                    steps = np.zeros(NN)
                    for jj in np.arange(0, NN):
                        steps[jj] = ((jj) * (1./20)).__pow__(2)
                        ll[jj], _, _, _, _ = loss_function(x - step * steps[jj], points, lamb)
                    x = x - step * steps[np.argmin(ll)]
                    LR = np.max([1,2*LR* steps[np.argmin(ll)]])
                    #print('LR=', LR)
                    lamb = lamb * LAMB_MULT
                    x.requires_grad_()
                    lamb.requires_grad_()

            print([i, j, time.time() - time_start])
            print(x[0],x[1],x[2])
            # visualize
            if True:  # np.bool((np.mod(i_optim,499)==0)&(i_optim!=0)):
                draw_bars = True
                draw_bars = False
                plt.figure(1)
                plt.clf()
                ax = plt.axes(projection='3d')

                ax.set_aspect('equal')
                set_axes_equal(ax)
                [xx, yy] = np.meshgrid(
                    np.arange(-20 * grid_res / 2 + grid_res / 2, 20 * grid_res / 2 + grid_res / 2, grid_res),
                    np.arange(-20 * grid_res / 2 + grid_res / 2, 20 * grid_res / 2 + grid_res / 2, grid_res))
                zz = np.zeros_like(xx)
                zz[3:-6,-7:] = 0.1
                Z = height.detach().cpu().numpy()
                if draw_bars:
                    #ax.bar3d(X.ravel(), Y.ravel(), Z.ravel(), 0.1, 0.1, 0.01, shade=True, alpha=0.5)
                    #ax.bar3d(X.ravel(), Y.ravel(), Z.ravel()/2, 0.1, 0.1, Z.ravel(), shade=True, alpha=0.5)
                    ax.bar3d(xx.ravel(), yy.ravel(), zz.ravel(),0.1, 0.1, 0.05,  alpha=1)
                else:
                    #surf = ax.plot_surface(xx,yy, zz, linewidth=0, antialiased=True,alpha=0.4,color='k')

                    surf = ax.plot_surface(X, Y, Z, linewidth=0, antialiased=False,alpha=0.5)
                PT = pt.detach().cpu().numpy()
                idx = (pt[2, :].detach().cpu() - min_heights.detach().cpu()).numpy() < 0.02
                ax.scatter(xx, yy,zz, marker='.', color='k',alpha=0.1)
                ax.scatter(PT[0, ~idx], PT[1, ~idx], PT[2, ~idx], marker='o', color='b', alpha=1)
                ax.scatter(PT[0, idx], PT[1, idx], PT[2, idx], marker='o', color='r',alpha=1)
                ax.plot_wireframe(np.reshape(PT[0], robot_grid[0].shape), np.reshape(PT[1], robot_grid[0].shape),
                                  np.reshape(PT[2], robot_grid[0].shape), color='k',linewidth=2)
                plt.savefig('../data/rigid_body/rigid_body_{:03.2f}'.format(yaw)+'_{:03}'.format(i)+'_{:03}'.format(j)+'.png')
                #plt.pause(0.001)
                plt.axis('off')
                plt.show()
                raw_input("Press Enter to continue...")

            #plt.savefig('../data/rigid_body/rigid_body_{:03.2f}'.format(yaw) + '_{:03}'.format(i) + '_{:03}'.format(j) + '.png')
            if False:
                plt.figure(2)
                plt.clf()
                plt.plot(ll)
                plt.pause(0.001)
                plt.draw()
'''
            if np.bool((np.mod(i_optim, EPOCHS-1) == 0) & (i_optim != 0)):
                idx_grid = np.reshape(idx, robot_grid[0].shape)
                idx_grid_tracks = idx_grid[(LT == 1) | (RT == 1)]
                idx_grid_body = idx_grid[(LT == 0) & (RT == 0)]
                rpz_label[0:6, i, j] = x.detach().numpy()
                rpz_label[6, i, j] = np.sum(idx_grid_tracks)
                rpz_label[7, i, j] = np.sum(idx_grid_body)
                rpz_label[8, i, j] = loss_energy
                np.save("../data/d2rpz/000052_label",rpz_label)
'''