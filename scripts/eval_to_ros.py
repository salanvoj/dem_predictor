import numpy as np
import matplotlib.pyplot as plt
import torch
import network_sf2d
import network_d2t
import network_d2rpz
from scipy import ndimage
import glob
import rospy
from sensor_msgs.msg import PointCloud2, Image
from PIL import Image as PilImage
from mit_semseg.models import ModelBuilder, SegmentationModule
import torch.nn as nn
import ros_numpy
import cv2
from torchvision.transforms import Compose, CenterCrop, Normalize, Resize, ToTensor


def class_mapping(input):
    c = input.copy()
    # our class 0 = irrelevant
    condition_0 = (c == 2) | (c == 5) | (c == 7) | (c == 8) | (c == 18) | (c == 23) | (c == 24) | (c == 30) | (c == 31) | (c == 37) | (c == 78) | (c == 81) | (c == 82) | (c == 85) | (c == 86) | (c == 92) | (c == 108) | (c == 134) | (c == 137)
    input[condition_0] = 0

    # class 1 = very soft (water)
    condition_1 = (c == 21) | (c == 26) | (c == 60) | (c == 109) |(c == 113) | (c == 128)
    input[condition_1] = 1

    # class 2 = soft (grass, plants)
    condition_2 = (c == 9) | (c == 17) | (c == 39) | (c == 57) | (c == 66)
    input[condition_2] = 2

    # class 3 = medium (sand)
    condition_3 = (c == 29) | (c == 46) | (c == 91) | (c == 94) | (c == 125)
    input[condition_3] = 3

    # class 4 = hard (trees)
    condition_4 = (c == 4) | (c == 72) | (c == 114) | (c == 115) | (c == 126) | (c == 131) | (c == 149)
    input[condition_4] = 4

    # class 5 = very hard (walls, buildings)
    condition_5 = ~ (condition_0 | condition_1 | condition_2 | condition_3 | condition_4)
    # condition_5 = ~ (condition_0 | condition_1 | condition_2 | condition_3 | condition_4 | (c == -1))     # !!! if there is something unlabelled as -1 in labelling (only for future training)
    input[condition_5] = 5
    return input

def to_pc_msg(frame_id, robot_grid, values):
    data = np.zeros(len(values[~np.isnan(values)].T), dtype=[
        ('x', np.float32),
        ('y', np.float32),
        ('z', np.float32),
        ('val', np.float32)
    ])
    data['x'] = robot_grid[0][~np.isnan(values)]
    data['y'] = robot_grid[1][~np.isnan(values)]
    data['z'] = values[~np.isnan(values)]
    data['val'] = np.ones_like(values[~np.isnan(values)])

    msg = ros_numpy.msgify(PointCloud2, data)
    msg.header.frame_id = frame_id
    msg.header.stamp = rospy.Time.now()
    return msg

def to_pc_3d_msg(frame_id, robot_grid, z_data, values):
    data = np.zeros(len(values[~np.isnan(values)].T), dtype=[
        ('x', np.float32),
        ('y', np.float32),
        ('z', np.float32),
        ('val', np.float32)
    ])
    data['x'] = robot_grid[0][~np.isnan(values)]
    data['y'] = robot_grid[1][~np.isnan(values)]
    data['z'] = z_data[~np.isnan(values)]
    data['val'] = values[~np.isnan(values)]

    msg = ros_numpy.msgify(PointCloud2, data)
    msg.header.frame_id = frame_id
    msg.header.stamp = rospy.Time.now()
    return msg

def load_resnet_model():
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')
    # Network Builders
    net_encoder = ModelBuilder.build_encoder(
        arch="resnet50dilated",
        fc_dim=2048,
        #weights="/home/honza/trav_project/semantic-segmentation-pytorch/ade20k-resnet50dilated-ppm_deepsup/encoder_epoch_20.pth")
        weights = "/home/salanvoj/PycharmProjects/semantic-segmentation-pytorch/ade20k-resnet50dilated-ppm_deepsup/encoder_epoch_20.pth")

    net_decoder = ModelBuilder.build_decoder(
        arch="ppm_deepsup",
        fc_dim=2048,
        num_class=150,
        #weights="/home/honza/trav_project/semantic-segmentation-pytorch/ade20k-resnet50dilated-ppm_deepsup/decoder_epoch_20.pth",use_softmax=True)
        weights="/home/salanvoj/PycharmProjects/semantic-segmentation-pytorch/ade20k-resnet50dilated-ppm_deepsup/decoder_epoch_20.pth",
        use_softmax=True)
    crit = nn.NLLLoss(ignore_index=-1)

    segmentation_module = SegmentationModule(net_encoder, net_decoder, crit)
    segmentation_module.to(device)
    segmentation_module.eval()

    return segmentation_module

def get_features_resnet(img, resnet, dim_reduction=1):
    input_transform = Compose([
        Resize((1616 / (dim_reduction * 2), 1232 / (dim_reduction * 2)), PilImage.BILINEAR),
        ToTensor(),
        Normalize([.485, .456, .406], [.229, .224, .225]),
    ])
    #image_np = cv2.imdecode(img, cv2.IMREAD_COLOR)
    #image_np = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
    image_np = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    im_pil = PilImage.fromarray(image_np).convert("RGB")
    img = input_transform(im_pil).unsqueeze(0)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    img = img.to(device)

    with torch.no_grad():
        feed_dict = {}
        feed_dict['img_data'] = img
        segSize = (img.shape[2], img.shape[3])
        pred_tmp = resnet(feed_dict, segSize=segSize)
    _, pred = torch.max(pred_tmp, dim=1)
    pred = pred.detach().cpu().numpy()
    pred = pred.transpose((1, 2, 0))
    return pred
    #return class_mapping(pred)


def onclick(event,ax,feat):
    print('%s click: button=%d, x=%d, y=%d, xdata=%f, ydata=%f' %
          ('double' if event.dblclick else 'single', event.button,
           event.x, event.y, event.xdata, event.ydata))
    #print(feat.shape)
    print(feat[:,event.ydata.astype(np.int32),event.xdata.astype(np.int32)])
    if(event.button==1):
        ax.plot(feat[:, event.ydata.astype(np.int32), event.xdata.astype(np.int32)],'g')
    if (event.button == 3):
        ax.plot(feat[:,event.ydata.astype(np.int32),event.xdata.astype(np.int32)],'r')
    if (event.button == 2):
        ax.cla()

    #ax.plot(feat[:, 0, 0])

if __name__ == '__main__':
    rospy.init_node('SupportTerrain_publisher')

    resnet = load_resnet_model()
    input_2d_pc_pub = rospy.Publisher('input_2d_pc', PointCloud2, queue_size=2)
    output_2d_pc_pub = rospy.Publisher('output_2d_pc', PointCloud2, queue_size=2)
    st_2d_pc_pub = rospy.Publisher('support_terrain_pc', PointCloud2, queue_size=2)
    hardness_2d_pc_pub = rospy.Publisher('hardness_pc', PointCloud2, queue_size=2)
    im_pub = rospy.Publisher('img', Image, queue_size=2)
    f_pub = rospy.Publisher('im_features', Image, queue_size=2)


    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model_s2d = network_sf2d.Net()
    model_s2d.load_state_dict(torch.load("../data/from_grid/kn/dem", map_location=device))
    model_s2d.to(device)

    model_d2t = network_d2t.Net()
    model_d2t.load_state_dict(torch.load("../data/from_grid/kn/net_epoch_0499", map_location=device))
    model_d2t.to(device)


    model_d2rpz = network_d2rpz.Net()
    model_d2rpz.load_state_dict(torch.load("../data/d2rpz_network/net_weights_d2rpz", map_location=device))
    model_d2rpz.to(device)

    robot_grid = np.meshgrid(np.arange(-12.75, 12.85, 0.1), np.arange(-12.75, 12.85, 0.1))

    data_path = 's2d_3d_outdoor8_kn_val'

    list_labels = glob.glob('../data/'+data_path + '/*label.npz')
    #list_labels.sort()
    num_labels = len(list_labels)

    loss_dem = 0
    loss_st = 0
    loss_roll = []
    loss_pitch = []
    loss_z = []
    loss_abs = []

    sum_features = np.zeros([1, 150])
    print('---')
    for i in range(0,num_labels,1):
        label = np.load(list_labels[i])



        input_msg = to_pc_msg('base_link',robot_grid,label['input'])

        input_2d_pc_pub.publish(input_msg)

        yaw_mask = abs(label['yaw_label'])<(np.pi/8)

        dem_label = label['label']

        yaw_indexes = np.where(yaw_mask)  ## here are indexes of known roll, pitch, z

        roll_label = label['roll_label']
        pitch_label = label['pitch_label']
        z_label = label['z_baselink_label']
        roll_label[~yaw_mask] = np.nan
        pitch_label[~yaw_mask] = np.nan
        z_label[~yaw_mask] = np.nan


        vis = label['visible_mask']
        vis[~np.isnan(vis)] = 0
        vis[np.isnan(vis)] = 1
        bw_dist = ndimage.distance_transform_edt(vis)
        pos_mask = ~np.isnan(yaw_mask) & (bw_dist < 10)  # distance in decimeters from visible
        pos_indexes = np.where(pos_mask)

        roll_label[~pos_mask] = np.nan
        pitch_label[~pos_mask] = np.nan
        z_label[~pos_mask] = np.nan

        rpz = np.stack([roll_label,pitch_label,z_label])
        mask = torch.from_numpy((~np.isnan(label['input'])).astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0)
        input = torch.from_numpy(label['input'].astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0)

        dem_weights = ~np.isnan(dem_label) & (bw_dist < 10)  # distance in decimeters from visible

        input[torch.isnan(input)] = 0
        hardness = torch.from_numpy((label['hardness']).astype(np.float32)).to(device).unsqueeze(0)
        hardness[torch.isnan(hardness)] = 0
        pool = nn.MaxPool2d(3, stride=1, padding=1)
        #features = pool(hardness)
        features = hardness
        #input_w_mask = torch.cat([input,mask,features],1)
        input_w_mask = torch.cat([input, mask], 1)
        to_plot_hardness = label['hardness'].astype(np.float64)


        #hard_msg = to_pc_3d_msg('base_link', robot_grid, label['input'], to_plot_hardness)
        #hardness_2d_pc_pub.publish(hard_msg)

        #sum_features = sum_features + to_plot_hardness[:,~np.isnan(to_plot_hardness[1])].sum(axis=1)



        print(sum_features)

        dense = model_s2d(input_w_mask)

        to_plot_dense = dense[0, 0, :, :].detach().cpu().numpy()
        #to_plot_dense[~pos_mask] = np.nan
        #dense[0, 1, :, :].detach().cpu().numpy()
        to_plot_dense[dense[0, 1, :, :].detach().cpu().numpy() <-3] = np.nan



        dense_msg = to_pc_msg('base_link', robot_grid, to_plot_dense)
        output_2d_pc_pub.publish(dense_msg)

        relu = nn.LeakyReLU(0.0001)

        support_terrain = relu(model_d2t(torch.cat([dense[:, 0:1, :, :],features],1)))

        to_plot_support_terrain = support_terrain[0, 0, :, :].detach().cpu().numpy()
        #to_plot_support_terrain[~pos_mask] = np.nan
        #to_plot_dense[dense[0, 1, :, :].detach().cpu().numpy() < -1] = np.nan

        st_msg = to_pc_msg('base_link', robot_grid, (to_plot_dense-to_plot_support_terrain))
        st_2d_pc_pub.publish(st_msg)

        #im_msg = ros_numpy.msgify(Image, label['img0'][np.newaxis], encoding='rgb8')
        im_cv = cv2.imdecode(label['img0'], cv2.IMREAD_COLOR)
        #im_cv = im_cv[:1150,:,:]
        im_r = im_cv.transpose([1, 0, 2])
        
        f1 = get_features_resnet(im_cv, resnet)
        f1 = f1.transpose([1, 0, 2])
        im_cv = cv2.imdecode(label['img4'], cv2.IMREAD_COLOR)
        #im_cv = im_cv[:1150,:,:]
        im_l = im_cv.transpose([1, 0, 2])

        f2 = get_features_resnet(im_cv, resnet)
        f2 = f2.transpose([1, 0, 2])

        f_np = np.concatenate([f2,f1]).transpose([1, 0, 2]).astype(np.uint8)+1
        f_msg = ros_numpy.msgify(Image, f_np, encoding='mono8')
        f_pub.publish(f_msg)

        im_np = np.concatenate([im_l,im_r])
        im_msg = ros_numpy.msgify(Image, im_np.transpose([1, 0, 2]), encoding='rgb8')
        im_pub.publish(im_msg)

        output_rpz = model_d2rpz(dense[:, 0, :, :][np.newaxis]-support_terrain[:, 0, :, :][np.newaxis]).permute([1, 0, 2, 3])
        output_rpz = output_rpz.detach().cpu().numpy()
        output_rpz = np.pad(output_rpz[:, 0, :, :], [(0, 0), (3, 3), (4, 4)], mode='constant')

        deg_th = 0
        rpz_mask = (abs(rpz[0,:,:]) >deg_th) | (abs(rpz[1,:,:]) >deg_th)
        if (np.sum(yaw_mask & pos_mask)>0)&(np.sum(np.isnan(rpz[0, yaw_mask&pos_mask].T))==0):
            loss_roll.append((rpz[0, yaw_mask&pos_mask].T-output_rpz[0,yaw_mask&pos_mask].T)**2)
            loss_pitch.append((rpz[1, yaw_mask&pos_mask].T-output_rpz[1,yaw_mask&pos_mask].T)**2)
            loss_z.append((rpz[2, yaw_mask & pos_mask].T - output_rpz[2, yaw_mask & pos_mask].T) ** 2)

        dem_label[np.isnan(dem_label)] = 0
        #dem_weights[(dem_label>0.3)] = 0

        output = dense
        target = torch.from_numpy(dem_label.astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0)
        dem_weights_rpz = dem_weights.copy()
        weight = torch.from_numpy(dem_weights_rpz.astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0)
        pred = (output[:, 0, :, :][np.newaxis]).permute([1, 0, 2, 3])
        loss_prediction = torch.sqrt((torch.sum(weight * (pred - target) ** 2) / weight.sum()))
#        loss_abs.append(abs((weight * (pred - target))[weight == 1].cpu().detach().numpy()))


#        _, loss_dem_one,_ = network_sf2d.weighted_mse_loss(dense,torch.from_numpy(dem_label.astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0),torch.from_numpy(dem_weights.astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0))
#        _, loss_st_one, _ = network_sf2d.weighted_mse_loss(dense-support_terrain,torch.from_numpy(dem_label.astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0),torch.from_numpy(dem_weights.astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0))

#        loss_dem += loss_dem_one.cpu().detach().numpy()
#        loss_st += loss_st_one.cpu().detach().numpy()

#        loss_abs = np.concatenate(loss_abs)

        print(["error",'-s2d-','-s2d2rpz-','-s2d_kkt-'])
        print(['roll ','{:.3}'.format(np.sqrt(np.mean(np.concatenate(loss_roll))))])
        print(['pitch','{:.3}'.format(np.sqrt(np.mean(np.concatenate(loss_pitch))))])
        print(['z    ','{:.3}'.format(np.sqrt(np.mean(np.concatenate(loss_z))))])
#        print(['dem  ','{:.3}'.format(loss_dem/num_labels)])
#        print(['st  ','{:.3}'.format(loss_st/num_labels)])
        plt.imshow(f_np[:, :, 0])
        plt.waitforbuttonpress()
        plt.close("all")
        '''

        fig, (ax1,ax2) = plt.subplots(1,2)
        ax1.imshow(label['input'].astype(np.float32))


        cid = fig.canvas.mpl_connect('button_press_event', lambda event: onclick(event,ax2,label['hardness'].astype(np.float32)))
        '''
    #print(sum_features)












