import torch.nn as nn
import torch
import numpy as np
import random

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        chanells = 64

        self.relu = nn.LeakyReLU(0.1, inplace=True)
        self.pool = nn.MaxPool2d(2, 2)
        self.upsample = nn.Upsample(scale_factor=4, mode='bilinear')

        self.conv1 = nn.Conv2d(20, chanells, 5, 1, 2, bias=True)
        self.conv2 = nn.Conv2d(chanells, 2*chanells, 5, 1, 2, bias=True)
        self.conv3 = nn.Conv2d(2*chanells, 4*chanells, 5, 1, 2, bias=True)
        self.conv4 = nn.Conv2d(4*chanells, 8*chanells, 5, 1, 2, bias=True)
        self.conv5 = nn.Conv2d(8*chanells, 16*chanells, 5, 1, 2, bias=True)
        self.conv6 = nn.Conv2d(16*chanells, 20, 5, 1, 2, bias=True)


    def forward(self, input):
        x = self.relu(self.conv1(input))
        x = self.pool(self.relu(self.conv2(x)))
        x = self.pool(self.relu(self.conv3(x)))
        x = self.relu(self.conv4(x))
        x = self.relu(self.conv5(x))
        x = self.conv6(x)
        x = self.upsample(x)

        return x



def bce_loss(output, target, weight):

    loss_bce = nn.BCELoss(reduction='mean', weight=weight)
    sigm = nn.Sigmoid()

    loss = loss_bce(sigm(output), target)
    return loss


if __name__ == '__main__':
    model_s2d_3d = Net()
    inp = torch.Tensor(np.ones([1,32,256,256]))
    out = model_s2d_3d(inp)

    #print out.shape

