
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

label = np.load('../data/s2d_3d_outdoor8_kn/000000_label.npz')

fl_rl = label['flipper_rl']
fl_rl = fl_rl[~np.isnan(fl_rl)]
fl_rr = label['flipper_rr']
fl_rr = fl_rr[~np.isnan(fl_rr)]
fl_fr = label['flipper_fr']
fl_fr = fl_fr[~np.isnan(fl_fr)]
fl_fl = label['flipper_fl']
fl_fl = fl_fl[~np.isnan(fl_fl)]

ax = plt.axes(projection='3d')
grid_res = 0.1

### Define rigid body (set of 3D points)
length = 0.8
width = 0.6
robot_grid = np.meshgrid(np.arange(-length / 2, length / 2 + grid_res, grid_res),
                         np.arange(-width / 2, width / 2 + grid_res, grid_res),np.arange(-grid_res,grid_res,grid_res))
### Define tracks (set of 3D points)
LT = np.zeros_like(robot_grid[0], dtype=int)
LT[0:2, :,:] = 1
RT = np.zeros_like(robot_grid[0], dtype=int)
RT[5:, :,:] = 1
X = robot_grid[0]
Y = robot_grid[1]
Z = robot_grid[2]
Z[(LT == 0) & (RT == 0)] += 0.1
N = Z.size
P = np.stack((X.reshape([1, N]).squeeze(), Y.reshape([1, N]).squeeze(), Z.reshape([1, N]).squeeze()), 0)
#ax.scatter(P[0, :], P[1, :], P[2, :], marker='x')

flipper_length = 0.4
flipper_width = 0.1
flipper_LF = np.meshgrid(np.arange(0, flipper_length, grid_res / 2), np.arange(0, flipper_width, grid_res / 2), np.arange(-grid_res / 2, grid_res / 2 + grid_res, grid_res))
flipper_LF[2][:, :, 1] -= np.ones_like(flipper_LF[2][:, :, 1]) * np.linspace(0, 0.02, flipper_LF[2][:, :, 1].shape[1])
flipper_LF[2][:, :, 0] += np.ones_like(flipper_LF[2][:, :, 0]) * np.linspace(0, 0.02, flipper_LF[2][:, :, 0].shape[1])

flipper_points = np.asarray([flipper_LF[0].ravel(),flipper_LF[1].ravel(),flipper_LF[2].ravel(),np.ones_like(flipper_LF[2].ravel())])
angle = -fl_fl[0]
R = np.asarray([[np.cos(angle),0,-np.sin(angle),0],
                [0,1,0,0],
               [np.sin(angle),0,np.cos(angle),0],
               [0,0,0,1]])
rot_flipper_points = np.matmul(R,flipper_points)
flipper_LF[0] = np.reshape(rot_flipper_points[0],flipper_LF[0].shape)
flipper_LF[1] = np.reshape(rot_flipper_points[1],flipper_LF[0].shape)
flipper_LF[2] = np.reshape(rot_flipper_points[2],flipper_LF[0].shape)

flipper_LF[0] += 0.3
flipper_LF[1] += 0.3
flipper_LF[2] -= 0.05


flipper_RF = np.meshgrid(np.arange(0, flipper_length, grid_res / 2), np.arange(0, flipper_width, grid_res / 2), np.arange(-grid_res / 2, grid_res / 2 + grid_res, grid_res))
flipper_points = np.asarray([flipper_LF[0].ravel(),flipper_LF[1].ravel(),flipper_LF[2].ravel(),np.ones_like(flipper_LF[2].ravel())])
flipper_RF[2][:, :, 1] -= np.ones_like(flipper_RF[2][:, :, 1]) * np.linspace(0, 0.02, flipper_RF[2][:, :, 1].shape[1])
flipper_RF[2][:, :, 0] += np.ones_like(flipper_RF[2][:, :, 0]) * np.linspace(0, 0.02, flipper_RF[2][:, :, 0].shape[1])
flipper_points = np.asarray([flipper_RF[0].ravel(),flipper_RF[1].ravel(),flipper_RF[2].ravel(),np.ones_like(flipper_RF[2].ravel())])

angle = -fl_fr[0]
R = np.asarray([[np.cos(angle),0,-np.sin(angle),0],
                [0,1,0,0],
               [np.sin(angle),0,np.cos(angle),0],
               [0,0,0,1]])
rot_flipper_points = np.matmul(R,flipper_points)
flipper_RF[0] = np.reshape(rot_flipper_points[0],flipper_RF[0].shape)
flipper_RF[1] = np.reshape(rot_flipper_points[1],flipper_RF[0].shape)
flipper_RF[2] = np.reshape(rot_flipper_points[2],flipper_RF[0].shape)

flipper_RF[0] += 0.3
flipper_RF[1] -= 0.35
flipper_RF[2] -= 0.05


flipper_RB = np.meshgrid(np.arange(0, flipper_length, grid_res / 2), np.arange(0, flipper_width, grid_res / 2), np.arange(-grid_res / 2, grid_res / 2 + grid_res, grid_res))
flipper_RB[2][:, :, 1] -= np.ones_like(flipper_RB[2][:, :, 1]) * np.linspace(0, 0.02, flipper_RB[2][:, :, 1].shape[1])
flipper_RB[2][:, :, 0] += np.ones_like(flipper_RB[2][:, :, 0]) * np.linspace(0, 0.02, flipper_RB[2][:, :, 0].shape[1])
flipper_points = np.asarray([flipper_RB[0].ravel(),flipper_RB[1].ravel(),flipper_RB[2].ravel(),np.ones_like(flipper_RB[2].ravel())])
angle = fl_rr[0]-np.pi/2
R = np.asarray([[np.cos(angle),0,-np.sin(angle),0],
                [0,1,0,0],
               [np.sin(angle),0,np.cos(angle),0],
               [0,0,0,1]])
rot_flipper_points = np.matmul(R,flipper_points)
flipper_RB[0] = np.reshape(rot_flipper_points[0],flipper_RB[0].shape)
flipper_RB[1] = np.reshape(rot_flipper_points[1],flipper_RB[0].shape)
flipper_RB[2] = np.reshape(rot_flipper_points[2],flipper_RB[0].shape)


flipper_RB[0] -= 0.3
flipper_RB[1] -= 0.35
flipper_RB[2] -= 0.05


flipper_LB = np.meshgrid(np.arange(0, flipper_length, grid_res / 2), np.arange(0, flipper_width, grid_res / 2), np.arange(-grid_res / 2, grid_res / 2 + grid_res, grid_res))
flipper_LB[2][:, :, 1] -= np.ones_like(flipper_LB[2][:, :, 1]) * np.linspace(0, 0.02, flipper_LB[2][:, :, 1].shape[1])
flipper_LB[2][:, :, 0] += np.ones_like(flipper_LB[2][:, :, 0]) * np.linspace(0, 0.02, flipper_LB[2][:, :, 0].shape[1])
flipper_points = np.asarray([flipper_LB[0].ravel(),flipper_LB[1].ravel(),flipper_LB[2].ravel(),np.ones_like(flipper_LB[2].ravel())])
angle = fl_rl[0]-np.pi/2
R = np.asarray([[np.cos(angle),0,-np.sin(angle),0],
                [0,1,0,0],
               [np.sin(angle),0,np.cos(angle),0],
               [0,0,0,1]])
rot_flipper_points = np.matmul(R,flipper_points)
flipper_LB[0] = np.reshape(rot_flipper_points[0],flipper_LB[0].shape)
flipper_LB[1] = np.reshape(rot_flipper_points[1],flipper_LB[0].shape)
flipper_LB[2] = np.reshape(rot_flipper_points[2],flipper_LB[0].shape)

flipper_LB[0] -= 0.3
flipper_LB[1] += 0.3
flipper_LB[2] -= 0.05


#ax.plot_wireframe(X[:,:,0], Y[:,:,0],Z[:,:,0], color='k',linewidth=2)
#ax.plot_wireframe(X[:,:,-1], Y[:,:,-1],Z[:,:,-1], color='k',linewidth=2)

#ax.plot_wireframe(X[:,0,:], Y[:,0,:],Z[:,0,:], color='k',linewidth=2)
#ax.plot_wireframe(X[:,-1,:], Y[:,-1,:],Z[:,-1,:], color='k',linewidth=2)

# PLOT TRACKS
ax.plot_wireframe(X[0,:,:], Y[0,:,:],Z[0,:,:], color='k',linewidth=2)
ax.plot_wireframe(X[1,:,:], Y[1,:,:],Z[1,:,:], color='k',linewidth=2)

ax.plot_wireframe(X[-1,:,:], Y[-1,:,:],Z[-1,:,:], color='k',linewidth=2)
ax.plot_wireframe(X[-2,:,:], Y[-2,:,:],Z[-2,:,:], color='k',linewidth=2)

ax.plot_wireframe(X[0:2,:,0], Y[0:2,:,0],Z[0:2,:,0], color='k',linewidth=2)
ax.plot_wireframe(X[0:2,:,-1], Y[0:2,:,-1],Z[0:2,:,-1], color='k',linewidth=2)

ax.plot_wireframe(X[-2:,:,0], Y[-2:,:,0],Z[-2:,:,0], color='k',linewidth=2)
ax.plot_wireframe(X[-2:,:,-1], Y[-2:,:,-1],Z[-2:,:,-1], color='k',linewidth=2)

# PLOT BODY
Z_body = np.concatenate([Z[1,:,1][np.newaxis], Z[2:-2,:,0], Z[-2,:,1][np.newaxis]])
ax.plot_wireframe(X[1:-1,:,0], Y[1:-1,:,0],Z_body, color='b',linewidth=2)
ax.plot_wireframe(X[1:-1,:,0], Y[1:-1,:,0],Z[1:-1,:,-1], color='b',linewidth=2)



#ax.plot_wireframe(X[-2,:,:], Y[-2,:,:],Z[-2,:,:], color='k',linewidth=2)
# PLOT FLIPPER
ax.plot_wireframe(flipper_LF[0][:, :, 0], flipper_LF[1][:, :, 0], flipper_LF[2][:, :, 0], color='r', linewidth=2)
ax.plot_wireframe(flipper_LF[0][:, :, -1], flipper_LF[1][:, :, -1], flipper_LF[2][:, :, -1], color='r', linewidth=2)
ax.plot_wireframe(flipper_LF[0][:, 0, :], flipper_LF[1][:, 0, :], flipper_LF[2][:, 0, :], color='r', linewidth=2)
ax.plot_wireframe(flipper_LF[0][:, -1, :], flipper_LF[1][:, -1, :], flipper_LF[2][:, -1, :], color='r', linewidth=2)
# PLOT FLIPPER
ax.plot_wireframe(flipper_RF[0][:, :, 0], flipper_RF[1][:, :, 0], flipper_RF[2][:, :, 0], color='r', linewidth=2)
ax.plot_wireframe(flipper_RF[0][:, :, -1], flipper_RF[1][:, :, -1], flipper_RF[2][:, :, -1], color='r', linewidth=2)
ax.plot_wireframe(flipper_RF[0][:, 0, :], flipper_RF[1][:, 0, :], flipper_RF[2][:, 0, :], color='r', linewidth=2)
ax.plot_wireframe(flipper_RF[0][:, -1, :], flipper_RF[1][:, -1, :], flipper_RF[2][:, -1, :], color='r', linewidth=2)
# PLOT FLIPPER
ax.plot_wireframe(flipper_RB[0][:, :, 0], flipper_RB[1][:, :, 0], flipper_RB[2][:, :, 0], color='r', linewidth=2)
ax.plot_wireframe(flipper_RB[0][:, :, -1], flipper_RB[1][:, :, -1], flipper_RB[2][:, :, -1], color='r', linewidth=2)
ax.plot_wireframe(flipper_RB[0][:, 0, :], flipper_RB[1][:, 0, :], flipper_RB[2][:, 0, :], color='r', linewidth=2)
ax.plot_wireframe(flipper_RB[0][:, -1, :], flipper_RB[1][:, -1, :], flipper_RB[2][:, -1, :], color='r', linewidth=2)
# PLOT FLIPPER
ax.plot_wireframe(flipper_LB[0][:, :, 0], flipper_LB[1][:, :, 0], flipper_LB[2][:, :, 0], color='r', linewidth=2)
ax.plot_wireframe(flipper_LB[0][:, :, -1], flipper_LB[1][:, :, -1], flipper_LB[2][:, :, -1], color='r', linewidth=2)
ax.plot_wireframe(flipper_LB[0][:, 0, :], flipper_LB[1][:, 0, :], flipper_LB[2][:, 0, :], color='r', linewidth=2)
ax.plot_wireframe(flipper_LB[0][:, -1, :], flipper_LB[1][:, -1, :], flipper_LB[2][:, -1, :], color='r', linewidth=2)

plt.show()