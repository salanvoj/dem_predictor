#!/usr/bin/env python
import rospy
import roslib
import message_filters
import sys
import os
from sensor_msgs.msg import PointCloud2, CompressedImage, CameraInfo
from image_geometry import PinholeCameraModel
import ros_numpy
import tf2_ros
import tf.transformations
import numpy as np
import cv2
import matplotlib.pyplot as plt
import math
import time
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data as tdata
# from node_utils import *
from ERFNet import load_model
from OccupancyMap import OccupancyMap, filter_pc
import DEM_sparse2dense
import queue
from network_s2d import Net
# from net_traversability import TraversabilityNet
from PIL import Image
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision.transforms import Compose, CenterCrop, Normalize, Resize
from torchvision.transforms import ToTensor, ToPILImage

DEBUG = True


class Zbuffer():
    def __init__(self, features, dim_reduction=2):
        self.width = 1232/(2*dim_reduction)
        self.height = 1616/(2*dim_reduction)
        self.depths = np.ones((1616/(2*dim_reduction), 1232/(2*dim_reduction))) * 100
        self.pcd = np.zeros((3, 1616/(2*dim_reduction), 1232/(2*dim_reduction)))
        # self.features = np.ones((1616/(2*dim_reduction), 1232/(2*dim_reduction))) * 255*256*256     # just init red color for DEBUG
        self.features = features        # ERFNet features

    def projection(self, voxels, camera_model, resolution, dim_reduction=2):
        start = time.time()
        fx = camera_model.fx()
        fy = camera_model.fy()
        Tx = camera_model.Tx()
        Ty = camera_model.Ty()
        cx = camera_model.cx()
        cy = camera_model.cy()

        u0_arr = np.round(
            ((fx * (voxels[0, :] - resolution / 2) + Tx) / voxels[2, :] + cx) / (
                        2 * dim_reduction)).astype(np.int)
        v0_arr = np.round(
            ((fy * (voxels[1, :] - resolution / 2) + Ty) / voxels[2, :] + cy) / (
                        2 * dim_reduction)).astype(np.int)

        u2_arr = np.round(
            ((fx * (voxels[0, :] + resolution / 2) + Tx) / voxels[2, :] + cx) / (
                        2 * dim_reduction)).astype(np.int)
        v1_arr = np.round(
            ((fy * (voxels[1, :] + resolution / 2) + Ty) / voxels[2, :] + cy) / (
                        2 * dim_reduction)).astype(np.int)

        # pick only valid pixels (in image plane)
        condition_indices = np.logical_not(
            np.logical_or(np.logical_and(u0_arr < 0, u2_arr < 0), np.logical_and(v0_arr < 0, v1_arr < 0),
                          np.logical_or(np.logical_and(u0_arr >= self.width, u2_arr >= self.width),
                                        np.logical_and(v0_arr >= self.height, v1_arr >= self.height))))
        u_arr0 = u0_arr[condition_indices]
        u_arr2 = u2_arr[condition_indices]
        v_arr0 = v0_arr[condition_indices]
        v_arr1 = v1_arr[condition_indices]
        z_arr = voxels[2, :][condition_indices]
        pcd_arr = voxels[:, condition_indices]

        # correction of indices
        u_arr0[u_arr0 < 0] = 0
        u_arr2[u_arr2 < 0] = 0
        v_arr0[v_arr0 < 0] = 0
        v_arr1[v_arr1 < 0] = 0
        u_arr0[u_arr0 >= self.width] = self.width - 1
        u_arr2[u_arr2 >= self.width] = self.width - 1
        v_arr0[v_arr0 >= self.height] = self.height - 1
        v_arr1[v_arr1 >= self.height] = self.height - 1

        # Z-buffering
        start_buffer = time.time()
        for i in range(len(u_arr0)):
            u0 = u_arr0[i]
            u2 = u_arr2[i]
            v0 = v_arr0[i]
            v1 = v_arr1[i]
            for ii in range(u0, u2 + 1):  # for ii in range(min(u0, u2), max(u0, u2)+1):
                for jj in range(v0, v1 + 1):  # for jj in range(min(v0, v1), max(v0, v1)+1):
                    if self.depths[jj, ii] > z_arr[i]:
                        self.depths[jj, ii] = z_arr[i]
                        self.pcd[:, jj, ii] = pcd_arr[:, i]

        print("Z buffering through reduced PCD took: " + str(time.time() - start_buffer))

        print("Projection to pixels took: " + str(time.time() - start))
        frontiers_voxels = self.pcd[:, self.depths < 100]
        features = self.features[self.depths < 100]

        return frontiers_voxels, features


class RenderNode():
    def __init__(self):
        self.tf = tf2_ros.Buffer(cache_time=rospy.Duration(20))
        self.tf_sub = tf2_ros.TransformListener(self.tf)

        self.map_frame = 'map'
        self.grid_resolution = 0.1
        # get camera parameters
        cam_info_topic1 = rospy.get_param("~camera_info_topic", "/viz/camera_0/camera_info")
        cam_info_topic2 = rospy.get_param("~camera_info_topic2", "/viz/camera_1/camera_info")
        cam_info_topic3 = rospy.get_param("~camera_info_topic3", "/viz/camera_2/camera_info")
        cam_info_topic4 = rospy.get_param("~camera_info_topic4", "/viz/camera_3/camera_info")
        cam_info_topic5 = rospy.get_param("~camera_info_topic5", "/viz/camera_4/camera_info")
        self.camera_ids = [cam_info_topic1, cam_info_topic2, cam_info_topic3, cam_info_topic4, cam_info_topic5]

        print('waiting for camera info')
        self.camera_model = {}
        for i in range(len(self.camera_ids)):
            msg = rospy.wait_for_message(self.camera_ids[i], CameraInfo)
            self.camera_model[msg.header.frame_id] = PinholeCameraModel()
            self.camera_model[msg.header.frame_id].fromCameraInfo(msg)
        print('camera info recieved')

        # create subscriber for camera images and pointcloud
        pcd_sub = message_filters.Subscriber(rospy.get_param("~bigbox_topic", '/dynamic_point_cloud_bigbox'),
                                             PointCloud2)
        image0_sub = message_filters.Subscriber("/viz/camera_0/image/compressed", CompressedImage)
        image1_sub = message_filters.Subscriber("/viz/camera_1/image/compressed", CompressedImage)
        image2_sub = message_filters.Subscriber("/viz/camera_2/image/compressed", CompressedImage)
        image3_sub = message_filters.Subscriber("/viz/camera_3/image/compressed", CompressedImage)
        image4_sub = message_filters.Subscriber("/viz/camera_4/image/compressed", CompressedImage)

        time_synchro = message_filters.ApproximateTimeSynchronizer(
            [pcd_sub, image0_sub, image1_sub, image2_sub, image3_sub, image4_sub], 10, 1)
        time_synchro.registerCallback(self.callback)

        # voxel maps
        self.input_3d_map = OccupancyMap(self.map_frame, self.grid_resolution)
        self.input_2d_map = OccupancyMap(self.map_frame, self.grid_resolution)
        self.voxel_map = OccupancyMap(self.map_frame, self.grid_resolution)

        if torch.cuda.is_available():
            self.device = torch.device('cuda')
        else:
            self.device = torch.device('cpu')
        self.model_s2d = Net()
        self.model_s2d = self.model_s2d.to(torch.device("cpu"))
        self.model_s2d.load_state_dict(
            torch.load("/home/honza/trav_project/net_weights_s2d", map_location=torch.device("cpu")))
        self.confidence_threshold = -3
        self.model_erfnet = load_model()

        self.voxelmap_publisher = rospy.Publisher('output_3d_pc', PointCloud2, queue_size=2)
        self.frontiers_publisher = rospy.Publisher('frontiers', PointCloud2, queue_size=2)

    def callback(self, pointcloud, cam_0, cam_1, cam_2, cam_3, cam_4):
        # TODO: handle pointcloud (transform to map frame, fill heights!, build voxel map)
        start_callback = time.time()
        pcd_to_map = self.tf.lookup_transform(self.map_frame, pointcloud.header.frame_id, pointcloud.header.stamp)
        pc_cloud = ros_numpy.numpify(pointcloud)
        pc_array_homogenous = np.concatenate(
            [np.asarray([pc_cloud['x'].ravel(), pc_cloud['y'].ravel(), pc_cloud['z'].ravel()]),
             np.ones_like(pc_cloud['z'])[np.newaxis]])

        T = ros_numpy.numpify(pcd_to_map.transform)
        pc_cloud_map_frame = np.matmul(T, pc_array_homogenous)

        # create sensor origin in map frame
        origins = np.ones_like(pc_cloud['x']) * np.array(
            [pcd_to_map.transform.translation.x, pcd_to_map.transform.translation.y,
             pcd_to_map.transform.translation.z], )[np.newaxis].T

        # create 3D voxel map
        self.input_3d_map.voxel_map.clear()

        self.input_3d_map.voxel_map.update_lines(origins, pc_cloud_map_frame[:3, :])
        self.input_3d_map = filter_pc(self.input_3d_map)
        self.input_2d_map = self.input_3d_map.to_2d(self.input_2d_map, pcd_to_map)

        # grid in robot frame
        robot_grid = np.meshgrid(np.arange(-12.75, 12.85, self.grid_resolution),
                                 np.arange(-12.75, 12.85, self.grid_resolution))
        robot_grid_array = np.asarray(
            [robot_grid[0].ravel(), robot_grid[1].ravel(), np.ones_like(robot_grid[1].ravel()) * self.grid_resolution])
        # to homogeneous coordinates
        robot_grid_array = np.concatenate(
            [robot_grid_array, np.ones(robot_grid_array.shape[1])[np.newaxis]])  # not sure about z=0.1
        # transform robot grid to map frame
        robot_grid_mapframe = np.matmul(T, robot_grid_array)
        # fill the input from 2d map
        robot_grid_mapframe_2d = np.concatenate([robot_grid_mapframe[0:2, :], np.ones(robot_grid_array.shape[1])[
            np.newaxis] * self.grid_resolution / 2])  # x,y,z in 2.5d map
        v = self.input_2d_map.voxel_map.get_voxels(robot_grid_mapframe_2d, np.zeros(
            robot_grid_array.shape[1])) - pcd_to_map.transform.translation.z
        # sparse map
        input_grid = np.reshape(v, robot_grid[0].shape)
        mask = torch.from_numpy((~np.isnan(input_grid)).astype(np.float32)).to(torch.device("cpu")).unsqueeze(
            0).unsqueeze(0)
        input_grid[np.isnan(input_grid)] = 0
        input = torch.from_numpy(input_grid.astype(np.float32)).to(torch.device("cpu")).unsqueeze(0).unsqueeze(0)

        input_w_mask = torch.cat([input, mask], 1)

        # fill heights
        output = self.model_s2d(input_w_mask)
        output = output.detach().cpu().numpy()

        output_conf = output[0, 1, :, :]
        # fill 2d map from output
        output = output[0, 0, (output_conf >= self.confidence_threshold)]
        robot_grid_mapframe_2d = robot_grid_mapframe_2d[:, (output_conf >= self.confidence_threshold).ravel()]
        output_map2d = OccupancyMap(self.map_frame, self.grid_resolution)
        output_map2d.voxel_map.set_voxels(robot_grid_mapframe_2d, np.zeros_like(output.ravel(), dtype=np.float64),
                                          output.ravel()-self.grid_resolution)
        # convert output to 3d map
        self.tracing_map = OccupancyMap(self.map_frame, self.grid_resolution)
        self.tracing_map = output_map2d.to_3d_dem(self.tracing_map, pcd_to_map)

        if DEBUG:
            self.voxelmap_publisher.publish(self.tracing_map.to_pc_msg(self.map_frame))

        # TODO: Z-buffer (get pcd, transform to camera frame and take only minimal Z-coord)
        # voxels, _, _ = self.tracing_map.voxel_map.get_voxels()
        # voxels = np.asarray(voxels)
        # voxels = np.concatenate([voxels, np.ones(voxels.shape[1])[np.newaxis]])
        # map_to_cam = self.tf.lookup_transform(cam_0.header.frame_id, self.map_frame, cam_0.header.stamp)
        # T = ros_numpy.numpify(map_to_cam.transform)
        #
        # voxels_cam_frame = np.matmul(T, voxels)
        # # omit voxels behind camera
        # voxels_cam_frame = voxels_cam_frame[:3, voxels_cam_frame[2] > 0]    # z-coord is negative for such voxels
        #
        # self.voxel_map.voxel_map.set_voxels(voxels_cam_frame, np.zeros_like(voxels_cam_frame[1]), np.ones_like(voxels_cam_frame[1]))
        # voxels_cam_frame, _, _ = self.voxel_map.voxel_map.get_voxels()

        voxels_cam_frame_0 = self.transform_to_cam(cam_0)
        voxels_cam_frame_1 = self.transform_to_cam(cam_1)
        voxels_cam_frame_2 = self.transform_to_cam(cam_2)
        voxels_cam_frame_3 = self.transform_to_cam(cam_3)
        voxels_cam_frame_4 = self.transform_to_cam(cam_4)

        dim_reduction = 2
        print("Getting image features ...")
        features_time = time.time()
        features_0 = self.get_features(cam_0, dim_reduction=dim_reduction)
        features_1 = self.get_features(cam_1, dim_reduction=dim_reduction)
        features_2 = self.get_features(cam_2, dim_reduction=dim_reduction)
        features_3 = self.get_features(cam_3, dim_reduction=dim_reduction)
        features_4 = self.get_features(cam_4, dim_reduction=dim_reduction)
        features_time = time.time() - features_time
        # if DEBUG:
        #     plt.imsave("/home/honza/Desktop/features.png", features_0[:, :, 15])
        z_buffer_0 = Zbuffer(features_0, dim_reduction=dim_reduction)
        z_buffer_1 = Zbuffer(features_1, dim_reduction=dim_reduction)
        z_buffer_2 = Zbuffer(features_2, dim_reduction=dim_reduction)
        z_buffer_3 = Zbuffer(features_3, dim_reduction=dim_reduction)
        z_buffer_4 = Zbuffer(features_4, dim_reduction=dim_reduction)

        print("Find my frontiers ...")

        frontiers_voxels0, features0 = z_buffer_0.projection(voxels_cam_frame_0, self.camera_model[cam_0.header.frame_id], self.grid_resolution, dim_reduction=dim_reduction)
        frontiers_voxels1, features1 = z_buffer_1.projection(voxels_cam_frame_1, self.camera_model[cam_1.header.frame_id],
                                                           self.grid_resolution, dim_reduction=dim_reduction)
        frontiers_voxels2, features2 = z_buffer_2.projection(voxels_cam_frame_2, self.camera_model[cam_2.header.frame_id],
                                                           self.grid_resolution, dim_reduction=dim_reduction)
        frontiers_voxels3, features3 = z_buffer_3.projection(voxels_cam_frame_3, self.camera_model[cam_3.header.frame_id],
                                                           self.grid_resolution, dim_reduction=dim_reduction)
        frontiers_voxels4, features4 = z_buffer_4.projection(voxels_cam_frame_4, self.camera_model[cam_4.header.frame_id],
                                                           self.grid_resolution, dim_reduction=dim_reduction)

        frontiers_voxels0 = np.concatenate([frontiers_voxels0, np.ones(frontiers_voxels0.shape[1])[np.newaxis]])
        cam_to_map = self.tf.lookup_transform(self.map_frame, cam_0.header.frame_id, cam_0.header.stamp)
        T = ros_numpy.numpify(cam_to_map.transform)
        frontiers_map_frame0 = np.matmul(T, frontiers_voxels0)

        frontiers_voxels1 = np.concatenate([frontiers_voxels1, np.ones(frontiers_voxels1.shape[1])[np.newaxis]])
        cam_to_map = self.tf.lookup_transform(self.map_frame, cam_1.header.frame_id, cam_1.header.stamp)
        T = ros_numpy.numpify(cam_to_map.transform)
        frontiers_map_frame1 = np.matmul(T, frontiers_voxels1)

        frontiers_voxels2 = np.concatenate([frontiers_voxels2, np.ones(frontiers_voxels2.shape[1])[np.newaxis]])
        cam_to_map = self.tf.lookup_transform(self.map_frame, cam_2.header.frame_id, cam_2.header.stamp)
        T = ros_numpy.numpify(cam_to_map.transform)
        frontiers_map_frame2 = np.matmul(T, frontiers_voxels2)

        frontiers_voxels3 = np.concatenate([frontiers_voxels3, np.ones(frontiers_voxels3.shape[1])[np.newaxis]])
        cam_to_map = self.tf.lookup_transform(self.map_frame, cam_3.header.frame_id, cam_3.header.stamp)
        T = ros_numpy.numpify(cam_to_map.transform)
        frontiers_map_frame3 = np.matmul(T, frontiers_voxels3)

        frontiers_voxels4 = np.concatenate([frontiers_voxels4, np.ones(frontiers_voxels4.shape[1])[np.newaxis]])
        cam_to_map = self.tf.lookup_transform(self.map_frame, cam_4.header.frame_id, cam_4.header.stamp)
        T = ros_numpy.numpify(cam_to_map.transform)
        frontiers_map_frame4 = np.matmul(T, frontiers_voxels4)

        # frontiers_map_frame = frontiers_map_frame0
        frontiers_map_frame = np.hstack((frontiers_map_frame0, frontiers_map_frame1, frontiers_map_frame2, frontiers_map_frame3, frontiers_map_frame4))
        # features = features0
        features = np.vstack((features0, features1, features2, features3, features4))

        frontiers_map = OccupancyMap(self.map_frame, self.grid_resolution)
        if not DEBUG:
            # set voxels with features
            frontiers_map.voxel_map.set_voxels(frontiers_map_frame[:3, :], np.zeros_like(frontiers_map_frame[0], dtype=np.float64), features)
        if DEBUG:
            print("Publishing frontiers PCD ...")
            # set color by given feature
            color_features = features[:, 15].reshape((features[:, 15].size, 1))
            vis_img = plt.imshow(color_features)
            color = vis_img.cmap(vis_img.norm(color_features))
            color = np.asarray(color[:, :, :3] * 255, dtype=np.int)
            color = color[:, :, 0] * 256 * 256 + color[:, :, 1] * 256 + color[:, :, 2]
            # set voxels with colors and publish
            frontiers_map.voxel_map.set_voxels(frontiers_map_frame[:3, :], np.zeros_like(frontiers_map_frame[0], dtype=np.float64), color)
            self.frontiers_publisher.publish(frontiers_map.to_pc_msg_coloured(self.map_frame))
        print("Done. Callback overall took: " + str(time.time() - start_callback - features_time))

    def get_features(self, cam, dim_reduction=2):
        input_transform = Compose([
            Resize((1616/dim_reduction, 1232/dim_reduction), Image.BILINEAR),
            ToTensor(),
            # Normalize([.485, .456, .406], [.229, .224, .225]),
        ])

        np_arr = np.fromstring(cam.data, np.uint8)
        image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
        im_pil = Image.fromarray(image_np)
        im_pil = input_transform(im_pil).unsqueeze(0)
        im_pil = im_pil.to(self.device)
        # dim = (int(image_np.shape[1] / dim_reduction), int(image_np.shape[0] / dim_reduction))
        # image_np = cv2.resize(image_np, dim)
        #
        # image_np = image_np.reshape(1, image_np.shape[0], image_np.shape[1], image_np.shape[2]).astype('f4').transpose(0, 3, 1, 2)
        # image_tensor = torch.from_numpy(image_np)
        # image_tensor = image_tensor.to(self.device)

        # features = self.erfnet_model(image_tensor)
        features = self.model_erfnet(im_pil)
        features = features.detach().cpu().numpy().transpose(0, 2, 3, 1)
        return features.reshape(features.shape[1], features.shape[2], features.shape[3])

    def transform_to_cam(self, cam):
        voxels, _, _ = self.tracing_map.voxel_map.get_voxels()
        voxels = np.asarray(voxels)
        voxels = np.concatenate([voxels, np.ones(voxels.shape[1])[np.newaxis]])
        map_to_cam = self.tf.lookup_transform(cam.header.frame_id, self.map_frame, cam.header.stamp)
        T = ros_numpy.numpify(map_to_cam.transform)

        voxels_cam_frame = np.matmul(T, voxels)
        # omit voxels behind camera
        voxels_cam_frame = voxels_cam_frame[:3, voxels_cam_frame[2] > 0]    # z-coord is negative for such voxels

        self.voxel_map.voxel_map.set_voxels(voxels_cam_frame, np.zeros_like(voxels_cam_frame[1]), np.ones_like(voxels_cam_frame[1]))
        voxels_cam_frame, _, _ = self.voxel_map.voxel_map.get_voxels()
        self.voxel_map.voxel_map.clear()

        return voxels_cam_frame

def render_node():
    rospy.init_node("render_node", anonymous=True)

    render = RenderNode()

    rospy.spin()


if __name__ == '__main__':
    # args = rospy.myargv(argv=sys.argv)
    render_node()
