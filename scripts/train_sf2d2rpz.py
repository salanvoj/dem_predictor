import torch
import network_sf2d
import network_d2rpz
import dataset_sf2d
from time import gmtime, strftime
from tensorboardX import SummaryWriter
import os
import torch.optim as optim
import numpy as np
import torch.nn as nn
import matplotlib.pyplot as plt
import math
from shutil import copyfile
from project_features import Render



if __name__ == '__main__':
    epochs = 500
    batch_size = 8
    learning_rate = np.ones([epochs, 1])*0.1


    runtime = strftime("%Y-%m-%d_%H:%M:%S", gmtime())
    output_file = "../data/sf2d_network/run_"+runtime
    writer = SummaryWriter('../data/sf2d_network/tensorboardX/run_' + runtime)

    if not os.path.exists(output_file):
        os.makedirs(output_file)

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    net = network_sf2d.Net()
    net.load_state_dict(torch.load("../data/sf2d_network/run_2020-09-03_11:43:21/net_epoch_0012", map_location=device))

    net.to(device)

    net_d2rpz = network_d2rpz.Net()
    net_d2rpz.load_state_dict(torch.load("../data/d2rpz_network/net_weights_d2rpz", map_location=device))
    net_d2rpz.to(device)

    dataset_trn = dataset_sf2d.Dataset("../data/s2d_3d_outdoor/")
    trainloader = torch.utils.data.DataLoader(dataset_trn, batch_size=batch_size, shuffle=True, num_workers=0)

    #dataset_val = dataset_s2d.Dataset("../data/s2d_val/")
    #valloader = torch.utils.data.DataLoader(dataset_val, batch_size=batch_size, shuffle=True, num_workers=2)

    copyfile('train_sf2d2rpz.py', output_file + '/train_script.py')
    copyfile('network_sf2d.py', output_file + '/network_sf2d.py')
    render_class = Render(net='resnet50dilated')


    seen = 0
    optimizer_s2d = optim.Adam(net.parameters(), lr=0.0001)

    for epoch in range(epochs):  # loop over the dataset multiple times

        epoch_loss = 0
        epoch_loss_pred = 0
        epoch_loss_conf = 0
        epoch_loss_rpz = 0
        for i, data in enumerate(trainloader):
            input = data['input']
            label_dem = data['label']
            input_mask = data['mask']
            weights = data['weights']
            imgs = data['images']
            label_rpz = data['label_rpz']

            T_baselink_zpr = data['T_baselink_zpr'].numpy()[0]
            input, label_dem, input_mask, weights = input.to(device), label_dem.to(device), input_mask.to(device), weights.to(device)
            '''
            features = render_class.render(input.cpu().numpy()[0], imgs[0].numpy()[0], imgs[1].numpy()[0],
                                           imgs[2].numpy()[0], imgs[3].numpy()[0], imgs[4].numpy()[0],
                                           T_baselink_zpr)

            features[np.isnan(features)] = 0
            '''
            input_w_mask = torch.cat([input,input_mask,data['features'].to(device)],1)

            output_DEM = net(input_w_mask)

            output_rpz = net_d2rpz((output_DEM[:, 0, :, :][np.newaxis]).permute([1, 0, 2, 3]))

            pos_mask = (~torch.isnan(label_rpz)).type(torch.float).to(device)
            # label_rpz[torch.isnan(label_rpz)] = 0

            loss_dem, loss_pred, loss_conf = network_sf2d.weighted_mse_loss(output_DEM, label_dem, weights)
            loss_dem.backward(retain_graph=True)

            label_rpz[torch.isnan(label_rpz)] = 0

            l_rpz = 0 * torch.ones(label_rpz.shape)
            rpz = label_rpz.cpu().detach().numpy()
            idx_b, idx1, idx2 = np.where((rpz[:, 2, :, :])!=0)
            #K = np.argmax(np.abs(rpz[:, 1, idx1, idx2] - output_rpz.cpu().detach().numpy()[:, 1, idx1, idx2]))
            #p_mask = 0 * torch.ones(pos_mask.shape)
            #l_rpz = label_rpz
            p_mask = pos_mask
            #l_rpz[:,:,idx1[K], idx2[K]] = label_rpz[:, :, idx1[K], idx2[K]]
            #p_mask[:,:,idx1[K], idx2[K]] =1
            loss_rpz = torch.sqrt((torch.sum((output_rpz[idx_b,:,idx1,idx2] - label_rpz[idx_b,:,idx1,idx2].to(device)) ** 2) / torch.tensor(idx1.size).to(device)))

            # loss_rpz = torch.sqrt((torch.sum(pos_mask * (output_rpz - label_rpz.to(device)) ** 2) / pos_mask.sum()))
            loss_rpz.backward(retain_graph=False)
            # epoch_loss_rpz += loss_rpz
            optimizer_s2d.step()
            optimizer_s2d.zero_grad()

            epoch_loss += loss_dem
            epoch_loss_pred += loss_pred
            epoch_loss_conf += loss_conf
            epoch_loss_rpz += loss_rpz
            writer.add_scalar('data/loss', loss_dem, seen)
            writer.add_scalar('data/loss_pred', loss_pred, seen)
            writer.add_scalar('data/loss_conf', loss_conf, seen)
            writer.add_scalar('data/loss_rpz', loss_rpz, seen)

            if i%100==0:
                input = input[0, :, :, :]
                input = input.cpu().numpy()
                input = input - input.min()
                input = input / input.max()
                writer.add_image('data/Image', input, seen)
                label = label_dem[0, :, :, :]
                label = label.cpu().numpy()
                label[label>0.5] = 0.5
                label[label<-0.5] = -0.5
                label = label - label.min()
                label = label / label.max()
                writer.add_image('data/Label', label, seen)
                out = output_DEM[0, 0, :, :].detach().cpu().numpy()
                out[out>0.5] = 0.5
                out[out<-0.5] = -0.5
                out = out - out.min()
                out = out / out.max()
                writer.add_image('data/Output', out[np.newaxis], seen)
                out = output_DEM[0, 1, :, :].detach().cpu().numpy()
                out = 1 / (1 + np.exp(-out))
                out[out>0.5] = 0.5
                out[out<-0.5] = -0.5
                out = out - out.min()
                out = out / out.max()
                writer.add_image('data/Output_conf', out[np.newaxis], seen)
            seen += 1

        writer.add_scalar('data/epoch_loss', epoch_loss/dataset_trn.size, epoch)
        writer.add_scalar('data/epoch_loss_pred', epoch_loss_pred/dataset_trn.size, epoch)
        writer.add_scalar('data/epoch_loss_conf', epoch_loss_conf/dataset_trn.size, epoch)
        writer.add_scalar('data/epoch_loss_rpz', epoch_loss_rpz/dataset_trn.size, epoch)

        print(epoch_loss)
        epoch_loss = 0
        epoch_val_loss = 0
        epoch_val_loss_pred = 0
        epoch_val_loss_conf = 0
        torch.save(net.state_dict(), output_file+'/net_epoch_{:04}'.format(epoch))

        '''
        for i, data in enumerate(valloader):
            input = data['input']
            label = data['label']
            input_mask = data['mask']
            weights = data['weights']
            input, label, input_mask, weights = input.to(device), label.to(device), input_mask.to(device), weights.to(device)
            input_w_mask = torch.cat([input,input_mask],1)
            with torch.no_grad():
                output = net(input_w_mask)
                loss,loss_pred,loss_conf = network_s2d.weighted_mse_loss(output, label, weights)
            epoch_val_loss += loss
            epoch_val_loss_pred += loss_pred
            epoch_val_loss_conf += loss_conf

            if i%100==0:
                input = input[0, :, :, :]
                input = input.cpu().numpy()
                # input = input.transpose((1, 2, 0))
                input = input - input.min()
                input = input / input.max()
                writer.add_image('data/val_Image', input, seen)
                label = label[0, :, :, :]
                label = label.cpu().numpy()
                label = label - label.min()
                label = label / label.max()
                writer.add_image('data/val_Label', label, seen)
                # out = torch.sigmoid(output[0,:, :, :].clone())
                out = output[0, 0, :, :].detach().cpu().numpy()
                out = out - out.min()
                out = out / out.max()
                writer.add_image('data/val_Output', out[np.newaxis], seen)
                out = output[0, 1, :, :].detach().cpu().numpy()
                out = 1 / (1 + np.exp(-out))
                out = out - out.min()
                out = out / out.max()
                writer.add_image('data/val_Output_conf', out[np.newaxis], seen)
            seen += 1

        writer.add_scalar('data/val_loss', epoch_val_loss / dataset_val.size, epoch)
        writer.add_scalar('data/val_loss_pred', epoch_val_loss_pred / dataset_val.size, epoch)
        writer.add_scalar('data/val_loss_conf', epoch_val_loss_conf / dataset_val.size, epoch)
        torch.save(net.state_dict(), output_file+'/net_epoch_{:04}'.format(epoch))
        '''
