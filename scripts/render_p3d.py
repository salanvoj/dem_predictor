#!/usr/bin/env python
import rospy
import roslib
import message_filters
import sys
import os
from sensor_msgs.msg import PointCloud2, CompressedImage, CameraInfo
from image_geometry import PinholeCameraModel
import ros_numpy
import tf2_ros
import tf.transformations
import numpy as np
import cv2
import matplotlib.pyplot as plt
import math
import time
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data as tdata
from ERFNet import load_model
from OccupancyMap import OccupancyMap, filter_pc
import DEM_sparse2dense
import queue
from network_s2d import Net
from PIL import Image
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision.transforms import Compose, CenterCrop, Normalize, Resize
from torchvision.transforms import ToTensor, ToPILImage

import pytorch3d
from pytorch3d.structures import Pointclouds, Meshes
import pytorch3d.renderer.points.rasterizer as rasterizer
import pytorch3d.renderer.mesh.rasterizer as mesh_rasterizer
import pytorch3d.renderer.cameras as cameras

DEBUG = True


def rendering(camera, pcd, features, device):
    x = pcd[0]
    y = pcd[1]
    z = pcd[2]

    x0 = x + 0.05  # coords of corners of voxels
    x1 = x - 0.05
    y0 = y + 0.05
    y1 = y - 0.05

    x = np.hstack((x0, x0, x1, x1))
    y = np.hstack((y0, y1, y0, y1))
    z = np.hstack((z, z, z, z))

    pts = np.vstack((x, y, z)).T

    # create faces (indices of vertex in triangles)
    ind_arange = np.arange(0, len(x0))
    first_faces = np.vstack((ind_arange, ind_arange + len(x0), ind_arange + 2 * len(x0))).T
    second_faces = np.vstack((ind_arange + len(x0), ind_arange + 2 * len(x0), ind_arange + 3 * len(x0))).T
    faces = np.vstack((first_faces, second_faces))

    pts = torch.from_numpy(pts).unsqueeze(0).to(device)
    pts = pts.type(torch.float)
    faces = torch.from_numpy(faces).unsqueeze(0).to(device)

    # create mesh class
    mesh = Meshes(pts, faces)

    # rasterization
    raster_settings = mesh_rasterizer.RasterizationSettings(image_size=308)

    my_rasterizer = mesh_rasterizer.MeshRasterizer(camera, raster_settings)
    my_rasterizer = my_rasterizer.to(device)
    pix_to_face, zbuf, bary_coords, dists = my_rasterizer(mesh)

    # handle features
    features = features[:308, ...]

    projected_idx = (pix_to_face != -1.)
    projected_pts = pix_to_face % len(x0)   # normalize to input points

    indices = projected_pts[projected_idx]
    indices = indices.view(-1)
    pts_pcd = pcd[:, indices]

    pts_features = features[projected_idx[0, :, :, 0], :]

    return pts_pcd, pts_features


class RenderNode():
    def __init__(self):
        self.tf = tf2_ros.Buffer(cache_time=rospy.Duration(20))
        self.tf_sub = tf2_ros.TransformListener(self.tf)

        self.map_frame = 'map'
        self.grid_resolution = 0.1
        self.width = 308
        self.height = 404
        # get camera parameters
        cam_info_topic1 = rospy.get_param("~camera_info_topic", "/viz/camera_0/camera_info")
        cam_info_topic2 = rospy.get_param("~camera_info_topic2", "/viz/camera_1/camera_info")
        cam_info_topic3 = rospy.get_param("~camera_info_topic3", "/viz/camera_2/camera_info")
        cam_info_topic4 = rospy.get_param("~camera_info_topic4", "/viz/camera_3/camera_info")
        cam_info_topic5 = rospy.get_param("~camera_info_topic5", "/viz/camera_4/camera_info")
        self.camera_ids = [cam_info_topic1, cam_info_topic2, cam_info_topic3, cam_info_topic4, cam_info_topic5]

        print('waiting for camera info')
        self.camera_model = {}
        for i in range(len(self.camera_ids)):
            msg = rospy.wait_for_message(self.camera_ids[i], CameraInfo)
            self.camera_model[msg.header.frame_id] = PinholeCameraModel()
            self.camera_model[msg.header.frame_id].fromCameraInfo(msg)
        print('camera info recieved')

        # create subscriber for camera images and pointcloud
        pcd_sub = message_filters.Subscriber(rospy.get_param("~bigbox_topic", '/dynamic_point_cloud_bigbox'),
                                             PointCloud2)
        image0_sub = message_filters.Subscriber("/viz/camera_0/image/compressed", CompressedImage)
        image1_sub = message_filters.Subscriber("/viz/camera_1/image/compressed", CompressedImage)
        image2_sub = message_filters.Subscriber("/viz/camera_2/image/compressed", CompressedImage)
        image3_sub = message_filters.Subscriber("/viz/camera_3/image/compressed", CompressedImage)
        image4_sub = message_filters.Subscriber("/viz/camera_4/image/compressed", CompressedImage)

        time_synchro = message_filters.ApproximateTimeSynchronizer(
            [pcd_sub, image0_sub, image1_sub, image2_sub, image3_sub, image4_sub], 10, 1)
        time_synchro.registerCallback(self.callback)

        # voxel maps
        self.input_3d_map = OccupancyMap(self.map_frame, self.grid_resolution)
        self.input_2d_map = OccupancyMap(self.map_frame, self.grid_resolution)
        self.voxel_map = OccupancyMap(self.map_frame, self.grid_resolution)

        if torch.cuda.is_available():
            self.device = torch.device('cuda')
        else:
            self.device = torch.device('cpu')
        self.model_s2d = Net()
        self.model_s2d = self.model_s2d.to(torch.device("cpu"))
        self.model_s2d.load_state_dict(
            torch.load("/home/honza/trav_project/net_weights_s2d", map_location=torch.device("cpu")))
        self.confidence_threshold = -3
        self.model_erfnet = load_model()

        self.voxelmap_publisher = rospy.Publisher('output_3d_pc', PointCloud2, queue_size=2)
        self.frontiers_publisher = rospy.Publisher('frontiers', PointCloud2, queue_size=2)

    def callback(self, pointcloud, cam_0, cam_1, cam_2, cam_3, cam_4):
        start_callback = time.time()
        pcd_to_map = self.tf.lookup_transform(self.map_frame, pointcloud.header.frame_id, pointcloud.header.stamp)
        pc_cloud = ros_numpy.numpify(pointcloud)
        pc_array_homogenous = np.concatenate(
            [np.asarray([pc_cloud['x'].ravel(), pc_cloud['y'].ravel(), pc_cloud['z'].ravel()]),
             np.ones_like(pc_cloud['z'])[np.newaxis]])

        T = ros_numpy.numpify(pcd_to_map.transform)
        pc_cloud_map_frame = np.matmul(T, pc_array_homogenous)

        # create sensor origin in map frame
        origins = np.ones_like(pc_cloud['x']) * np.array(
            [pcd_to_map.transform.translation.x, pcd_to_map.transform.translation.y,
             pcd_to_map.transform.translation.z], )[np.newaxis].T

        # create 3D voxel map
        self.input_3d_map.voxel_map.clear()

        self.input_3d_map.voxel_map.update_lines(origins, pc_cloud_map_frame[:3, :])
        self.input_3d_map = filter_pc(self.input_3d_map)
        self.input_2d_map = self.input_3d_map.to_2d(self.input_2d_map, pcd_to_map)

        # grid in robot frame
        robot_grid = np.meshgrid(np.arange(-12.75, 12.85, self.grid_resolution),
                                 np.arange(-12.75, 12.85, self.grid_resolution))
        robot_grid_array = np.asarray(
            [robot_grid[0].ravel(), robot_grid[1].ravel(), np.ones_like(robot_grid[1].ravel()) * self.grid_resolution])
        # to homogeneous coordinates
        robot_grid_array = np.concatenate(
            [robot_grid_array, np.ones(robot_grid_array.shape[1])[np.newaxis]])  # not sure about z=0.1
        # transform robot grid to map frame
        robot_grid_mapframe = np.matmul(T, robot_grid_array)
        # fill the input from 2d map
        robot_grid_mapframe_2d = np.concatenate([robot_grid_mapframe[0:2, :], np.ones(robot_grid_array.shape[1])[
            np.newaxis] * self.grid_resolution / 2])  # x,y,z in 2.5d map
        v = self.input_2d_map.voxel_map.get_voxels(robot_grid_mapframe_2d, np.zeros(
            robot_grid_array.shape[1])) - pcd_to_map.transform.translation.z
        # sparse map
        input_grid = np.reshape(v, robot_grid[0].shape)
        mask = torch.from_numpy((~np.isnan(input_grid)).astype(np.float32)).to(torch.device("cpu")).unsqueeze(
            0).unsqueeze(0)
        input_grid[np.isnan(input_grid)] = 0
        input = torch.from_numpy(input_grid.astype(np.float32)).to(torch.device("cpu")).unsqueeze(0).unsqueeze(0)

        input_w_mask = torch.cat([input, mask], 1)

        # fill heights
        output = self.model_s2d(input_w_mask)
        output = output.detach().cpu().numpy()

        output_conf = output[0, 1, :, :]
        # fill 2d map from output
        output = output[0, 0, (output_conf >= self.confidence_threshold)]
        robot_grid_mapframe_2d = robot_grid_mapframe_2d[:, (output_conf >= self.confidence_threshold).ravel()]
        output_map2d = OccupancyMap(self.map_frame, self.grid_resolution)
        output_map2d.voxel_map.set_voxels(robot_grid_mapframe_2d, np.zeros_like(output.ravel(), dtype=np.float64),
                                          output.ravel()-self.grid_resolution)
        # convert output to 3d map
        self.tracing_map = OccupancyMap(self.map_frame, self.grid_resolution)
        self.tracing_map = output_map2d.to_3d_dem(self.tracing_map, pcd_to_map)

        if DEBUG:
            self.voxelmap_publisher.publish(self.tracing_map.to_pc_msg(self.map_frame))

        dim_reduction = 2

        # transform voxels to camera coord systems, filter them only to camera views
        voxels_cam_frame_0 = self.transform_to_cam(cam_0)
        voxels_cam_frame_1 = self.transform_to_cam(cam_1)
        voxels_cam_frame_2 = self.transform_to_cam(cam_2)
        voxels_cam_frame_3 = self.transform_to_cam(cam_3)
        voxels_cam_frame_4 = self.transform_to_cam(cam_4)
        voxels_cam_frame_0 = self.reduce_pointcloud(voxels_cam_frame_0, self.camera_model[cam_0.header.frame_id], self.grid_resolution, dim_reduction=dim_reduction)
        voxels_cam_frame_1 = self.reduce_pointcloud(voxels_cam_frame_1, self.camera_model[cam_1.header.frame_id], self.grid_resolution, dim_reduction=dim_reduction)
        voxels_cam_frame_2 = self.reduce_pointcloud(voxels_cam_frame_2, self.camera_model[cam_2.header.frame_id], self.grid_resolution, dim_reduction=dim_reduction)
        voxels_cam_frame_3 = self.reduce_pointcloud(voxels_cam_frame_3, self.camera_model[cam_3.header.frame_id], self.grid_resolution, dim_reduction=dim_reduction)
        voxels_cam_frame_4 = self.reduce_pointcloud(voxels_cam_frame_4, self.camera_model[cam_4.header.frame_id], self.grid_resolution, dim_reduction=dim_reduction)

        print("Getting image features ...")
        features_time = time.time()
        features_0 = self.get_features(cam_0, dim_reduction=dim_reduction)
        features_1 = self.get_features(cam_1, dim_reduction=dim_reduction)
        features_2 = self.get_features(cam_2, dim_reduction=dim_reduction)
        features_3 = self.get_features(cam_3, dim_reduction=dim_reduction)
        features_4 = self.get_features(cam_4, dim_reduction=dim_reduction)

        # TODO: correct camera settings in init
        R = torch.from_numpy(np.eye(3)).unsqueeze(0)
        T = torch.from_numpy(np.zeros((1, 3)))
        camera = cameras.OpenGLPerspectiveCameras(device=self.device, R=R, T=T)

        # render
        frontiers_voxels0, features0 = rendering(camera, voxels_cam_frame_0, features_0, self.device)
        frontiers_voxels1, features1 = rendering(camera, voxels_cam_frame_1, features_1, self.device)
        frontiers_voxels2, features2 = rendering(camera, voxels_cam_frame_2, features_2, self.device)
        frontiers_voxels3, features3 = rendering(camera, voxels_cam_frame_3, features_3, self.device)
        frontiers_voxels4, features4 = rendering(camera, voxels_cam_frame_4, features_4, self.device)

        frontiers_voxels0 = np.concatenate([frontiers_voxels0, np.ones(frontiers_voxels0.shape[1])[np.newaxis]])
        cam_to_map = self.tf.lookup_transform(self.map_frame, cam_0.header.frame_id, cam_0.header.stamp)
        T = ros_numpy.numpify(cam_to_map.transform)
        frontiers_map_frame0 = np.matmul(T, frontiers_voxels0)

        frontiers_voxels1 = np.concatenate([frontiers_voxels1, np.ones(frontiers_voxels1.shape[1])[np.newaxis]])
        cam_to_map = self.tf.lookup_transform(self.map_frame, cam_1.header.frame_id, cam_1.header.stamp)
        T = ros_numpy.numpify(cam_to_map.transform)
        frontiers_map_frame1 = np.matmul(T, frontiers_voxels1)

        frontiers_voxels2 = np.concatenate([frontiers_voxels2, np.ones(frontiers_voxels2.shape[1])[np.newaxis]])
        cam_to_map = self.tf.lookup_transform(self.map_frame, cam_2.header.frame_id, cam_2.header.stamp)
        T = ros_numpy.numpify(cam_to_map.transform)
        frontiers_map_frame2 = np.matmul(T, frontiers_voxels2)

        frontiers_voxels3 = np.concatenate([frontiers_voxels3, np.ones(frontiers_voxels3.shape[1])[np.newaxis]])
        cam_to_map = self.tf.lookup_transform(self.map_frame, cam_3.header.frame_id, cam_3.header.stamp)
        T = ros_numpy.numpify(cam_to_map.transform)
        frontiers_map_frame3 = np.matmul(T, frontiers_voxels3)

        frontiers_voxels4 = np.concatenate([frontiers_voxels4, np.ones(frontiers_voxels4.shape[1])[np.newaxis]])
        cam_to_map = self.tf.lookup_transform(self.map_frame, cam_4.header.frame_id, cam_4.header.stamp)
        T = ros_numpy.numpify(cam_to_map.transform)
        frontiers_map_frame4 = np.matmul(T, frontiers_voxels4)

        frontiers_map_frame = np.hstack((frontiers_map_frame0, frontiers_map_frame1, frontiers_map_frame2, frontiers_map_frame3, frontiers_map_frame4))
        features = np.vstack((features0, features1, features2, features3, features4))

        frontiers_map = OccupancyMap(self.map_frame, self.grid_resolution)
        if not DEBUG:
            # set voxels with features
            frontiers_map.voxel_map.set_voxels(frontiers_map_frame[:3, :], np.zeros_like(frontiers_map_frame[0], dtype=np.float64), features)
        if DEBUG:
            print("Publishing frontiers PCD ...")
            # set color by given feature
            color_features = features[:, 15].reshape((features[:, 15].size, 1))
            vis_img = plt.imshow(color_features)
            color = vis_img.cmap(vis_img.norm(color_features))
            color = np.asarray(color[:, :, :3] * 255, dtype=np.int)
            color = color[:, :, 0] * 256 * 256 + color[:, :, 1] * 256 + color[:, :, 2]
            # set voxels with colors and publish
            frontiers_map.voxel_map.set_voxels(frontiers_map_frame[:3, :], np.zeros_like(frontiers_map_frame[0], dtype=np.float64), color)
            self.frontiers_publisher.publish(frontiers_map.to_pc_msg_coloured(self.map_frame))
        print("Done. Callback overall took: " + str(time.time() - start_callback - features_time))

    def get_features(self, cam, dim_reduction=2):
        input_transform = Compose([
            Resize((1616/dim_reduction, 1232/dim_reduction), Image.BILINEAR),
            ToTensor(),
            # Normalize([.485, .456, .406], [.229, .224, .225]),
        ])

        np_arr = np.fromstring(cam.data, np.uint8)
        image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
        im_pil = Image.fromarray(image_np)
        im_pil = input_transform(im_pil).unsqueeze(0)
        im_pil = im_pil.to(self.device)
        features = self.model_erfnet(im_pil)
        features = features.detach().cpu().numpy().transpose(0, 2, 3, 1)
        return features.reshape(features.shape[1], features.shape[2], features.shape[3])

    def transform_to_cam(self, cam):
        voxels, _, _ = self.tracing_map.voxel_map.get_voxels()
        voxels = np.asarray(voxels)
        voxels = np.concatenate([voxels, np.ones(voxels.shape[1])[np.newaxis]])
        map_to_cam = self.tf.lookup_transform(cam.header.frame_id, self.map_frame, cam.header.stamp)
        T = ros_numpy.numpify(map_to_cam.transform)

        voxels_cam_frame = np.matmul(T, voxels)
        # omit voxels behind camera
        voxels_cam_frame = voxels_cam_frame[:3, voxels_cam_frame[2] > 0]    # z-coord is negative for such voxels

        self.voxel_map.voxel_map.set_voxels(voxels_cam_frame, np.zeros_like(voxels_cam_frame[1]), np.ones_like(voxels_cam_frame[1]))
        voxels_cam_frame, _, _ = self.voxel_map.voxel_map.get_voxels()
        self.voxel_map.voxel_map.clear()

        return voxels_cam_frame

    def reduce_pointcloud(self, voxels, camera_model, resolution, dim_reduction=2):
        # pick only voxels in camera view
        fx = camera_model.fx()
        fy = camera_model.fy()
        Tx = camera_model.Tx()
        Ty = camera_model.Ty()
        cx = camera_model.cx()
        cy = camera_model.cy()

        u0_arr = np.round(
            ((fx * (voxels[0, :] - resolution / 2) + Tx) / voxels[2, :] + cx) / (
                        2 * dim_reduction)).astype(np.int)
        v0_arr = np.round(
            ((fy * (voxels[1, :] - resolution / 2) + Ty) / voxels[2, :] + cy) / (
                        2 * dim_reduction)).astype(np.int)

        u2_arr = np.round(
            ((fx * (voxels[0, :] + resolution / 2) + Tx) / voxels[2, :] + cx) / (
                        2 * dim_reduction)).astype(np.int)
        v1_arr = np.round(
            ((fy * (voxels[1, :] + resolution / 2) + Ty) / voxels[2, :] + cy) / (
                        2 * dim_reduction)).astype(np.int)

        # pick only valid pixels (in image plane)
        condition_indices = np.logical_not(
            np.logical_or(np.logical_and(u0_arr < 0, u2_arr < 0), np.logical_and(v0_arr < 0, v1_arr < 0),
                          np.logical_or(np.logical_and(u0_arr >= self.width, u2_arr >= self.width),
                                        np.logical_and(v0_arr >= self.height, v1_arr >= self.height))))

        pcd_arr = voxels[:, condition_indices]

        return pcd_arr


def render_node():
    rospy.init_node("render_node", anonymous=True)

    render = RenderNode()

    rospy.spin()


if __name__ == '__main__':
    # args = rospy.myargv(argv=sys.argv)
    render_node()
