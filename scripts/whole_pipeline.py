import numpy as np
import rospy
import torch
import matplotlib.pyplot as plt
from ros_numpy import numpify
from voxel_map import VoxelMap
from sensor_msgs.msg import PointCloud2
import sensor_msgs.point_cloud2 as pc2
from nav_msgs.msg import OccupancyGrid
from OccupancyMap import OccupancyMap, filter_pc
import network_s2d
import network_d2rpz
import tf2_ros
import time
from PIL import Image

DEBUG = True

class DEM_s2d2rpz():
    def __init__(self):
        rospy.init_node('s2d2rpz')
        self.grid_res = 0.1
        self.map_frame = 'map_fast'

        self.tf = tf2_ros.Buffer(cache_time=rospy.Duration(30))
        self.tf_sub = tf2_ros.TransformListener(self.tf)
        self.input_map3d = OccupancyMap(self.map_frame, self.grid_res)
        self.input_map2d = OccupancyMap(self.map_frame, self.grid_res)

        self.points_sub = rospy.Subscriber(rospy.get_param("~bigbox_topic", '/dynamic_point_cloud_bigbox'), PointCloud2, self.points_cb, queue_size=2)

        self.input_pc_pub = rospy.Publisher('input_pc', PointCloud2, queue_size=2)
        self.input_filtered_pc_pub = rospy.Publisher('input_filtered_pc', PointCloud2, queue_size=2)
        self.input_2d_pc_pub = rospy.Publisher('input_2d_pc',  PointCloud2, queue_size=2)
        self.output_2d_pc_pub = rospy.Publisher('output_2d_pc',  PointCloud2, queue_size=2)
        self.output_3d_pc_pub = rospy.Publisher('output_3d_pc',  PointCloud2, queue_size=2)
        self.output_3d_pc_dem_pub = rospy.Publisher('output_3d_pc_dem',  PointCloud2, queue_size=2)
        self.output_trav_pc_pub = rospy.Publisher('traversability',  PointCloud2, queue_size=2)

        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.model_s2d = network_s2d.Net()
        self.model_s2d.load_state_dict(torch.load("../data/s2d_network/net_weights_s2d", map_location=self.device))
        self.model_s2d.to(self.device)
        self.model_d2rpz = network_d2rpz.Net()
        self.model_d2rpz.load_state_dict(torch.load("../data/d2rpz_network/net_weights_d2rpz", map_location=self.device))
        self.model_d2rpz.to(self.device)

        self.confidence_threshold = -5
        print('s2d2t ready')



    def points_cb(self, msg):
        if (rospy.get_rostime() - msg.header.stamp).secs > 5:
            print('old msg')
            print((rospy.get_rostime() - msg.header.stamp).secs)
            return
        time_start = time.time()

        # get TF
        sensor_to_map = self.tf.lookup_transform(self.map_frame, msg.header.frame_id, msg.header.stamp,timeout=rospy.Duration(1))

        # create pc from msg
        pc_cloud = numpify(msg)
        pc_array_homo = np.concatenate([np.asarray([pc_cloud['x'].ravel(), pc_cloud['y'].ravel(), pc_cloud['z'].ravel()]), np.ones_like(pc_cloud['z'])[np.newaxis]])
        T = numpify(sensor_to_map.transform)
        # cloud in map frame
        pc_cloud_in_map = np.matmul(T, pc_array_homo)

        # creates sensor origin in map frame
        origins = np.ones_like(pc_cloud['x']) * np.array([sensor_to_map.transform.translation.x, sensor_to_map.transform.translation.y, sensor_to_map.transform.translation.z], )[np.newaxis].T

        self.input_map3d = OccupancyMap(self.map_frame, self.grid_res)
        # create 3d voxelmap
        self.input_map3d.voxel_map.update_lines(origins, pc_cloud_in_map[:3, :])
        if DEBUG:
            self.input_pc_pub.publish(self.input_map3d.to_pc_msg(self.map_frame))

        ##  NOT FILTERING CEILING NOW!
        # filter ceilings and measurements outliers
        # self.input_map3d = filter_pc(self.input_map3d)
        # if DEBUG:
        #     self.input_filtered_pc_pub.publish(self.input_map3d.to_pc_msg(self.map_frame))

        # get 2d from 3d
        self.input_map2d = self.input_map3d.to_2d(self.input_map2d, sensor_to_map)
        if DEBUG:
            self.input_2d_pc_pub.publish(self.input_map2d.to_pc_msg(self.map_frame, -np.inf))

        # grid in robot frame
        robot_grid = np.meshgrid(np.arange(-12.75, 12.85, 0.1), np.arange(-12.75, 12.85, 0.1))
        robot_grid_array = np.asarray([robot_grid[0].ravel(), robot_grid[1].ravel(), np.ones_like(robot_grid[1].ravel()) * self.grid_res])
        # to homogeneous coordinates
        robot_grid_array_h = np.concatenate([robot_grid_array, np.ones(robot_grid_array.shape[1])[np.newaxis]])  # not sure about z=0.1
        # transform robot grid to map frame
        robot_grid_mapframe = np.matmul(T, robot_grid_array_h)
        # fill the input from 2d map
        robot_grid_mapframe_2d = np.concatenate([robot_grid_mapframe[0:2, :], np.ones(robot_grid_array.shape[1])[np.newaxis] * self.grid_res / 2])  # x,y,z in 2.5d map
        v = self.input_map2d.voxel_map.get_voxels(robot_grid_mapframe_2d, np.zeros(robot_grid_array.shape[1])) - sensor_to_map.transform.translation.z
        # sparse map
        input_grid = np.reshape(v, robot_grid[0].shape)
        mask = torch.from_numpy((~np.isnan(input_grid)).astype(np.float32)).to(self.device).unsqueeze(0).unsqueeze(0)
        input_grid[np.isnan(input_grid)] = 0
        input = torch.from_numpy(input_grid.astype(np.float32)).to(self.device).unsqueeze(0).unsqueeze(0)
        input_w_mask = torch.cat([input, mask], 1)

        output_s2d = self.model_s2d(input_w_mask)

        output_d = output_s2d.detach().cpu().numpy()

        output_conf = output_d[0, 1, :, :]
        output_d = output_d[0, 0, :, :]
        #output_d = output_d[0, 0, (output_conf >= self.confidence_threshold)]
        #robot_grid_mapframe_2d = robot_grid_mapframe_2d[:, (output_conf >= self.confidence_threshold).ravel()]




        print(['sd2', time.time() - time_start])
        time_start = time.time()
        roll_all_rot = np.zeros([8,250,248])
        pitch_all_rot = np.zeros([8,250,248])

        # prepare positions of trav output
        output_trav_robot_grid = []
        output_trav_robot_grid.append(robot_grid[0][3:-3, 4:-4])
        output_trav_robot_grid.append(robot_grid[1][3:-3, 4:-4])
        output_trav_robot_grid_array = np.asarray(
            [output_trav_robot_grid[0].ravel(), output_trav_robot_grid[1].ravel(),
             np.ones_like(output_trav_robot_grid[1].ravel()) * self.grid_res/2])
        # to homogeneous coordinates
        output_trav_robot_grid_array_h = np.concatenate(
            [output_trav_robot_grid_array,
             np.ones(output_trav_robot_grid_array.shape[1])[np.newaxis]])  # not sure about z=0.1
        # transform robot grid to map frame
        output_trav_robot_grid_mapframe = np.matmul(T, output_trav_robot_grid_array_h)
        # fill the input from 2d map
        output_trav_robot_grid_mapframe_2d = np.concatenate(
            [output_trav_robot_grid_mapframe[0:2, :], np.ones(output_trav_robot_grid_array.shape[1])[
                np.newaxis] * self.grid_res / 2])  # x,y,z in 2.5d map


        yaw_i = 0
        for yaw_rot in np.linspace(0,315,8).tolist():
            dem_rotate = np.asarray(Image.fromarray(output_d[:, :]).rotate(yaw_rot))
            input_dem = torch.from_numpy(dem_rotate).to(self.device).unsqueeze(0).unsqueeze(0)
            output_d2rpz = self.model_d2rpz(input_dem)
            output_d2rpz = output_d2rpz.detach().cpu().numpy()
            roll_d2rpz_grid = np.asarray(Image.fromarray(output_d2rpz[0, 0, :, :]).rotate(-yaw_rot))
            pitch_d2rpz_grid = np.asarray(Image.fromarray(output_d2rpz[0, 1, :, :]).rotate(-yaw_rot))
            roll_all_rot[yaw_i,:,:] = roll_d2rpz_grid[:,:]
            pitch_all_rot[yaw_i,:,:] = pitch_d2rpz_grid[:,:]
            yaw_i += 1
        print(['d2rpz', time.time() - time_start])

        output_d = output_d[(output_conf >= self.confidence_threshold)]
        robot_grid_mapframe_2d = robot_grid_mapframe_2d[:, (output_conf >= self.confidence_threshold).ravel()]

        # fill 2d map from output
        output_map2d = OccupancyMap(self.map_frame, self.grid_res)
        output_map2d.voxel_map.set_voxels(robot_grid_mapframe_2d, np.zeros_like(output_d.ravel(), dtype=np.float64),
                                          output_d.ravel())

        if DEBUG:
            self.output_2d_pc_pub.publish(output_map2d.to_pc_msg(self.map_frame, -np.inf))

        # convert output to 3d map
        output_map3d = OccupancyMap(self.map_frame, self.grid_res)
        output_map3d = output_map2d.to_3d(output_map3d, sensor_to_map)
        if DEBUG:
            self.output_3d_pc_pub.publish(output_map3d.to_pc_msg(self.map_frame))

        output_map3d_dem = OccupancyMap(self.map_frame, self.grid_res)
        output_map3d_dem = output_map2d.to_3d_dem(output_map3d_dem, sensor_to_map, depth=0.2)
        self.output_3d_pc_dem_pub.publish(output_map3d_dem.to_pc_msg(self.map_frame))

        output_trav_conf = output_conf[3:-3, 4:-4]

        output_trav_robot_grid_mapframe_2d = output_trav_robot_grid_mapframe_2d[:, (output_trav_conf >= self.confidence_threshold).ravel()]

        # get travesty by roll pitch thresholding

        traversable = (np.sum(abs(pitch_all_rot),0)+np.sum(abs(roll_all_rot),0))
        traversable = (traversable/np.median(traversable))[output_trav_conf >= self.confidence_threshold]
        z = output_map2d.voxel_map.get_voxels(output_trav_robot_grid_mapframe_2d, np.zeros(output_trav_robot_grid_mapframe_2d.shape[1])) + sensor_to_map.transform.translation.z

        #z = np.reshape(v, output_trav_robot_grid[0].shape)
        output_trav_robot_grid_mapframe_2d[2] = z
        output_traversability_map = OccupancyMap(self.map_frame, self.grid_res)
        output_traversability_map.voxel_map.set_voxels(output_trav_robot_grid_mapframe_2d, np.zeros_like(traversable.ravel(), dtype=np.float64), traversable.ravel())
        self.output_trav_pc_pub.publish(output_traversability_map.to_pc_msg(self.map_frame))

if __name__ == '__main__':
    s2d2rpz = DEM_s2d2rpz()
    rospy.spin()