import numpy as np
from network_s2d import Net, weighted_mse_loss
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cm
import time
import os
from voxel_map import VoxelMap
from scipy import ndimage
from scipy.optimize import nnls

INPUT_PATH = '../data/s2d_newdata_rpz/'
OUTPUT_PATH = '../data/s2d_evaldata_rpz/rigid_body/output/003/'
CREATE_MOVIE = False  # True
EPOCHS = 25
# TRAINING_SAMPLES = np.linspace(0,1,1, dtype ='int')
TRAINING_SAMPLES = np.linspace(0, 300, 301, dtype='int')
VISU = False


# device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
class RigidBodyOptimizer():
    def __init__(self, net):
        self.optimizer = optim.Adam(net.parameters(), lr=0.0001)

    ##### KKT Loss #####

    def optimize_network(self, net, input_w_mask, x_in, points, indexes, label_dem, weights, visu=False):
        # initialization
        # self.optimizer = optim.Adam(net.parameters(), lr=0.0001)
        device = torch.device("cpu")
        N = points.shape[1]  # number of robot points
        C = 100  # linear barrier penalty
        r1 = indexes[0]
        r2 = indexes[1]
        c1 = indexes[2]
        c2 = indexes[3]
        lamb = torch.tensor(1 * torch.ones(N, 1), requires_grad=True, dtype=torch.float32).to(device)
        x = torch.tensor(x_in, requires_grad=True, dtype=torch.float32).to(device)
        Ggx = torch.tensor(0 * torch.ones(x.shape[0], N), requires_grad=False, dtype=torch.float32).to(device)
        Lkkt = np.zeros([1, EPOCHS])

        for epoch in range(EPOCHS):
            # print("epoch:", epoch)

            # Compute feedforward pass and crop terrain under robot
            output_dem = net(input_w_mask.unsqueeze(0))

            loss_dem, _, _ = weighted_mse_loss(output_dem, label_dem, weights)
            loss_dem.backward(retain_graph=True)
            outputs = output_dem[0, 0, r1:r2, c1:c2]

            # Compute LOSS
            loss, loss_energy, loss_collision, pt, min_heights = loss_KKT(x, points, outputs.squeeze(), visu)

            ##### Estimate optimal lambda wrt fixed heights and dual feasibility constraint
            Lgx = torch.autograd.grad(loss_energy + (lamb.squeeze() * loss_collision).sum(), x, create_graph=True,
                                      retain_graph=True)[0].clone().detach()
            Fgx = torch.autograd.grad(loss_energy, x, create_graph=True, retain_graph=True)[0].clone().detach()
            for ii in range(N):
                Ggx[:, ii] = torch.autograd.grad(loss_collision[ii], x, create_graph=True, retain_graph=True)[
                    0].clone().detach()
            g = loss_collision.detach().numpy()
            bb = np.concatenate((np.reshape(Fgx.detach().numpy(), (6, 1)), np.zeros((63, 1)))).squeeze()
            AA = np.concatenate(
                (-np.tile(Ggx.detach().numpy(), (1, 1)), np.diagflat(np.tile(g, (1, 1)))))  # np.tile(g, (2, 1))
            try:
                s, acc = nnls(AA, bb)
            except:
                print('nnls failed')
                return
            lamb = torch.tensor(s.reshape(N, 1), requires_grad=False, dtype=torch.float32).to(device)

            ###### Optimize network wrt lamda
            LOSS = Lgx.pow(2).sum() + (lamb.squeeze() * loss_collision).pow(2).sum() + C * torch.relu(
                loss_collision).pow(
                2).sum()
            # print("LOSS-KKT: ", LOSS.detach().numpy(), " = Stationarity:", Lgx.pow(2).sum().detach().numpy()," + Complementary slackness:", (lamb.squeeze() * loss_collision).pow(2).sum().detach().numpy(), " + Primal feasibility:", C * torch.relu(loss_collision).pow(2).sum().detach().numpy())
            Lkkt[0, epoch] = LOSS.detach().numpy()
            LOSS.backward(retain_graph=False)
            if LOSS.detach().numpy() > 100:
                self.optimizer.zero_grad()
                return
            self.optimizer.step()
            # zero the parameter gradients
            self.optimizer.zero_grad()

            '''



        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        # initialization
        r1 = indexes[0]
        r2 = indexes[1]
        c1 = indexes[2]
        c2 = indexes[3]
        N = points.shape[1]  # number of robot points
        C = 100  # linear barrier penalty
        #optimizer = optim.Adam(net.parameters(), lr=0.0001)
        lamb = torch.tensor(1 * torch.ones(N, 1), requires_grad=True, dtype=torch.float32)
        x = torch.tensor(x_in, requires_grad=True, dtype=torch.float32)
        Ggx = torch.tensor(0 * torch.ones(x.shape[0], N), requires_grad=False, dtype=torch.float32)
        Lkkt = np.zeros([1, EPOCHS])

        for epoch in range(EPOCHS):
            print("epoch:", epoch)

            # Compute feedforward pass and crop terrain under robot
            output_dem = net(input_w_mask.unsqueeze(0))
            outputs = output_dem[0, 0, r1:r2, c1:c2].cpu().detach()

            # Compute LOSS
            loss, loss_energy, loss_collision, pt, min_heights = loss_KKT(x, points.cpu(), outputs.squeeze(), visu)
            ##### Estimate optimal lambda wrt fixed heights and dual feasibility constraint
            for ii in range(N):
                Ggx[:, ii] = torch.autograd.grad(loss_collision[ii], x, create_graph=True, retain_graph=True)[0].detach()
            Lgx = torch.autograd.grad(loss_energy + (lamb.squeeze() * loss_collision).sum(), x, create_graph=True,
                                      retain_graph=True)[0].detach()
            Fgx = torch.autograd.grad(loss_energy, x., create_graph=True, retain_graph=True)[0].detach()




            g = loss_collision.detach().cpu().numpy()
            bb = np.concatenate((np.reshape(Fgx.detach().cpu().numpy(), (6, 1)), np.zeros((63, 1)))).squeeze()
            AA = np.concatenate(
                (-np.tile(Ggx.detach().cpu().numpy(), (1, 1)), np.diagflat(np.tile(g, (1, 1)))))  # np.tile(g, (2, 1))
            s, acc = nnls(AA, bb)
            lamb = torch.tensor(s.reshape(N, 1), requires_grad=False, dtype=torch.float32)

            ###### Optimize network wrt lamda
            LOSS = Lgx.pow(2).sum() + (lamb.squeeze() * loss_collision).pow(2).sum() + C * torch.relu(loss_collision).pow(
                2).sum()
            print("LOSS-KKT: ", LOSS.detach().cpu().numpy(), " = Stationarity:", Lgx.pow(2).sum().detach().cpu().numpy(), " + Complementary slackness:", (lamb.squeeze() * loss_collision).pow(2).sum().detach().cpu().numpy(), " + Primal feasibility:", C * torch.relu(loss_collision).pow(2).sum().detach().cpu().numpy())
            Lkkt[0, epoch] = LOSS.detach().cpu().numpy()
            LOSS.backward(retain_graph=False)

            optimizer.step()
            optimizer.zero_grad()
            '''

        return


def loss_KKT(x, points, height, visu=False):
    device = torch.device("cpu")

    N = points.shape[1]
    length = 0.8
    width = 0.6
    grid_res = 0.1
    length_t = torch.tensor(length).to(device)
    width_t = torch.tensor(width).to(device)
    step_t = torch.tensor(grid_res).to(device)

    # create rotation matrix and translation vector from variable x
    Rx = torch.tensor(torch.zeros(3, 3), requires_grad=False, dtype=torch.float32).to(device)
    Ry = torch.tensor(torch.zeros(3, 3), requires_grad=False, dtype=torch.float32).to(device)
    Rz = torch.tensor(torch.zeros(3, 3), requires_grad=False, dtype=torch.float32).to(device)
    t = torch.tensor(torch.zeros(3, 1), requires_grad=False, dtype=torch.float32).to(device)

    Rx[0, 0] = 1
    Rx[1, 1] = torch.cos(x[0]).to(device)
    Rx[1, 2] = -torch.sin(x[0]).to(device)
    Rx[2, 1] = torch.sin(x[0]).to(device)
    Rx[2, 2] = torch.cos(x[0]).to(device)

    Ry[0, 0] = torch.cos(x[1]).to(device)
    Ry[0, 2] = torch.sin(x[1]).to(device)
    Ry[1, 1] = 1
    Ry[2, 0] = -torch.sin(x[1]).to(device)
    Ry[2, 2] = torch.cos(x[1]).to(device)

    # yaw does not apply
    Rz[0, 0] = torch.cos(x[2])
    Rz[0, 1] = -torch.sin(x[2])
    Rz[1, 0] = torch.sin(x[2])
    Rz[1, 1] = torch.cos(x[2])
    Rz[2, 2] = 1

    t[0, 0] = x[3]
    t[1, 0] = x[4]
    t[2, 0] = x[5]

    # define loss
    pt = (torch.mm(torch.mm(torch.mm(Rx, Ry), Rz), points.to(device)) + t.repeat(1, N)).to(
        device)  # transform pointcloud
    rr = torch.clamp(((width_t / 2 + pt[1, :]) / step_t).long(), 0, height.shape[0] - 1).to(device)
    cc = torch.clamp(((length_t / 2 + pt[0, :]) / step_t).long(), 0, height.shape[1] - 1).to(device)
    min_heights = height[rr, cc].to(device)  # search for corresponding heights
    loss_collision = (min_heights - pt[2, :])
    loss_energy = (pt[2, :]).sum().to(device)
    loss = (loss_collision.sum() + loss_energy)
    if visu:
        draw_bars = True
        plt.figure(1)
        plt.clf()
        ax = plt.axes(projection='3d')
        Z = height.detach().cpu().numpy()
        if draw_bars:
            # ax.bar3d(X.ravel(), Y.ravel(), Z.ravel(), 0.1, 0.1, -0.01, shade=True, alpha=0.5)
            # ax.bar3d(X.ravel(), Y.ravel(), Z.ravel()/2, 0.1, 0.1, Z.ravel(), shade=True, alpha=0.5)
            # norm = colors.Normalize(lamb.min(), lamb.max())
            ax.bar3d(heightmap_grid[0].ravel(), -heightmap_grid[1].ravel(), Z.ravel(), 0.1, 0.1, -0.01, shade=True,
                     alpha=0.5)  # color=cm.jet(norm(lamb.detach().numpy()).ravel()), shade=True, alpha=0.5)
        else:
            surf = ax.plot_surface(X, Y, height.detach().cpu().numpy(), linewidth=0, antialiased=False, alpha=0.5)
        PT = pt.detach().cpu().numpy()
        idx = (min_heights.detach().cpu() - pt[2, :].detach().cpu()).numpy() > 0.01
        ax.scatter(PT[0, :], -PT[1, :], PT[2, :], marker='o', color='b')
        ax.scatter(PT[0, idx], -PT[1, idx], PT[2, idx], s=80, marker='o', color='r')
        ax.plot_wireframe(np.reshape(PT[0], robot_grid[0].shape), np.reshape(-PT[1], robot_grid[0].shape),
                          np.reshape(PT[2], robot_grid[0].shape), color='k')
        plt.pause(0.001)
        plt.draw()
    return loss, loss_energy, loss_collision, pt, min_heights


def load_data(LABEL_NUMBER):
    data = np.load(INPUT_PATH + '{:06d}_label.npz'.format(LABEL_NUMBER))
    yaw_mask = abs(data['yaw_label']) < (np.pi / 8)
    yaw_indexes = np.where(yaw_mask)  ## here are indexes of known roll, pitch, z
    yaw = data['yaw_label']

    roll_label = data['roll_label']
    pitch_label = data['pitch_label']
    z_label = data['z_baselink_label']
    roll_label[~yaw_mask] = np.nan
    pitch_label[~yaw_mask] = np.nan
    z_label[~yaw_mask] = np.nan

    vis = data['visible_mask']
    vis[~np.isnan(vis)] = 0
    vis[np.isnan(vis)] = 1
    bw_dist = ndimage.distance_transform_edt(vis)
    pos_mask = ~np.isnan(yaw_mask) & (bw_dist < 10)  # distance in decimeters from visible
    pos_indexes = np.where(pos_mask)

    roll_label[~pos_mask] = np.nan
    pitch_label[~pos_mask] = np.nan
    z_label[~pos_mask] = np.nan

    rpz = np.stack([roll_label, pitch_label, z_label])
    mask = torch.from_numpy((~np.isnan(data['input'])).astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0)
    input = torch.from_numpy(data['input'].astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0)

    input[torch.isnan(input)] = 0
    input_w_mask = torch.cat([input, mask], 1)

    label = torch.from_numpy(data['label'].astype(np.float32)).to(device).unsqueeze(0).unsqueeze(0)
    return input_w_mask, rpz, yaw, label


if __name__ == '__main__':

    float_formatter = "{:.4f}".format
    np.set_printoptions(formatter={'float_kind': float_formatter})
    device = torch.device("cpu")

    ##### Load Network #####
    net = Net()
    net.load_state_dict(torch.load("../data/s2d_network/net_weights_s2d", map_location='cpu'))

    ##### Define ROBOT  #####
    length = 0.8
    width = 0.6
    grid_res = 0.1
    border_x = torch.tensor(length / 2)
    border_y = torch.tensor(width / 2)
    length_t = torch.tensor(length).to(device)
    width_t = torch.tensor(width).to(device)
    step_t = torch.tensor(grid_res).to(device)
    robot_grid = np.meshgrid(np.arange(-length / 2, length / 2 + grid_res, grid_res),
                             np.arange(-width / 2, width / 2 + grid_res, grid_res))
    LT = np.zeros_like(robot_grid[0], dtype=int)
    LT[0:2, :] = 1
    RT = np.zeros_like(robot_grid[0], dtype=int)
    RT[5:, :] = 1
    X = robot_grid[0]
    Y = robot_grid[1]
    Z = np.zeros_like(X)
    Z[(LT == 0) & (RT == 0)] += 0.1
    Z = Z - 0.07  # Z.mean()
    N = Z.size
    P = np.stack((X.reshape([1, N]).squeeze(), Y.reshape([1, N]).squeeze(), Z.reshape([1, N]).squeeze()), 0)
    points = torch.tensor(P, dtype=torch.float32).to(device)

    ##### Initialize heightmap grid (just for visualization) #####

    heightmap_grid = np.meshgrid(
        np.arange(-robot_grid[0].shape[1] * grid_res / 2 + grid_res / 2,
                  robot_grid[0].shape[1] * grid_res / 2 + grid_res / 2, grid_res),
        np.arange(-robot_grid[0].shape[0] * grid_res / 2 + grid_res / 2,
                  robot_grid[0].shape[0] * grid_res / 2 + grid_res / 2, grid_res))

    Lkkt = np.zeros([TRAINING_SAMPLES.shape[0], EPOCHS])

    for LABEL_NUMBER in TRAINING_SAMPLES:
        print('TRAINING ON SAMPLE', LABEL_NUMBER)

        ##### Load training data #####
        input_w_mask, rpz, yaw, label = load_data(LABEL_NUMBER)

        ###### Find patch with steepest pitch
        idx1, idx2 = np.where(~np.isnan(rpz[2, :, :]))
        K = np.argmax(np.abs(rpz[1, idx1, idx2]))
        r1 = idx1[K] - 3
        r2 = idx1[K] + 4
        c1 = idx2[K] - 4
        c2 = idx2[K] + 5
        RPZ = rpz[:, idx1[K], idx2[K]]
        YAW = yaw[idx1[K], idx2[K]]
        x_in = torch.tensor([RPZ[0], RPZ[1], YAW, 0, 0, RPZ[2]])
        # x_in = torch.tensor([0, 0 , 0, 0, 0, 0])

        ################################## OPTIMIZE NETWORK ##################################
        time_start = time.time()
        net, Lkkt[LABEL_NUMBER, :] = optimize_network(net, input_w_mask, x_in, points, [r1, r2, c1, c2], VISU)
        print("Optimization time:", time.time() - time_start, "sec")
        ######################################################################################

        print('Mean Lkkt[0]=', np.mean(Lkkt[0:(LABEL_NUMBER + 1), 0]), 'Mean Lkkt[', EPOCHS - 1, ' ]=',
              np.mean(Lkkt[0:(LABEL_NUMBER + 1), EPOCHS - 1]))
        print(
            'Lkkt absolute reduction:', np.mean(Lkkt[0:(LABEL_NUMBER + 1), 0] - Lkkt[0:(LABEL_NUMBER + 1), EPOCHS - 1]))
        print(
            'Lkkt relative reduction:',
            np.nanmean(Lkkt[0:(LABEL_NUMBER + 1), EPOCHS - 1] / Lkkt[0:(LABEL_NUMBER + 1), 0]))