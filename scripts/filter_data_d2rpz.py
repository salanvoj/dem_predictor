import glob
import os
from shutil import copyfile
import numpy as np

if __name__ == '__main__':
    root_dir = '../data/d2rpz_labels/'
    list_data = glob.glob(root_dir + '*label.npy')
    size_data = len(list_data)
    list_data.sort()

    export_dir = '../data/d2rpz_labels/'
    if not os.path.exists(export_dir):
        os.makedirs(export_dir)
    list_ex_data = glob.glob(export_dir + '*label.npz')
    size_ex_data = len(list_ex_data)

    to_create_i = len(list_ex_data)

    for i in range(size_data):
        #source_file = root_dir + '{:06}'.format(i) + '_label.npz'
        source_file = list_data[i]
        new_file = export_dir +  '{:04}'.format(to_create_i) + '_label.npz'
        dem = np.load(root_dir+str(i)+"_dem.npy")
        label = np.load(root_dir+str(i)+"_label.npy")
        #copyfile(source_file, new_file)
        np.savez(new_file,input=dem,label=label)
        to_create_i += 1

    #label_file = '{:06}'.format(idx) + '_label.npz'
    #data = np.load(self.root_dir + label_file)